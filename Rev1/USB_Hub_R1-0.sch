<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.0.1">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="11" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="14" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="14" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Bemassung" color="7" fill="1" visible="yes" active="yes"/>
<layer number="102" name="bot_pads" color="7" fill="5" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="S_DOKU" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="OrgLBR" color="13" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="BIT_Library">
<packages>
<package name="LQFP-32_7X7">
<smd name="1" x="-4.2164" y="2.794" dx="0.508" dy="1.4732" layer="1" rot="R270"/>
<smd name="2" x="-4.2164" y="2.0066" dx="0.508" dy="1.4732" layer="1" rot="R270"/>
<smd name="3" x="-4.2164" y="1.1938" dx="0.508" dy="1.4732" layer="1" rot="R270"/>
<smd name="4" x="-4.2164" y="0.4064" dx="0.508" dy="1.4732" layer="1" rot="R270"/>
<smd name="5" x="-4.2164" y="-0.4064" dx="0.508" dy="1.4732" layer="1" rot="R270"/>
<smd name="6" x="-4.2164" y="-1.1938" dx="0.508" dy="1.4732" layer="1" rot="R270"/>
<smd name="7" x="-4.2164" y="-2.0066" dx="0.508" dy="1.4732" layer="1" rot="R270"/>
<smd name="8" x="-4.2164" y="-2.794" dx="0.508" dy="1.4732" layer="1" rot="R270"/>
<smd name="9" x="-2.794" y="-4.2164" dx="0.508" dy="1.4732" layer="1" rot="R180"/>
<smd name="10" x="-2.0066" y="-4.2164" dx="0.508" dy="1.4732" layer="1" rot="R180"/>
<smd name="11" x="-1.1938" y="-4.2164" dx="0.508" dy="1.4732" layer="1" rot="R180"/>
<smd name="12" x="-0.4064" y="-4.2164" dx="0.508" dy="1.4732" layer="1" rot="R180"/>
<smd name="13" x="0.4064" y="-4.2164" dx="0.508" dy="1.4732" layer="1" rot="R180"/>
<smd name="14" x="1.1938" y="-4.2164" dx="0.508" dy="1.4732" layer="1" rot="R180"/>
<smd name="15" x="2.0066" y="-4.2164" dx="0.508" dy="1.4732" layer="1" rot="R180"/>
<smd name="16" x="2.794" y="-4.2164" dx="0.508" dy="1.4732" layer="1" rot="R180"/>
<smd name="17" x="4.2164" y="-2.794" dx="0.508" dy="1.4732" layer="1" rot="R270"/>
<smd name="18" x="4.2164" y="-2.0066" dx="0.508" dy="1.4732" layer="1" rot="R270"/>
<smd name="19" x="4.2164" y="-1.1938" dx="0.508" dy="1.4732" layer="1" rot="R270"/>
<smd name="20" x="4.2164" y="-0.4064" dx="0.508" dy="1.4732" layer="1" rot="R270"/>
<smd name="21" x="4.2164" y="0.4064" dx="0.508" dy="1.4732" layer="1" rot="R270"/>
<smd name="22" x="4.2164" y="1.1938" dx="0.508" dy="1.4732" layer="1" rot="R270"/>
<smd name="23" x="4.2164" y="2.0066" dx="0.508" dy="1.4732" layer="1" rot="R270"/>
<smd name="24" x="4.2164" y="2.794" dx="0.508" dy="1.4732" layer="1" rot="R270"/>
<smd name="25" x="2.794" y="4.2164" dx="0.508" dy="1.4732" layer="1" rot="R180"/>
<smd name="26" x="2.0066" y="4.2164" dx="0.508" dy="1.4732" layer="1" rot="R180"/>
<smd name="27" x="1.1938" y="4.2164" dx="0.508" dy="1.4732" layer="1" rot="R180"/>
<smd name="28" x="0.4064" y="4.2164" dx="0.508" dy="1.4732" layer="1" rot="R180"/>
<smd name="29" x="-0.4064" y="4.2164" dx="0.508" dy="1.4732" layer="1" rot="R180"/>
<smd name="30" x="-1.1938" y="4.2164" dx="0.508" dy="1.4732" layer="1" rot="R180"/>
<smd name="31" x="-2.0066" y="4.2164" dx="0.508" dy="1.4732" layer="1" rot="R180"/>
<smd name="32" x="-2.794" y="4.2164" dx="0.508" dy="1.4732" layer="1" rot="R180"/>
<wire x1="-1.1938" y1="5.2832" x2="-1.1938" y2="5.9182" width="0.1524" layer="21"/>
<wire x1="-1.9812" y1="-5.8293" x2="-1.9812" y2="-5.2578" width="0.1524" layer="21"/>
<wire x1="5.2832" y1="-0.4064" x2="5.9182" y2="-0.4064" width="0.1524" layer="21"/>
<wire x1="-3.3782" y1="3.6068" x2="-3.6068" y2="3.6068" width="0.1524" layer="21"/>
<wire x1="3.6068" y1="3.3782" x2="3.6068" y2="3.6068" width="0.1524" layer="21"/>
<wire x1="3.3782" y1="-3.6068" x2="3.6068" y2="-3.6068" width="0.1524" layer="21"/>
<wire x1="-3.6068" y1="-3.6068" x2="-3.3782" y2="-3.6068" width="0.1524" layer="21"/>
<wire x1="3.6068" y1="-3.6068" x2="3.6068" y2="-3.3782" width="0.1524" layer="21"/>
<wire x1="3.6068" y1="3.6068" x2="3.3782" y2="3.6068" width="0.1524" layer="21"/>
<wire x1="-3.6068" y1="3.6068" x2="-3.6068" y2="3.3782" width="0.1524" layer="21"/>
<wire x1="-3.6068" y1="-3.3782" x2="-3.6068" y2="-3.6068" width="0.1524" layer="21"/>
<wire x1="2.5654" y1="3.6068" x2="3.0226" y2="3.6068" width="0" layer="51"/>
<wire x1="3.0226" y1="3.6068" x2="3.0226" y2="4.5974" width="0" layer="51"/>
<wire x1="3.0226" y1="4.5974" x2="2.5654" y2="4.5974" width="0" layer="51"/>
<wire x1="2.5654" y1="4.5974" x2="2.5654" y2="3.6068" width="0" layer="51"/>
<wire x1="1.778" y1="3.6068" x2="2.2352" y2="3.6068" width="0" layer="51"/>
<wire x1="2.2352" y1="3.6068" x2="2.2352" y2="4.5974" width="0" layer="51"/>
<wire x1="2.2352" y1="4.5974" x2="1.778" y2="4.5974" width="0" layer="51"/>
<wire x1="1.778" y1="4.5974" x2="1.778" y2="3.6068" width="0" layer="51"/>
<wire x1="0.9652" y1="3.6068" x2="1.4224" y2="3.6068" width="0" layer="51"/>
<wire x1="1.4224" y1="3.6068" x2="1.4224" y2="4.5974" width="0" layer="51"/>
<wire x1="1.4224" y1="4.5974" x2="0.9652" y2="4.5974" width="0" layer="51"/>
<wire x1="0.9652" y1="4.5974" x2="0.9652" y2="3.6068" width="0" layer="51"/>
<wire x1="0.1778" y1="3.6068" x2="0.635" y2="3.6068" width="0" layer="51"/>
<wire x1="0.635" y1="3.6068" x2="0.635" y2="4.5974" width="0" layer="51"/>
<wire x1="0.635" y1="4.5974" x2="0.1778" y2="4.5974" width="0" layer="51"/>
<wire x1="0.1778" y1="4.5974" x2="0.1778" y2="3.6068" width="0" layer="51"/>
<wire x1="-0.635" y1="3.6068" x2="-0.1778" y2="3.6068" width="0" layer="51"/>
<wire x1="-0.1778" y1="3.6068" x2="-0.1778" y2="4.5974" width="0" layer="51"/>
<wire x1="-0.1778" y1="4.5974" x2="-0.635" y2="4.5974" width="0" layer="51"/>
<wire x1="-0.635" y1="4.5974" x2="-0.635" y2="3.6068" width="0" layer="51"/>
<wire x1="-1.4224" y1="3.6068" x2="-0.9652" y2="3.6068" width="0" layer="51"/>
<wire x1="-0.9652" y1="3.6068" x2="-0.9652" y2="4.5974" width="0" layer="51"/>
<wire x1="-0.9652" y1="4.5974" x2="-1.4224" y2="4.5974" width="0" layer="51"/>
<wire x1="-1.4224" y1="4.5974" x2="-1.4224" y2="3.6068" width="0" layer="51"/>
<wire x1="-2.2352" y1="3.6068" x2="-1.778" y2="3.6068" width="0" layer="51"/>
<wire x1="-1.778" y1="3.6068" x2="-1.778" y2="4.5974" width="0" layer="51"/>
<wire x1="-1.778" y1="4.5974" x2="-2.2352" y2="4.5974" width="0" layer="51"/>
<wire x1="-2.2352" y1="4.5974" x2="-2.2352" y2="3.6068" width="0" layer="51"/>
<wire x1="-3.0226" y1="3.6068" x2="-2.5654" y2="3.6068" width="0" layer="51"/>
<wire x1="-2.5654" y1="3.6068" x2="-2.5654" y2="4.5974" width="0" layer="51"/>
<wire x1="-2.5654" y1="4.5974" x2="-3.0226" y2="4.5974" width="0" layer="51"/>
<wire x1="-3.0226" y1="4.5974" x2="-3.0226" y2="3.6068" width="0" layer="51"/>
<wire x1="-3.6068" y1="2.5654" x2="-3.6068" y2="3.0226" width="0" layer="51"/>
<wire x1="-3.6068" y1="3.0226" x2="-4.5974" y2="3.0226" width="0" layer="51"/>
<wire x1="-4.5974" y1="3.0226" x2="-4.5974" y2="2.5654" width="0" layer="51"/>
<wire x1="-4.5974" y1="2.5654" x2="-3.6068" y2="2.5654" width="0" layer="51"/>
<wire x1="-3.6068" y1="1.778" x2="-3.6068" y2="2.2352" width="0" layer="51"/>
<wire x1="-3.6068" y1="2.2352" x2="-4.5974" y2="2.2352" width="0" layer="51"/>
<wire x1="-4.5974" y1="2.2352" x2="-4.5974" y2="1.778" width="0" layer="51"/>
<wire x1="-4.5974" y1="1.778" x2="-3.6068" y2="1.778" width="0" layer="51"/>
<wire x1="-3.6068" y1="0.9652" x2="-3.6068" y2="1.4224" width="0" layer="51"/>
<wire x1="-3.6068" y1="1.4224" x2="-4.5974" y2="1.4224" width="0" layer="51"/>
<wire x1="-4.5974" y1="1.4224" x2="-4.5974" y2="0.9652" width="0" layer="51"/>
<wire x1="-4.5974" y1="0.9652" x2="-3.6068" y2="0.9652" width="0" layer="51"/>
<wire x1="-3.6068" y1="0.1778" x2="-3.6068" y2="0.635" width="0" layer="51"/>
<wire x1="-3.6068" y1="0.635" x2="-4.5974" y2="0.635" width="0" layer="51"/>
<wire x1="-4.5974" y1="0.635" x2="-4.5974" y2="0.1778" width="0" layer="51"/>
<wire x1="-4.5974" y1="0.1778" x2="-3.6068" y2="0.1778" width="0" layer="51"/>
<wire x1="-3.6068" y1="-0.635" x2="-3.6068" y2="-0.1778" width="0" layer="51"/>
<wire x1="-3.6068" y1="-0.1778" x2="-4.5974" y2="-0.1778" width="0" layer="51"/>
<wire x1="-4.5974" y1="-0.1778" x2="-4.5974" y2="-0.635" width="0" layer="51"/>
<wire x1="-4.5974" y1="-0.635" x2="-3.6068" y2="-0.635" width="0" layer="51"/>
<wire x1="-3.6068" y1="-1.4224" x2="-3.6068" y2="-0.9652" width="0" layer="51"/>
<wire x1="-3.6068" y1="-0.9652" x2="-4.5974" y2="-0.9652" width="0" layer="51"/>
<wire x1="-4.5974" y1="-0.9652" x2="-4.5974" y2="-1.4224" width="0" layer="51"/>
<wire x1="-4.5974" y1="-1.4224" x2="-3.6068" y2="-1.4224" width="0" layer="51"/>
<wire x1="-3.6068" y1="-2.2352" x2="-3.6068" y2="-1.778" width="0" layer="51"/>
<wire x1="-3.6068" y1="-1.778" x2="-4.5974" y2="-1.778" width="0" layer="51"/>
<wire x1="-4.5974" y1="-1.778" x2="-4.5974" y2="-2.2352" width="0" layer="51"/>
<wire x1="-4.5974" y1="-2.2352" x2="-3.6068" y2="-2.2352" width="0" layer="51"/>
<wire x1="-3.6068" y1="-3.0226" x2="-3.6068" y2="-2.5654" width="0" layer="51"/>
<wire x1="-3.6068" y1="-2.5654" x2="-4.5974" y2="-2.5654" width="0" layer="51"/>
<wire x1="-4.5974" y1="-2.5654" x2="-4.5974" y2="-3.0226" width="0" layer="51"/>
<wire x1="-4.5974" y1="-3.0226" x2="-3.6068" y2="-3.0226" width="0" layer="51"/>
<wire x1="-2.5654" y1="-3.6068" x2="-3.0226" y2="-3.6068" width="0" layer="51"/>
<wire x1="-3.0226" y1="-3.6068" x2="-3.0226" y2="-4.5974" width="0" layer="51"/>
<wire x1="-3.0226" y1="-4.5974" x2="-2.5654" y2="-4.5974" width="0" layer="51"/>
<wire x1="-2.5654" y1="-4.5974" x2="-2.5654" y2="-3.6068" width="0" layer="51"/>
<wire x1="-1.778" y1="-3.6068" x2="-2.2352" y2="-3.6068" width="0" layer="51"/>
<wire x1="-2.2352" y1="-3.6068" x2="-2.2352" y2="-4.5974" width="0" layer="51"/>
<wire x1="-2.2352" y1="-4.5974" x2="-1.778" y2="-4.5974" width="0" layer="51"/>
<wire x1="-1.778" y1="-4.5974" x2="-1.778" y2="-3.6068" width="0" layer="51"/>
<wire x1="-0.9652" y1="-3.6068" x2="-1.4224" y2="-3.6068" width="0" layer="51"/>
<wire x1="-1.4224" y1="-3.6068" x2="-1.4224" y2="-4.5974" width="0" layer="51"/>
<wire x1="-1.4224" y1="-4.5974" x2="-0.9652" y2="-4.5974" width="0" layer="51"/>
<wire x1="-0.9652" y1="-4.5974" x2="-0.9652" y2="-3.6068" width="0" layer="51"/>
<wire x1="-0.1778" y1="-3.6068" x2="-0.635" y2="-3.6068" width="0" layer="51"/>
<wire x1="-0.635" y1="-3.6068" x2="-0.635" y2="-4.5974" width="0" layer="51"/>
<wire x1="-0.635" y1="-4.5974" x2="-0.1778" y2="-4.5974" width="0" layer="51"/>
<wire x1="-0.1778" y1="-4.5974" x2="-0.1778" y2="-3.6068" width="0" layer="51"/>
<wire x1="0.635" y1="-3.6068" x2="0.1778" y2="-3.6068" width="0" layer="51"/>
<wire x1="0.1778" y1="-3.6068" x2="0.1778" y2="-4.5974" width="0" layer="51"/>
<wire x1="0.1778" y1="-4.5974" x2="0.635" y2="-4.5974" width="0" layer="51"/>
<wire x1="0.635" y1="-4.5974" x2="0.635" y2="-3.6068" width="0" layer="51"/>
<wire x1="1.4224" y1="-3.6068" x2="0.9652" y2="-3.6068" width="0" layer="51"/>
<wire x1="0.9652" y1="-3.6068" x2="0.9652" y2="-4.5974" width="0" layer="51"/>
<wire x1="0.9652" y1="-4.5974" x2="1.4224" y2="-4.5974" width="0" layer="51"/>
<wire x1="1.4224" y1="-4.5974" x2="1.4224" y2="-3.6068" width="0" layer="51"/>
<wire x1="2.2352" y1="-3.6068" x2="1.778" y2="-3.6068" width="0" layer="51"/>
<wire x1="1.778" y1="-3.6068" x2="1.778" y2="-4.5974" width="0" layer="51"/>
<wire x1="1.778" y1="-4.5974" x2="2.2352" y2="-4.5974" width="0" layer="51"/>
<wire x1="2.2352" y1="-4.5974" x2="2.2352" y2="-3.6068" width="0" layer="51"/>
<wire x1="3.0226" y1="-3.6068" x2="2.5654" y2="-3.6068" width="0" layer="51"/>
<wire x1="2.5654" y1="-3.6068" x2="2.5654" y2="-4.5974" width="0" layer="51"/>
<wire x1="2.5654" y1="-4.5974" x2="3.0226" y2="-4.5974" width="0" layer="51"/>
<wire x1="3.0226" y1="-4.5974" x2="3.0226" y2="-3.6068" width="0" layer="51"/>
<wire x1="3.6068" y1="-2.5654" x2="3.6068" y2="-3.0226" width="0" layer="51"/>
<wire x1="3.6068" y1="-3.0226" x2="4.5974" y2="-3.0226" width="0" layer="51"/>
<wire x1="4.5974" y1="-3.0226" x2="4.5974" y2="-2.5654" width="0" layer="51"/>
<wire x1="4.5974" y1="-2.5654" x2="3.6068" y2="-2.5654" width="0" layer="51"/>
<wire x1="3.6068" y1="-1.778" x2="3.6068" y2="-2.2352" width="0" layer="51"/>
<wire x1="3.6068" y1="-2.2352" x2="4.5974" y2="-2.2352" width="0" layer="51"/>
<wire x1="4.5974" y1="-2.2352" x2="4.5974" y2="-1.778" width="0" layer="51"/>
<wire x1="4.5974" y1="-1.778" x2="3.6068" y2="-1.778" width="0" layer="51"/>
<wire x1="3.6068" y1="-0.9652" x2="3.6068" y2="-1.4224" width="0" layer="51"/>
<wire x1="3.6068" y1="-1.4224" x2="4.5974" y2="-1.4224" width="0" layer="51"/>
<wire x1="4.5974" y1="-1.4224" x2="4.5974" y2="-0.9652" width="0" layer="51"/>
<wire x1="4.5974" y1="-0.9652" x2="3.6068" y2="-0.9652" width="0" layer="51"/>
<wire x1="3.6068" y1="-0.1778" x2="3.6068" y2="-0.635" width="0" layer="51"/>
<wire x1="3.6068" y1="-0.635" x2="4.5974" y2="-0.635" width="0" layer="51"/>
<wire x1="4.5974" y1="-0.635" x2="4.5974" y2="-0.1778" width="0" layer="51"/>
<wire x1="4.5974" y1="-0.1778" x2="3.6068" y2="-0.1778" width="0" layer="51"/>
<wire x1="3.6068" y1="0.635" x2="3.6068" y2="0.1778" width="0" layer="51"/>
<wire x1="3.6068" y1="0.1778" x2="4.5974" y2="0.1778" width="0" layer="51"/>
<wire x1="4.5974" y1="0.1778" x2="4.5974" y2="0.635" width="0" layer="51"/>
<wire x1="4.5974" y1="0.635" x2="3.6068" y2="0.635" width="0" layer="51"/>
<wire x1="3.6068" y1="1.4224" x2="3.6068" y2="0.9652" width="0" layer="51"/>
<wire x1="3.6068" y1="0.9652" x2="4.5974" y2="0.9652" width="0" layer="51"/>
<wire x1="4.5974" y1="0.9652" x2="4.5974" y2="1.4224" width="0" layer="51"/>
<wire x1="4.5974" y1="1.4224" x2="3.6068" y2="1.4224" width="0" layer="51"/>
<wire x1="3.6068" y1="2.2352" x2="3.6068" y2="1.778" width="0" layer="51"/>
<wire x1="3.6068" y1="1.778" x2="4.5974" y2="1.778" width="0" layer="51"/>
<wire x1="4.5974" y1="1.778" x2="4.5974" y2="2.2352" width="0" layer="51"/>
<wire x1="4.5974" y1="2.2352" x2="3.6068" y2="2.2352" width="0" layer="51"/>
<wire x1="3.6068" y1="3.0226" x2="3.6068" y2="2.5654" width="0" layer="51"/>
<wire x1="3.6068" y1="2.5654" x2="4.5974" y2="2.5654" width="0" layer="51"/>
<wire x1="4.5974" y1="2.5654" x2="4.5974" y2="3.0226" width="0" layer="51"/>
<wire x1="4.5974" y1="3.0226" x2="3.6068" y2="3.0226" width="0" layer="51"/>
<text x="-3.7846" y="4.699" size="0.6096" layer="25" ratio="10" rot="R180">&gt;NAME</text>
<text x="-3.4544" y="-9.525" size="2.0828" layer="27" ratio="10" rot="SR0">&gt;VALUE</text>
<circle x="-2.6035" y="2.7305" radius="0.3175" width="0.127" layer="21"/>
</package>
<package name="SOIC-16">
<wire x1="-4.9022" y1="-1.7907" x2="4.9022" y2="-1.7907" width="0.1524" layer="21"/>
<wire x1="4.9022" y1="-1.7907" x2="4.9022" y2="1.6637" width="0.1524" layer="21"/>
<wire x1="4.9022" y1="1.6637" x2="-4.9022" y2="1.6637" width="0.1524" layer="21"/>
<wire x1="-4.9022" y1="1.6637" x2="-4.9022" y2="-1.7907" width="0.1524" layer="21"/>
<smd name="1" x="-4.445" y="-2.794" dx="0.6096" dy="1.524" layer="1"/>
<smd name="16" x="-4.445" y="2.667" dx="0.6096" dy="1.524" layer="1"/>
<smd name="2" x="-3.175" y="-2.794" dx="0.6096" dy="1.524" layer="1"/>
<smd name="3" x="-1.905" y="-2.794" dx="0.6096" dy="1.524" layer="1"/>
<smd name="15" x="-3.175" y="2.667" dx="0.6096" dy="1.524" layer="1"/>
<smd name="14" x="-1.905" y="2.667" dx="0.6096" dy="1.524" layer="1"/>
<smd name="4" x="-0.635" y="-2.794" dx="0.6096" dy="1.524" layer="1"/>
<smd name="13" x="-0.635" y="2.667" dx="0.6096" dy="1.524" layer="1"/>
<smd name="5" x="0.635" y="-2.794" dx="0.6096" dy="1.524" layer="1"/>
<smd name="12" x="0.635" y="2.667" dx="0.6096" dy="1.524" layer="1"/>
<smd name="6" x="1.905" y="-2.794" dx="0.6096" dy="1.524" layer="1"/>
<smd name="7" x="3.175" y="-2.794" dx="0.6096" dy="1.524" layer="1"/>
<smd name="11" x="1.905" y="2.667" dx="0.6096" dy="1.524" layer="1"/>
<smd name="10" x="3.175" y="2.667" dx="0.6096" dy="1.524" layer="1"/>
<smd name="8" x="4.445" y="-2.794" dx="0.6096" dy="1.524" layer="1"/>
<smd name="9" x="4.445" y="2.667" dx="0.6096" dy="1.524" layer="1"/>
<text x="-5.3834" y="-1.7152" size="0.508" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<rectangle x1="4.2418" y1="1.7526" x2="4.6482" y2="2.7686" layer="51"/>
<rectangle x1="2.9718" y1="1.7526" x2="3.3782" y2="2.7686" layer="51"/>
<rectangle x1="1.7018" y1="1.7526" x2="2.1082" y2="2.7686" layer="51"/>
<rectangle x1="0.4318" y1="1.7526" x2="0.8382" y2="2.7686" layer="51"/>
<rectangle x1="-0.8382" y1="1.7526" x2="-0.4318" y2="2.7686" layer="51"/>
<rectangle x1="-2.1082" y1="1.7526" x2="-1.7018" y2="2.7686" layer="51"/>
<rectangle x1="-3.3782" y1="1.7526" x2="-2.9718" y2="2.7686" layer="51"/>
<rectangle x1="-4.6482" y1="1.7526" x2="-4.2418" y2="2.7686" layer="51"/>
<rectangle x1="-4.6482" y1="-2.8956" x2="-4.2418" y2="-1.8796" layer="51"/>
<rectangle x1="-3.3782" y1="-2.8956" x2="-2.9718" y2="-1.8796" layer="51"/>
<rectangle x1="-2.1082" y1="-2.8956" x2="-1.7018" y2="-1.8796" layer="51"/>
<rectangle x1="-0.8382" y1="-2.8956" x2="-0.4318" y2="-1.8796" layer="51"/>
<rectangle x1="0.4318" y1="-2.8956" x2="0.8382" y2="-1.8796" layer="51"/>
<rectangle x1="1.7018" y1="-2.8956" x2="2.1082" y2="-1.8796" layer="51"/>
<rectangle x1="2.9718" y1="-2.8956" x2="3.3782" y2="-1.8796" layer="51"/>
<rectangle x1="4.2418" y1="-2.8956" x2="4.6482" y2="-1.8796" layer="51"/>
<circle x="-4.1275" y="-1.016" radius="0.3175" width="0.127" layer="21"/>
</package>
<package name="USB-A_STACK">
<hole x="-6.35" y="5.68" drill="2.8"/>
<hole x="6.78941875" y="5.68" drill="2.8"/>
<pad name="T1" x="-3.28168125" y="5.330003125" drill="1" shape="square"/>
<pad name="B1" x="-3.28168125" y="2.710003125" drill="1" shape="square"/>
<pad name="T2" x="-0.78231875" y="5.330003125" drill="1"/>
<pad name="T3" x="1.217675" y="5.330003125" drill="1"/>
<pad name="T4" x="3.71754375" y="5.330003125" drill="1"/>
<pad name="B2" x="-0.78231875" y="2.710003125" drill="1"/>
<pad name="B3" x="1.217675" y="2.710003125" drill="1"/>
<pad name="B4" x="3.71754375" y="2.710003125" drill="1"/>
<pad name="SH1" x="-6.35" y="0" drill="2.8"/>
<pad name="SH2" x="6.78941875" y="0" drill="2.8"/>
<rectangle x1="-3.81" y1="-2.54" x2="4.445" y2="1.27" layer="39"/>
<rectangle x1="-4.445" y1="6.985" x2="5.08" y2="8.89" layer="39"/>
<wire x1="-5.08" y1="7.62" x2="5.08" y2="7.62" width="0.127" layer="21"/>
<wire x1="-6.35" y1="3.7465" x2="-6.35" y2="2.54" width="0.127" layer="21"/>
<wire x1="6.858" y1="3.7465" x2="6.858" y2="2.6035" width="0.127" layer="21"/>
<wire x1="-6.35" y1="0" x2="-6.35" y2="-10.287" width="0.127" layer="49"/>
<wire x1="-6.35" y1="-10.287" x2="6.7945" y2="-10.287" width="0.127" layer="49"/>
<wire x1="6.7945" y1="-10.287" x2="6.7945" y2="0" width="0.127" layer="49"/>
</package>
<package name="TSSOP-8_5.8MM">
<wire x1="-1.51765" y1="1.46685" x2="-1.51765" y2="-1.532890625" width="0.0508" layer="51"/>
<wire x1="2.752090625" y1="1.46685" x2="2.752090625" y2="-1.532890625" width="0.0508" layer="51"/>
<wire x1="-1.51765" y1="-1.532890625" x2="2.752090625" y2="-1.532890625" width="0.0508" layer="21"/>
<wire x1="-1.51765" y1="1.46685" x2="2.752090625" y2="1.46685" width="0.0508" layer="21"/>
<smd name="1" x="-2.21615" y="0.93345" dx="1.4478" dy="0.44958125" layer="1"/>
<smd name="2" x="-2.21615" y="0.283209375" dx="1.4478" dy="0.44958125" layer="1"/>
<smd name="3" x="-2.21615" y="-0.36703125" dx="1.4478" dy="0.44958125" layer="1"/>
<smd name="4" x="-2.21615" y="-1.01726875" dx="1.4478" dy="0.44958125" layer="1"/>
<smd name="8" x="3.583940625" y="0.93345" dx="1.4478" dy="0.44958125" layer="1"/>
<smd name="7" x="3.583940625" y="0.283209375" dx="1.4478" dy="0.44958125" layer="1"/>
<smd name="6" x="3.583940625" y="-0.36703125" dx="1.4478" dy="0.44958125" layer="1"/>
<smd name="5" x="3.583940625" y="-1.01726875" dx="1.4478" dy="0.44958125" layer="1"/>
<circle x="-0.97155" y="1.00965" radius="0.202603125" width="0.127" layer="21"/>
<rectangle x1="-2.51586875" y1="-0.5405125" x2="-1.51765" y2="-0.1905" layer="51" rot="R180"/>
<rectangle x1="-2.52221875" y1="-1.1945625" x2="-1.524" y2="-0.84455" layer="51" rot="R180"/>
<rectangle x1="2.76098125" y1="-1.2009125" x2="3.7592" y2="-0.8509" layer="51" rot="R180"/>
<rectangle x1="2.75463125" y1="-0.5468625" x2="3.75285" y2="-0.19685" layer="51" rot="R180"/>
<rectangle x1="-2.51586875" y1="0.1071875" x2="-1.51765" y2="0.4572" layer="51" rot="R180"/>
<rectangle x1="-2.50951875" y1="0.7548875" x2="-1.5113" y2="1.1049" layer="51" rot="R180"/>
<rectangle x1="2.76098125" y1="0.7612375" x2="3.7592" y2="1.11125" layer="51" rot="R180"/>
<rectangle x1="2.75463125" y1="0.1071875" x2="3.75285" y2="0.4572" layer="51" rot="R180"/>
<text x="-1.1557" y="1.8034" size="0.4064" layer="25">&gt;NAME</text>
</package>
<package name="SOT23-5">
<wire x1="1.422" y1="-0.81" x2="-1.422" y2="-0.81" width="0.1524" layer="51"/>
<wire x1="-1.422" y1="-0.81" x2="-1.422" y2="0.81" width="0.1524" layer="21"/>
<wire x1="-1.422" y1="0.81" x2="1.422" y2="0.81" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="-1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="2" x="0" y="-1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="3" x="0.95" y="-1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="4" x="0.95" y="1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="5" x="-0.95" y="1.3" dx="0.55" dy="1.2" layer="1"/>
<rectangle x1="-1.2" y1="-1.5" x2="-0.7" y2="-0.85" layer="51"/>
<rectangle x1="-0.25" y1="-1.5" x2="0.25" y2="-0.85" layer="51"/>
<rectangle x1="0.7" y1="0.85" x2="1.2" y2="1.5" layer="51"/>
<rectangle x1="-1.2" y1="0.85" x2="-0.7" y2="1.5" layer="51"/>
<rectangle x1="0.7" y1="-1.5" x2="1.2" y2="-0.85" layer="51"/>
<wire x1="1.422" y1="0.81" x2="1.422" y2="-0.81" width="0.1524" layer="21"/>
<text x="-1.905" y="-1.27" size="0.4064" layer="25" rot="R90">&gt;NAME</text>
</package>
<package name="153CLV-1012_6.6X6.6">
<description>Aluminum Electrolytic Capacitor, 6.6mm x 6.6mm</description>
<wire x1="2.522" y1="-3.168" x2="-3.168" y2="-3.168" width="0.2032" layer="21"/>
<wire x1="-3.168" y1="-3.168" x2="-3.168" y2="3.453" width="0.2032" layer="51"/>
<wire x1="-3.168" y1="3.429" x2="2.522" y2="3.429" width="0.2032" layer="21"/>
<wire x1="3.429" y1="2.522" x2="3.429" y2="1.1825" width="0.2032" layer="21"/>
<wire x1="3.422" y1="2.262" x2="3.422" y2="-2.268" width="0.2032" layer="51"/>
<wire x1="3.422" y1="-2.268" x2="2.522" y2="-3.168" width="0.2032" layer="21"/>
<wire x1="2.522" y1="3.429" x2="3.429" y2="2.522" width="0.2032" layer="21"/>
<wire x1="2.872" y1="-0.788" x2="2.8085" y2="1.169" width="0.2032" layer="51" curve="36.869898"/>
<wire x1="-2.618" y1="-0.788" x2="-2.5545" y2="1.169" width="0.2032" layer="51" curve="-36.869898"/>
<wire x1="-2.618" y1="-0.788" x2="2.872" y2="-0.788" width="0.2032" layer="21" curve="143.130102"/>
<wire x1="-2.5545" y1="1.169" x2="2.8085" y2="1.169" width="0.2032" layer="21" curve="-143.130102"/>
<smd name="-" x="-2.6895" y="0.1905" dx="3.81" dy="1.524" layer="1"/>
<text x="-3.1945" y="3.8155" size="0.508" layer="25">&gt;NAME</text>
<text x="-3.293" y="-3.9275" size="0.508" layer="27">&gt;VALUE</text>
<smd name="+" x="2.7715" y="0.1905" dx="3.81" dy="1.524" layer="1"/>
<wire x1="-1.143" y1="-2.4695" x2="-1.143" y2="2.7125" width="0.2032" layer="51"/>
<wire x1="4.3815" y1="1.2135" x2="4.3815" y2="2.5855" width="0.2032" layer="21"/>
<wire x1="5.073" y1="1.905" x2="3.701" y2="1.905" width="0.2032" layer="21"/>
<wire x1="3.429" y1="-0.907" x2="3.429" y2="-2.2465" width="0.2032" layer="21"/>
<wire x1="-3.175" y1="-0.907" x2="-3.175" y2="-3.1355" width="0.2032" layer="21"/>
<wire x1="-3.175" y1="3.411" x2="-3.175" y2="1.1825" width="0.2032" layer="21"/>
</package>
<package name="153CLV-1012_4.3X4.3">
<description>Aluminum Electrolytic Capacitor, 4.3mm x 4.3mm</description>
<wire x1="1.125" y1="-2.0885" x2="-2.025" y2="-2.0885" width="0.2032" layer="21"/>
<wire x1="-2.025" y1="-2.0885" x2="-2.025" y2="2.1971" width="0.2032" layer="51"/>
<wire x1="-2.025" y1="2.1971" x2="1.125" y2="2.1971" width="0.2032" layer="21"/>
<wire x1="2.2606" y1="1.0615" x2="2.2606" y2="-1.1885" width="0.2032" layer="51"/>
<wire x1="2.2606" y1="-1.1885" x2="1.125" y2="-2.0885" width="0.2032" layer="21"/>
<wire x1="1.125" y1="2.1971" x2="2.2606" y2="1.0615" width="0.2032" layer="21"/>
<wire x1="1.475" y1="-0.534" x2="1.475" y2="0.8515" width="0.2032" layer="51" curve="36.869898"/>
<wire x1="-1.475" y1="-0.4705" x2="-1.475" y2="0.8515" width="0.2032" layer="51" curve="-36.869898"/>
<wire x1="-1.475" y1="-0.534" x2="1.475" y2="-0.534" width="0.2032" layer="21" curve="143.130102"/>
<wire x1="-1.475" y1="0.8515" x2="1.475" y2="0.8515" width="0.2032" layer="21" curve="-143.130102"/>
<smd name="-" x="-1.864" y="0" dx="2.54" dy="1.524" layer="1"/>
<text x="-2.0515" y="2.4185" size="0.508" layer="25">&gt;NAME</text>
<smd name="+" x="1.8825" y="0" dx="2.54" dy="1.524" layer="1"/>
<wire x1="-0.762" y1="-1.39" x2="-0.762" y2="1.6965" width="0.2032" layer="21"/>
<wire x1="2.6035" y1="1.15" x2="2.6035" y2="2.522" width="0.2032" layer="21"/>
<wire x1="3.295" y1="1.8415" x2="1.923" y2="1.8415" width="0.2032" layer="21"/>
</package>
<package name="153CLV-1012_5.3X5.3">
<wire x1="1.887" y1="-2.8505" x2="-2.533" y2="-2.8505" width="0.2032" layer="21"/>
<wire x1="-2.533" y1="-2.8505" x2="-2.533" y2="2.432734375" width="0.2032" layer="51"/>
<wire x1="-2.533" y1="2.432734375" x2="1.887" y2="2.432734375" width="0.2032" layer="21"/>
<wire x1="2.7432" y1="1.5695" x2="2.7432" y2="-1.9505" width="0.2032" layer="51"/>
<wire x1="2.7432" y1="-1.9505" x2="1.887" y2="-2.8505" width="0.2032" layer="21"/>
<wire x1="1.887" y1="2.432734375" x2="2.7432" y2="1.5695" width="0.2032" layer="21"/>
<smd name="-" x="-2.0545" y="-0.0635" dx="2.54" dy="1.524" layer="1"/>
<smd name="+" x="2.1365" y="0" dx="2.54" dy="1.524" layer="1"/>
<wire x1="3.6195" y1="1.277" x2="3.6195" y2="2.014" width="0.2032" layer="21"/>
<wire x1="3.9935" y1="1.651" x2="3.193" y2="1.651" width="0.2032" layer="21"/>
<rectangle x1="1.68783125" y1="-1.15696875" x2="2.33553125" y2="1.04266875" layer="51" rot="R90"/>
<rectangle x1="-2.12216875" y1="-1.15696875" x2="-1.47446875" y2="1.04266875" layer="51" rot="R90"/>
<text x="2.032" y="-3.302" size="0.4064" layer="25">&gt;NAME</text>
</package>
<package name="SMD-2312(6032)">
<description>Covers 6032 metric packages - 2312, 2413</description>
<wire x1="-2.8" y1="1.55" x2="2.8" y2="1.55" width="0.1016" layer="21"/>
<wire x1="2.8" y1="1.55" x2="2.8" y2="-1.55" width="0.1016" layer="51"/>
<wire x1="2.8" y1="-1.55" x2="-2.8" y2="-1.55" width="0.1016" layer="21"/>
<wire x1="-2.8" y1="-1.55" x2="-2.8" y2="1.55" width="0.1016" layer="51"/>
<smd name="+" x="2.652" y="0" dx="1.905" dy="2.5" layer="1"/>
<smd name="-" x="-2.652" y="0" dx="1.905" dy="2.5" layer="1"/>
<rectangle x1="-3" y1="-1.1" x2="-2.85" y2="1.1" layer="51"/>
<rectangle x1="2.85" y1="-1.1" x2="3" y2="1.1" layer="51"/>
<rectangle x1="1.95" y1="-1.575" x2="2.45" y2="1.575" layer="51"/>
<wire x1="3.1115" y1="2.286" x2="3.1115" y2="1.651" width="0.127" layer="21"/>
<wire x1="2.7305" y1="1.9685" x2="3.429" y2="1.9685" width="0.127" layer="21"/>
<text x="-2.54" y="1.905" size="0.4064" layer="25">&gt;NAME</text>
<wire x1="-3.8112" y1="1.67185" x2="3.80485" y2="1.67185" width="0.0508" layer="39"/>
<wire x1="-3.8112" y1="-1.6492" x2="3.80485" y2="-1.6492" width="0.0508" layer="39"/>
<wire x1="3.8082" y1="-1.64585" x2="3.8082" y2="1.67125" width="0.0508" layer="39"/>
<wire x1="-3.8118" y1="-1.64585" x2="-3.8118" y2="1.67125" width="0.0508" layer="39"/>
</package>
<package name="SMD-1206">
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<text x="2.5781" y="-0.127" size="0.4064" layer="25">&gt;NAME</text>
<wire x1="-2.3876" y1="1.08585" x2="-2.3876" y2="-1.09855" width="0.127" layer="21"/>
<wire x1="-2.3876" y1="-1.09855" x2="2.39395" y2="-1.09855" width="0.127" layer="21"/>
<wire x1="2.39395" y1="-1.09855" x2="2.39395" y2="1.08585" width="0.127" layer="21"/>
<wire x1="2.39395" y1="1.08585" x2="-2.3876" y2="1.08585" width="0.127" layer="21"/>
</package>
<package name="SMD-1411_POL">
<smd name="1" x="-1.9685" y="0" dx="2.3495" dy="2.2098" layer="1" rot="R90"/>
<smd name="2" x="2.0955" y="0" dx="2.3495" dy="2.2098" layer="1" rot="R90"/>
<wire x1="-3.27025" y1="1.96215" x2="3.40995" y2="1.9685" width="0.127" layer="21"/>
<wire x1="3.40995" y1="1.9685" x2="3.41121875" y2="1.9685" width="0.127" layer="21"/>
<wire x1="-3.27025" y1="-2.03835" x2="-3.27025" y2="1.96215" width="0.127" layer="21"/>
<wire x1="3.40995" y1="-2.02565" x2="3.40995" y2="1.9685" width="0.127" layer="21"/>
<wire x1="-3.27025" y1="-2.03835" x2="3.40486875" y2="-2.032" width="0.127" layer="21"/>
<text x="-3.048" y="2.2225" size="0.6096" layer="25">&gt;NAME</text>
<wire x1="-0.3175" y1="1.4605" x2="-0.3175" y2="-1.4605" width="0.127" layer="21"/>
<wire x1="0.762" y1="-1.143" x2="0.762" y2="1.143" width="0.127" layer="21" curve="-137.726311"/>
<wire x1="-3.2524" y1="1.983" x2="3.41115" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-3.2524" y1="-2.0175" x2="3.41115" y2="-2.0175" width="0.0508" layer="39"/>
<wire x1="-3.253" y1="-2.0205" x2="-3.253" y2="1.9824" width="0.0508" layer="39"/>
<wire x1="3.4145" y1="-2.0205" x2="3.4145" y2="1.9824" width="0.0508" layer="39"/>
</package>
<package name="SMD-2917">
<smd name="+" x="-3.81" y="0" dx="1.524" dy="2.54" layer="1" rot="R180"/>
<smd name="-" x="3.4798" y="0" dx="1.524" dy="2.54" layer="1" rot="R180"/>
<wire x1="-3.81" y1="2.159" x2="3.4798" y2="2.159" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-2.159" x2="-3.81" y2="2.159" width="0.127" layer="51"/>
<wire x1="3.4798" y1="-2.159" x2="3.4798" y2="2.159" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-2.159" x2="3.4798" y2="-2.159" width="0.127" layer="51"/>
<wire x1="-3.81" y1="2.159" x2="3.4798" y2="2.159" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-2.159" x2="3.4798" y2="-2.159" width="0.127" layer="21"/>
<wire x1="-5.3975" y1="2.286" x2="-4.1275" y2="2.286" width="0.127" layer="21"/>
<wire x1="-4.7625" y1="1.651" x2="-4.7625" y2="2.921" width="0.127" layer="21"/>
<wire x1="-2.54" y1="0" x2="1.905" y2="0" width="0.127" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0.635" y1="1.27" x2="0.635" y2="-1.27" width="0.127" layer="21" curve="180"/>
<text x="-2.54" y="2.667" size="0.6096" layer="21">&gt;NAME</text>
</package>
<package name="SMD-0402">
<wire x1="-0.252" y1="0.2148" x2="0.238" y2="0.2148" width="0.1524" layer="51"/>
<wire x1="0.238" y1="-0.2332" x2="-0.252" y2="-0.2332" width="0.1524" layer="51"/>
<smd name="1" x="-0.5935" y="-0.0092" dx="0.508" dy="0.762" layer="1"/>
<smd name="2" x="0.5795" y="-0.0092" dx="0.508" dy="0.762" layer="1"/>
<rectangle x1="-0.561" y1="-0.314" x2="-0.261" y2="0.2859" layer="51"/>
<rectangle x1="0.2518" y1="-0.314" x2="0.5518" y2="0.2859" layer="51"/>
<text x="1.27" y="-0.03175" size="0.508" layer="25">&gt;NAME</text>
<wire x1="1.04775" y1="0.5715" x2="1.04775" y2="-0.60325" width="0.127" layer="21"/>
<wire x1="1.04775" y1="-0.60325" x2="-1.04775" y2="-0.60325" width="0.127" layer="21"/>
<wire x1="-1.04775" y1="-0.60325" x2="-1.04775" y2="0.5715" width="0.127" layer="21"/>
<wire x1="-1.04775" y1="0.5715" x2="1.04775" y2="0.5715" width="0.127" layer="21"/>
</package>
<package name="SMD-0805">
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
<text x="1.89865" y="-0.0254" size="0.4064" layer="25">&gt;NAME</text>
<wire x1="1.7145" y1="0.9525" x2="1.7145" y2="-0.9525" width="0.127" layer="21"/>
<wire x1="1.7145" y1="-0.9525" x2="-1.7145" y2="-0.9525" width="0.127" layer="21"/>
<wire x1="-1.7145" y1="-0.9525" x2="-1.7145" y2="0.9525" width="0.127" layer="21"/>
<wire x1="-1.7145" y1="0.9525" x2="1.7145" y2="0.9525" width="0.127" layer="21"/>
</package>
<package name="SMD-2512">
<description>SMD Cap/Res 2512 Package</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="1.473" x2="1.498" y2="1.473" width="0.1524" layer="21"/>
<wire x1="-1.473" y1="-1.473" x2="1.498" y2="-1.473" width="0.1524" layer="21"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="4.01955" y="-0.10795" size="0.6096" layer="25">&gt;NAME</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
<wire x1="-3.86835" y1="1.82425" x2="3.8844" y2="1.82425" width="0.0508" layer="39"/>
<wire x1="-3.86835" y1="-1.827" x2="3.8844" y2="-1.827" width="0.0508" layer="39"/>
<wire x1="-3.86895" y1="-1.8173" x2="-3.86895" y2="1.82365" width="0.0508" layer="39"/>
<wire x1="3.8844" y1="-1.827" x2="3.8844" y2="1.82425" width="0.0508" layer="39"/>
<wire x1="-3.8989" y1="1.8288" x2="-3.8989" y2="-1.83515" width="0.127" layer="21"/>
<wire x1="-3.8989" y1="-1.83515" x2="3.93065" y2="-1.83515" width="0.127" layer="21"/>
<wire x1="3.93065" y1="-1.83515" x2="3.93065" y2="1.83515" width="0.127" layer="21"/>
<wire x1="3.93065" y1="1.83515" x2="-3.8989" y2="1.8288" width="0.127" layer="21"/>
</package>
<package name="SMD-0603">
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<smd name="1" x="-0.69125" y="0" dx="0.762" dy="1.1" layer="1"/>
<smd name="2" x="0.69125" y="0" dx="0.762" dy="1.1" layer="1"/>
<text x="1.3335" y="0.0254" size="0.254" layer="25">&gt;NAME</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<wire x1="-1.27" y1="0.762" x2="-1.27" y2="-0.762" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-0.762" x2="1.27" y2="-0.762" width="0.127" layer="21"/>
<wire x1="1.27" y1="-0.762" x2="1.27" y2="0.762" width="0.127" layer="21"/>
<wire x1="1.27" y1="0.762" x2="-1.27" y2="0.762" width="0.127" layer="21"/>
</package>
<package name="SMD-1812">
<wire x1="-3.0619" y1="1.86055" x2="3.0746" y2="1.86055" width="0.0508" layer="39"/>
<wire x1="3.08095" y1="-1.8814" x2="-3.05555" y2="-1.8814" width="0.0508" layer="39"/>
<wire x1="-3.05555" y1="-1.8814" x2="-3.0619" y2="1.86055" width="0.0508" layer="39"/>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="3.0746" y1="1.86055" x2="3.08095" y2="-1.8814" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<text x="-2.413" y="2.159" size="0.4064" layer="25">&gt;NAME</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.3" y1="-0.4001" x2="0.3" y2="0.4001" layer="35"/>
<wire x1="-3.10515" y1="1.905" x2="-3.10515" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-3.10515" y1="-1.905" x2="3.13055" y2="-1.905" width="0.127" layer="21"/>
<wire x1="3.13055" y1="-1.905" x2="3.13055" y2="1.905" width="0.127" layer="21"/>
<wire x1="3.13055" y1="1.905" x2="-3.10515" y2="1.905" width="0.127" layer="21"/>
</package>
<package name="CAP_TH">
<description>0.886" pitch</description>
<pad name="1" x="0" y="0" drill="0.8"/>
<pad name="2" x="22.5044" y="0" drill="0.8"/>
<wire x1="-1.27" y1="2.54" x2="24.13" y2="2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="24.13" y2="-2.54" width="0.127" layer="21"/>
<wire x1="24.13" y1="-2.54" x2="24.13" y2="2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="2.54" width="0.127" layer="21"/>
<text x="-0.635" y="3.175" size="0.4064" layer="25">&gt;NAME</text>
</package>
<package name="SMD-2010">
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.2825" y1="1.5465" x2="3.2825" y2="1.5465" width="0.0508" layer="39"/>
<wire x1="3.2825" y1="1.5465" x2="3.2825" y2="-1.5465" width="0.0508" layer="39"/>
<wire x1="3.2825" y1="-1.5465" x2="-3.2825" y2="-1.5465" width="0.0508" layer="39"/>
<wire x1="-3.2825" y1="-1.5465" x2="-3.2825" y2="1.5465" width="0.0508" layer="39"/>
<wire x1="-1.027" y1="1.245" x2="1.027" y2="1.245" width="0.1524" layer="21"/>
<wire x1="-1.002" y1="-1.245" x2="1.016" y2="-1.245" width="0.1524" layer="21"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-2.159" y="1.651" size="0.4064" layer="25">&gt;NAME</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<wire x1="-3.2893" y1="1.5494" x2="-3.2893" y2="-1.5494" width="0.127" layer="21"/>
<wire x1="-3.2893" y1="-1.5494" x2="3.28295" y2="-1.5494" width="0.127" layer="21"/>
<wire x1="3.28295" y1="-1.5494" x2="3.28295" y2="1.5494" width="0.127" layer="21"/>
<wire x1="3.28295" y1="1.5494" x2="-3.2893" y2="1.5494" width="0.127" layer="21"/>
</package>
<package name="RES_TH">
<description>Resistor Through-hole (Tall)</description>
<pad name="1" x="-1.905" y="0" drill="0.8"/>
<pad name="2" x="1.5875" y="0" drill="0.8"/>
<circle x="-1.905" y="0" radius="1.27" width="0.127" layer="21"/>
<wire x1="-0.635" y1="0" x2="0.635" y2="0" width="0.127" layer="21"/>
<text x="-0.635" y="1.27" size="0.4064" layer="21">&gt;NAME</text>
</package>
<package name="XTAL_7XC-7A">
<smd name="2" x="1.85" y="0" dx="1.7" dy="2.4" layer="1"/>
<smd name="1" x="-1.85" y="0" dx="1.7" dy="2.4" layer="1"/>
<wire x1="-2.6" y1="1.6" x2="2.6" y2="1.6" width="0.127" layer="21"/>
<wire x1="2.6" y1="-1.6" x2="-2.6" y2="-1.6" width="0.127" layer="21"/>
<text x="-2.286" y="2.159" size="0.4064" layer="25">&gt;NAME</text>
</package>
<package name="XTAL_HC49UP">
<wire x1="-5.1091" y1="1.143" x2="-3.429" y2="2.0321" width="0.0508" layer="21" curve="-55.770993" cap="flat"/>
<wire x1="-5.715" y1="1.143" x2="-5.715" y2="2.159" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.032" x2="5.1091" y2="1.143" width="0.0508" layer="21" curve="-55.772485" cap="flat"/>
<wire x1="5.715" y1="1.143" x2="5.715" y2="2.159" width="0.1524" layer="21"/>
<wire x1="3.429" y1="-1.27" x2="-3.429" y2="-1.27" width="0.0508" layer="21"/>
<wire x1="3.429" y1="-2.032" x2="-3.429" y2="-2.032" width="0.0508" layer="21"/>
<wire x1="-3.429" y1="1.27" x2="3.429" y2="1.27" width="0.0508" layer="21"/>
<wire x1="5.461" y1="-2.413" x2="-5.461" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-0.381" x2="6.477" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="5.715" y1="0.381" x2="6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="6.477" y1="-0.381" x2="6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="5.461" y1="-2.413" x2="5.715" y2="-2.159" width="0.1524" layer="21" curve="90"/>
<wire x1="5.715" y1="-1.143" x2="5.715" y2="1.143" width="0.1524" layer="51"/>
<wire x1="5.715" y1="-2.159" x2="5.715" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.429" y1="-1.27" x2="3.9826" y2="-1.143" width="0.0508" layer="21" curve="25.842828" cap="flat"/>
<wire x1="3.429" y1="1.27" x2="3.9826" y2="1.143" width="0.0508" layer="21" curve="-25.842828" cap="flat"/>
<wire x1="3.429" y1="-2.032" x2="5.109" y2="-1.1429" width="0.0508" layer="21" curve="55.771157" cap="flat"/>
<wire x1="3.9826" y1="-1.143" x2="3.9826" y2="1.143" width="0.0508" layer="51" curve="128.314524" cap="flat"/>
<wire x1="5.1091" y1="-1.143" x2="5.1091" y2="1.143" width="0.0508" layer="51" curve="68.456213" cap="flat"/>
<wire x1="-5.1091" y1="-1.143" x2="-3.429" y2="-2.032" width="0.0508" layer="21" curve="55.772485" cap="flat"/>
<wire x1="-3.9826" y1="-1.143" x2="-3.9826" y2="1.143" width="0.0508" layer="51" curve="-128.314524" cap="flat"/>
<wire x1="-3.9826" y1="-1.143" x2="-3.429" y2="-1.27" width="0.0508" layer="21" curve="25.842828" cap="flat"/>
<wire x1="-3.9826" y1="1.143" x2="-3.429" y2="1.27" width="0.0508" layer="21" curve="-25.842828" cap="flat"/>
<wire x1="-6.477" y1="-0.381" x2="-6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-5.1091" y1="-1.143" x2="-5.1091" y2="1.143" width="0.0508" layer="51" curve="-68.456213" cap="flat"/>
<wire x1="-5.715" y1="-1.143" x2="-5.715" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="-0.381" x2="-5.715" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="0.381" x2="-5.715" y2="1.143" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="-2.159" x2="-5.715" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-2.159" x2="-5.461" y2="-2.413" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.715" y1="-0.381" x2="-6.477" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="0.381" x2="-6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-3.429" y1="2.032" x2="3.429" y2="2.032" width="0.0508" layer="21"/>
<wire x1="5.461" y1="2.413" x2="-5.461" y2="2.413" width="0.1524" layer="21"/>
<wire x1="5.461" y1="2.413" x2="5.715" y2="2.159" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.715" y1="2.159" x2="-5.461" y2="2.413" width="0.1524" layer="21" curve="-90"/>
<wire x1="-0.254" y1="0.635" x2="-0.254" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-0.635" x2="0.254" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-0.635" x2="0.254" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.635" x2="-0.254" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0.635" x2="-0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-1.016" y2="0" width="0.0508" layer="21"/>
<wire x1="0.635" y1="0.635" x2="0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.016" y2="0" width="0.0508" layer="21"/>
<smd name="1" x="-4.826" y="0" dx="5.334" dy="1.9304" layer="1"/>
<smd name="2" x="4.826" y="0" dx="5.334" dy="1.9304" layer="1"/>
<text x="-6.35" y="3.429" size="0.508" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.35" y="-4.191" size="0.508" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-6.604" y1="-3.048" x2="6.604" y2="3.048" layer="43"/>
</package>
<package name="XTAL_HC49US">
<wire x1="-3.429" y1="-2.286" x2="3.429" y2="-2.286" width="0.2032" layer="21"/>
<wire x1="3.429" y1="2.286" x2="-3.429" y2="2.286" width="0.2032" layer="21"/>
<wire x1="3.429" y1="2.286" x2="3.429" y2="-2.286" width="0.2032" layer="21" curve="-180"/>
<wire x1="-3.429" y1="2.286" x2="-3.429" y2="-2.286" width="0.2032" layer="21" curve="180"/>
<pad name="1" x="-2.54" y="0" drill="0.7" diameter="1.651" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="0.7" diameter="1.651" rot="R90"/>
<text x="-5.08" y="-3.302" size="0.508" layer="27" ratio="10">&gt;VALUE</text>
<text x="-5.08" y="2.667" size="0.508" layer="25" ratio="10">&gt;NAME</text>
</package>
<package name="XTAL_HC49UV">
<wire x1="-2.921" y1="-2.286" x2="2.921" y2="-2.286" width="0.4064" layer="21"/>
<wire x1="-2.921" y1="2.286" x2="2.921" y2="2.286" width="0.4064" layer="21"/>
<wire x1="-2.921" y1="-1.778" x2="2.921" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.778" x2="-2.921" y2="1.778" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.778" x2="2.921" y2="-1.778" width="0.1524" layer="21" curve="-180"/>
<wire x1="2.921" y1="2.286" x2="2.921" y2="-2.286" width="0.4064" layer="21" curve="-180"/>
<wire x1="-2.921" y1="2.286" x2="-2.921" y2="-2.286" width="0.4064" layer="21" curve="180"/>
<wire x1="-2.921" y1="1.778" x2="-2.921" y2="-1.778" width="0.1524" layer="21" curve="180"/>
<wire x1="-0.254" y1="0.889" x2="0.254" y2="0.889" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.889" x2="0.254" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-0.889" x2="-0.254" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-0.889" x2="-0.254" y2="0.889" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0.889" x2="0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0.889" x2="-0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.27" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-1.27" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-2.413" y="0" drill="0.8128"/>
<pad name="2" x="2.413" y="0" drill="0.8128"/>
<text x="-5.08" y="2.921" size="0.508" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-3.556" size="0.508" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.81" y1="-2.794" x2="3.81" y2="2.794" layer="43"/>
<rectangle x1="-4.318" y1="-2.54" x2="-3.81" y2="2.54" layer="43"/>
<rectangle x1="-4.826" y1="-2.286" x2="-4.318" y2="2.286" layer="43"/>
<rectangle x1="-5.334" y1="-1.778" x2="-4.826" y2="1.778" layer="43"/>
<rectangle x1="-5.588" y1="-1.27" x2="-5.334" y2="1.016" layer="43"/>
<rectangle x1="3.81" y1="-2.54" x2="4.318" y2="2.54" layer="43"/>
<rectangle x1="4.318" y1="-2.286" x2="4.826" y2="2.286" layer="43"/>
<rectangle x1="4.826" y1="-1.778" x2="5.334" y2="1.778" layer="43"/>
<rectangle x1="5.334" y1="-1.016" x2="5.588" y2="1.016" layer="43"/>
</package>
<package name="XTAL_SMD_10.5X4.8">
<wire x1="-6.2" y1="1.5" x2="-6.2" y2="2.4" width="0.127" layer="21"/>
<wire x1="-6.2" y1="2.4" x2="6.2" y2="2.4" width="0.127" layer="21"/>
<wire x1="6.2" y1="2.4" x2="6.2" y2="1.5" width="0.127" layer="21"/>
<wire x1="-6.2" y1="-1.5" x2="-6.2" y2="-2.4" width="0.127" layer="21"/>
<wire x1="-6.2" y1="-2.4" x2="6.2" y2="-2.4" width="0.127" layer="21"/>
<wire x1="6.2" y1="-2.4" x2="6.2" y2="-1.5" width="0.127" layer="21"/>
<smd name="1" x="-4.3" y="0" dx="5.5" dy="1.5" layer="1"/>
<smd name="2" x="4.3" y="0" dx="5.5" dy="1.5" layer="1"/>
</package>
<package name="XTAL_TC26H">
<wire x1="-0.889" y1="-0.889" x2="0.889" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="0.762" y1="5.207" x2="1.016" y2="4.953" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.016" y1="4.953" x2="-0.762" y2="5.207" width="0.1524" layer="21" curve="-90"/>
<wire x1="-0.762" y1="5.207" x2="0.762" y2="5.207" width="0.1524" layer="21"/>
<wire x1="0.889" y1="-0.889" x2="0.889" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="1.016" y1="-0.508" x2="1.016" y2="4.953" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-0.889" x2="-0.889" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="-0.508" x2="-0.889" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="-0.508" x2="-1.016" y2="4.953" width="0.1524" layer="21"/>
<wire x1="0.508" y1="-1.778" x2="0.508" y2="-1.397" width="0.4064" layer="21"/>
<wire x1="-0.508" y1="-1.778" x2="-0.508" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="0.635" y1="-1.905" x2="1.27" y2="-2.54" width="0.4064" layer="51"/>
<wire x1="-0.635" y1="-1.905" x2="-1.27" y2="-2.54" width="0.4064" layer="51"/>
<wire x1="-0.508" y1="2.413" x2="-0.508" y2="2.032" width="0.1524" layer="21"/>
<wire x1="0.508" y1="2.032" x2="-0.508" y2="2.032" width="0.1524" layer="21"/>
<wire x1="0.508" y1="2.032" x2="0.508" y2="2.413" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="2.413" x2="0.508" y2="2.413" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="2.794" x2="0" y2="2.794" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="1.651" x2="0" y2="1.651" width="0.1524" layer="21"/>
<wire x1="0" y1="1.651" x2="0" y2="1.143" width="0.1524" layer="21"/>
<wire x1="0" y1="1.651" x2="0.508" y2="1.651" width="0.1524" layer="21"/>
<wire x1="0" y1="2.794" x2="0" y2="3.302" width="0.1524" layer="21"/>
<wire x1="0" y1="2.794" x2="0.508" y2="2.794" width="0.1524" layer="21"/>
<wire x1="1.016" y1="-0.508" x2="0.889" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.889" y1="-0.508" x2="-0.889" y2="-0.508" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="-2.54" drill="0.8128" diameter="1.6764"/>
<pad name="2" x="1.27" y="-2.54" drill="0.8128" diameter="1.6764"/>
<text x="-1.397" y="-0.508" size="0.508" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="2.667" y="-0.508" size="0.508" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="0.3048" y1="-1.524" x2="0.7112" y2="-0.9398" layer="21"/>
<rectangle x1="-0.7112" y1="-1.524" x2="-0.3048" y2="-0.9398" layer="21"/>
<rectangle x1="-1.778" y1="-1.778" x2="1.778" y2="5.842" layer="43"/>
</package>
<package name="XTAL_TC38H">
<wire x1="-1.397" y1="-0.889" x2="1.397" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="7.366" x2="1.524" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.524" y1="7.112" x2="-1.27" y2="7.366" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="7.366" x2="1.27" y2="7.366" width="0.1524" layer="21"/>
<wire x1="1.397" y1="-0.889" x2="1.397" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-0.508" x2="1.397" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-0.508" x2="1.524" y2="7.112" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="-0.889" x2="-1.397" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-0.508" x2="-1.397" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-0.508" x2="-1.524" y2="7.112" width="0.1524" layer="21"/>
<wire x1="1.397" y1="-0.508" x2="-1.397" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.5588" y1="-1.8288" x2="0.508" y2="-1.778" width="0.4064" layer="51"/>
<wire x1="0.508" y1="-1.778" x2="0.508" y2="-1.397" width="0.4064" layer="21"/>
<wire x1="-0.508" y1="-1.778" x2="-0.508" y2="-1.524" width="0.4064" layer="21"/>
<wire x1="-0.5588" y1="-1.8288" x2="-0.508" y2="-1.778" width="0.4064" layer="51"/>
<wire x1="0.635" y1="-1.905" x2="1.27" y2="-2.54" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="-1.905" x2="-1.27" y2="-2.54" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="3.048" x2="-0.762" y2="2.667" width="0.1524" layer="21"/>
<wire x1="0.762" y1="2.667" x2="-0.762" y2="2.667" width="0.1524" layer="21"/>
<wire x1="0.762" y1="2.667" x2="0.762" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="3.048" x2="0.762" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="3.429" x2="0" y2="3.429" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="2.286" x2="0" y2="2.286" width="0.1524" layer="21"/>
<wire x1="0" y1="2.286" x2="0" y2="1.778" width="0.1524" layer="21"/>
<wire x1="0" y1="2.286" x2="0.762" y2="2.286" width="0.1524" layer="21"/>
<wire x1="0" y1="3.429" x2="0" y2="3.937" width="0.1524" layer="21"/>
<wire x1="0" y1="3.429" x2="0.762" y2="3.429" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="-2.54" drill="0.8128" diameter="1.6764"/>
<pad name="2" x="1.27" y="-2.54" drill="0.8128" diameter="1.6764"/>
<text x="-1.905" y="-0.508" size="0.508" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="2.54" y="-0.508" size="0.508" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="0.3048" y1="-1.524" x2="0.7112" y2="-0.9398" layer="21"/>
<rectangle x1="-0.7112" y1="-1.524" x2="-0.3048" y2="-0.9398" layer="21"/>
<rectangle x1="-1.778" y1="-1.524" x2="1.778" y2="7.874" layer="43"/>
</package>
<package name="XTAL_TH_11.5X5">
<pad name="1" x="-2.54" y="0" drill="0.5334"/>
<pad name="2" x="2.3368" y="0" drill="0.5334"/>
<wire x1="-3.3202875" y1="2.48970625" x2="3.231134375" y2="2.4892" width="0.127" layer="21"/>
<wire x1="-3.390140625" y1="-2.487675" x2="3.1790625" y2="-2.4892" width="0.127" layer="21"/>
<wire x1="-3.294634375" y1="2.491996875" x2="-3.290571875" y2="-2.495040625" width="0.127" layer="21" curve="180"/>
<wire x1="3.16255" y1="-2.488440625" x2="3.1412125" y2="2.48716875" width="0.127" layer="21" curve="180"/>
<text x="-3.69849375" y="2.79044375" size="0.6096" layer="25">&gt;NAME</text>
<text x="-3.7917125" y="-3.34848125" size="0.6096" layer="27">&gt;VALUE</text>
</package>
<package name="XTAL_CM2X0C">
<smd name="4" x="-2.72415" y="1.5875" dx="1.30048125" dy="1.905" layer="1" rot="R180"/>
<smd name="1" x="-2.72415" y="-1.5875" dx="1.30048125" dy="1.905" layer="1" rot="R180"/>
<smd name="2" x="2.73685" y="-1.55575" dx="1.30048125" dy="1.905" layer="1" rot="R180"/>
<smd name="3" x="2.73685" y="1.5875" dx="1.30048125" dy="1.905" layer="1" rot="R180"/>
<wire x1="-3.80365" y1="-1.27" x2="-3.80365" y2="1.27" width="0.127" layer="21"/>
<wire x1="3.82143125" y1="-1.27" x2="3.82143125" y2="1.27" width="0.127" layer="21"/>
<wire x1="-3.54965" y1="1.27" x2="-1.89865" y2="1.27" width="0.127" layer="51"/>
<wire x1="1.97485" y1="1.27" x2="3.62585" y2="1.27" width="0.127" layer="51"/>
<wire x1="1.97485" y1="-1.27" x2="3.62585" y2="-1.27" width="0.127" layer="51"/>
<wire x1="-3.54965" y1="-1.27" x2="-1.89865" y2="-1.27" width="0.127" layer="51"/>
<wire x1="-3.80365" y1="-1.27" x2="-3.54965" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-1.8669" y1="-1.27" x2="1.8923" y2="-1.27" width="0.127" layer="21"/>
<wire x1="3.81635" y1="-1.27" x2="3.62585" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-3.80365" y1="1.27" x2="-3.54965" y2="1.27" width="0.127" layer="21"/>
<wire x1="-1.8669" y1="1.27" x2="1.8923" y2="1.27" width="0.127" layer="21"/>
<wire x1="3.81635" y1="1.27" x2="3.62585" y2="1.27" width="0.127" layer="21"/>
<circle x="2.9591" y="0" radius="0.179603125" width="0.127" layer="21"/>
<text x="-4.064" y="-1.27" size="0.508" layer="25" rot="R90">&gt;NAME</text>
</package>
<package name="XTAL_4-SMD">
<wire x1="-1.6002" y1="-1.249678125" x2="-1.6002" y2="1.24968125" width="0.127" layer="51"/>
<wire x1="-1.6002" y1="-1.249678125" x2="1.6002" y2="-1.249678125" width="0.127" layer="51"/>
<wire x1="-1.6002" y1="1.24968125" x2="1.6002" y2="1.24968125" width="0.127" layer="51"/>
<wire x1="1.6002" y1="-1.249678125" x2="1.6002" y2="1.24968125" width="0.127" layer="51"/>
<smd name="4" x="-1.1557" y="0.941071875" dx="1.397" dy="1.1938" layer="1"/>
<smd name="1" x="-1.1557" y="-0.900428125" dx="1.397" dy="1.1938" layer="1"/>
<smd name="3" x="1.1938" y="0.941071875" dx="1.397" dy="1.1938" layer="1"/>
<smd name="2" x="1.1938" y="-0.900428125" dx="1.397" dy="1.1938" layer="1"/>
<circle x="2.24155" y="1.004571875" radius="0.15875" width="0.127" layer="21"/>
<circle x="-2.2352" y="-1.154428125" radius="0.15875" width="0.127" layer="21"/>
<text x="-1.524" y="2.032003125" size="0.6096" layer="25">&gt;NAME</text>
</package>
<package name="HDR_3X1">
<description>3-Pin, 1-Row Header</description>
<wire x1="-1.27" y1="3.175" x2="-0.635" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="3.81" x2="0.635" y2="3.81" width="0.1524" layer="21"/>
<wire x1="0.635" y1="3.81" x2="1.27" y2="3.175" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-3.175" x2="0.635" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="3.175" x2="-1.27" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-3.175" x2="-0.635" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-3.81" x2="-0.635" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="1.27" y1="3.175" x2="1.27" y2="-3.175" width="0.1524" layer="21"/>
<pad name="1" x="0" y="-2.54" drill="1.016" shape="octagon"/>
<pad name="2" x="0" y="0" drill="1.016" shape="octagon"/>
<pad name="3" x="0" y="2.54" drill="1.016" shape="octagon"/>
<rectangle x1="-0.254" y1="-2.794" x2="0.254" y2="-2.286" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="2.286" x2="0.254" y2="2.794" layer="51"/>
<text x="1.905" y="-1.905" size="0.4064" layer="25" rot="R90">&gt;NAME</text>
</package>
<package name="HDR_2X1">
<description>2-pin, 1-Row Header</description>
<wire x1="-0.6096" y1="2.5146" x2="0.6604" y2="2.5146" width="0.1524" layer="21"/>
<wire x1="0.6604" y1="2.5146" x2="1.2954" y2="1.8796" width="0.1524" layer="21"/>
<wire x1="-0.6096" y1="2.5146" x2="-1.2446" y2="1.8796" width="0.1524" layer="21"/>
<wire x1="1.2954" y1="-1.9304" x2="0.6604" y2="-2.5654" width="0.1524" layer="21"/>
<wire x1="-1.2446" y1="1.8796" x2="-1.2446" y2="-1.9304" width="0.1524" layer="21"/>
<wire x1="-1.2446" y1="-1.9304" x2="-0.6096" y2="-2.5654" width="0.1524" layer="21"/>
<wire x1="0.6604" y1="-2.5654" x2="-0.6096" y2="-2.5654" width="0.1524" layer="21"/>
<wire x1="1.2954" y1="1.8796" x2="1.2954" y2="-1.9304" width="0.1524" layer="21"/>
<pad name="1" x="0.0254" y="-1.2954" drill="1.016" shape="octagon"/>
<pad name="2" x="0.0254" y="1.2446" drill="1.016" shape="octagon"/>
<text x="1.5621" y="-2.4003" size="0.6096" layer="21" ratio="10">1</text>
<rectangle x1="-0.2286" y1="-1.5494" x2="0.2794" y2="-1.0414" layer="51"/>
<rectangle x1="-0.2286" y1="0.9906" x2="0.2794" y2="1.4986" layer="51"/>
<text x="1.905" y="-1.27" size="0.4064" layer="25" rot="R90">&gt;NAME</text>
</package>
<package name="SW_K12A">
<description>C&amp;K K12A 0.1A 30V</description>
<pad name="1" x="-1.3" y="0" drill="0.9"/>
<pad name="2" x="1.3" y="0" drill="0.9"/>
<hole x="-3.75" y="-2.5" drill="1.1"/>
<hole x="3.75" y="2.5" drill="1.1"/>
<circle x="0" y="0" radius="6" width="0.127" layer="21"/>
</package>
<package name="SM08">
<smd name="A1" x="-3.1" y="1.9" dx="1.8" dy="1" layer="1" cream="no"/>
<smd name="A2" x="3.1" y="1.9" dx="1.8" dy="1" layer="1" cream="no"/>
<smd name="B2" x="3.1" y="-1.9" dx="1.8" dy="1" layer="1" cream="no"/>
<smd name="B1" x="-3.1" y="-1.9" dx="1.8" dy="1" layer="1" cream="no"/>
<text x="-3.2258" y="0.508" size="0.508" layer="21">&gt;NAME</text>
<text x="-3.2258" y="-0.889" size="0.508" layer="21">&gt;VALUE</text>
</package>
<package name="RST_TAC_SW-TH_RA">
<description>FSMRA2JH Tactile switch (through hole, right angle)</description>
<pad name="1" x="-2.2987" y="0.53975" drill="1.1"/>
<pad name="2" x="2.1971" y="0.53975" drill="1.1"/>
<wire x1="-3.556" y1="-2.00025" x2="3.556" y2="-2.00025" width="0.127" layer="21"/>
<wire x1="-1.651" y1="-3.27025" x2="-1.651" y2="-2.01295" width="0.127" layer="21"/>
<wire x1="1.8415" y1="-3.27025" x2="1.8415" y2="-2.02565" width="0.127" layer="21"/>
<wire x1="-1.651" y1="-3.27025" x2="1.8415" y2="-3.27025" width="0.127" layer="21"/>
<wire x1="1.8415" y1="-3.27025" x2="1.8542" y2="-3.27025" width="0.127" layer="21"/>
<wire x1="3.556" y1="-2.00025" x2="3.556" y2="2.00025" width="0.127" layer="21"/>
<wire x1="3.556" y1="2.00025" x2="3.556" y2="4.60375" width="0.127" layer="51"/>
<wire x1="-3.556" y1="-2.00025" x2="-3.556" y2="2.00025" width="0.127" layer="21"/>
<wire x1="-3.556" y1="2.00025" x2="-3.556" y2="4.60375" width="0.127" layer="51"/>
<wire x1="-3.556" y1="2.00025" x2="3.556" y2="2.00025" width="0.127" layer="21"/>
<wire x1="3.175" y1="4.60375" x2="3.175" y2="2.00025" width="0.127" layer="51"/>
<wire x1="-3.175" y1="4.60375" x2="-3.175" y2="2.00025" width="0.127" layer="51"/>
<wire x1="-3.556" y1="4.60375" x2="-3.175" y2="4.60375" width="0.127" layer="21"/>
<wire x1="3.556" y1="4.60375" x2="3.175" y2="4.60375" width="0.127" layer="21"/>
<hole x="3.4544" y="3.07975" drill="1.3"/>
<hole x="-3.556" y="3.07975" drill="1.3"/>
</package>
<package name="SW_KMR">
<smd name="4" x="-1.987553125" y="0.85216875" dx="0.899921875" dy="0.999996875" layer="1" rot="R270"/>
<smd name="3" x="2.006596875" y="0.85216875" dx="0.899921875" dy="0.999996875" layer="1" rot="R90"/>
<smd name="2" x="2.006596875" y="-0.84963125" dx="0.899921875" dy="0.999996875" layer="1" rot="R90"/>
<smd name="1" x="-1.987553125" y="-0.84963125" dx="0.899921875" dy="0.999996875" layer="1" rot="R90"/>
<wire x1="-1.97358125" y1="-1.30048125" x2="-1.97358125" y2="1.2979375" width="0.0508" layer="51"/>
<wire x1="1.9710375" y1="-1.30048125" x2="1.9710375" y2="1.2915875" width="0.0508" layer="51"/>
<wire x1="-1.346203125" y1="1.2994625" x2="1.358896875" y2="1.2994625" width="0.0508" layer="21"/>
<wire x1="-1.346203125" y1="-1.30048125" x2="1.358896875" y2="-1.30048125" width="0.0508" layer="21"/>
<wire x1="-1.97358125" y1="-1.30048125" x2="-1.346203125" y2="-1.30048125" width="0.0508" layer="51"/>
<wire x1="1.968496875" y1="-1.30048125" x2="1.358896875" y2="-1.30048125" width="0.0508" layer="51"/>
<wire x1="1.358896875" y1="1.2994625" x2="1.9710375" y2="1.2994625" width="0.0508" layer="51"/>
<wire x1="-1.974853125" y1="1.2994625" x2="-1.346203125" y2="1.2994625" width="0.0508" layer="51"/>
<circle x="-0.001271875" y="-0.000509375" radius="0.621259375" width="0.0508" layer="51"/>
<text x="-1.159509375" y="1.69875" size="0.508" layer="25">&gt;NAME</text>
</package>
<package name="PTS645_6MM">
<description>PTS645 6mm SMT Tactile Switch</description>
<smd name="A1" x="-3.9751" y="2.249421875" dx="1.6002" dy="1.3462" layer="1" cream="no"/>
<smd name="A2" x="3.9751" y="2.249421875" dx="1.6002" dy="1.3462" layer="1" cream="no"/>
<smd name="B2" x="3.9751" y="-2.250440625" dx="1.6002" dy="1.3462" layer="1" cream="no"/>
<smd name="B1" x="-3.9751" y="-2.250440625" dx="1.6002" dy="1.3462" layer="1" cream="no"/>
<text x="0" y="-0.000509375" size="0.508" layer="21">&gt;NAME</text>
<wire x1="-3.2131" y1="1.305559375" x2="-3.2131" y2="-1.234440625" width="0.127" layer="21"/>
<wire x1="-2.9591" y1="3.083559375" x2="2.8829" y2="3.083559375" width="0.127" layer="21"/>
<wire x1="-2.9591" y1="-3.012440625" x2="2.8829" y2="-3.012440625" width="0.127" layer="21"/>
<wire x1="3.1369" y1="1.305559375" x2="3.1369" y2="-1.234440625" width="0.127" layer="21"/>
<circle x="0" y="-0.000509375" radius="1.481059375" width="0.127" layer="21"/>
<wire x1="-2.4511" y1="2.321559375" x2="0.0889" y2="2.321559375" width="0.127" layer="51"/>
<wire x1="0.0889" y1="2.321559375" x2="2.3749" y2="2.321559375" width="0.127" layer="51"/>
<wire x1="-2.4511" y1="-2.250440625" x2="0.0889" y2="-2.250440625" width="0.127" layer="51"/>
<wire x1="0.0889" y1="-2.250440625" x2="2.3749" y2="-2.250440625" width="0.127" layer="51"/>
<wire x1="0.0889" y1="2.321559375" x2="0.0889" y2="0.797559375" width="0.127" layer="51"/>
<wire x1="0.0889" y1="-2.250440625" x2="0.0889" y2="-0.726440625" width="0.127" layer="51"/>
<wire x1="0.0889" y1="0.797559375" x2="-0.9271" y2="-0.472440625" width="0.127" layer="51"/>
</package>
<package name="HDR_1X1">
<description>Single Pin Header</description>
<wire x1="-0.6096" y1="1.2446" x2="0.6604" y2="1.2446" width="0.1524" layer="21"/>
<wire x1="0.6604" y1="1.2446" x2="1.2954" y2="0.6096" width="0.1524" layer="21"/>
<wire x1="-0.6096" y1="1.2446" x2="-1.2446" y2="0.6096" width="0.1524" layer="21"/>
<wire x1="1.2954" y1="-0.6604" x2="0.6604" y2="-1.2954" width="0.1524" layer="21"/>
<wire x1="-1.2446" y1="0.6096" x2="-1.2446" y2="-0.6604" width="0.1524" layer="21"/>
<wire x1="-1.2446" y1="-0.6604" x2="-0.6096" y2="-1.2954" width="0.1524" layer="21"/>
<wire x1="0.6604" y1="-1.2954" x2="-0.6096" y2="-1.2954" width="0.1524" layer="21"/>
<wire x1="1.2954" y1="0.6096" x2="1.2954" y2="-0.6604" width="0.1524" layer="21"/>
<pad name="1" x="0.0254" y="-0.0254" drill="1.016" shape="octagon"/>
<text x="-1.6256" y="-1.2446" size="0.4064" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<rectangle x1="-0.2286" y1="-0.2794" x2="0.2794" y2="0.2286" layer="51"/>
</package>
<package name="HDR_1X1_SMD">
<smd name="P$1" x="0" y="0" dx="1.3" dy="1.5" layer="1" rot="R90"/>
</package>
<package name="LED_5MM">
<wire x1="2.5654" y1="-1.8542" x2="2.5654" y2="1.9558" width="0.2032" layer="21"/>
<wire x1="2.5654" y1="-1.8542" x2="2.5654" y2="1.9558" width="0.254" layer="21" curve="-286.260205"/>
<wire x1="-1.1176" y1="0.0508" x2="0.0254" y2="1.1938" width="0.1524" layer="51" curve="-90"/>
<wire x1="0.0254" y1="-1.0922" x2="1.1684" y2="0.0508" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.6256" y1="0.0508" x2="0.0254" y2="1.7018" width="0.1524" layer="51" curve="-90"/>
<wire x1="0.0254" y1="-1.6002" x2="1.6764" y2="0.0508" width="0.1524" layer="51" curve="90"/>
<wire x1="-2.1336" y1="0.0508" x2="0.0254" y2="2.2098" width="0.1524" layer="51" curve="-90"/>
<wire x1="0.0254" y1="-2.1082" x2="2.1844" y2="0.0508" width="0.1524" layer="51" curve="90"/>
<circle x="0.0254" y="0.0508" radius="2.54" width="0.1524" layer="21"/>
<pad name="A" x="-1.2446" y="0.0508" drill="0.8128" shape="octagon"/>
<pad name="C" x="1.2954" y="0.0508" drill="0.8128" shape="octagon"/>
<text x="-2.1336" y="3.6322" size="0.508" layer="21">&gt;NAME</text>
<text x="-2.1082" y="-4.064" size="0.508" layer="21">&gt;VALUE</text>
</package>
<package name="LED_ARRAY_9">
<description>Circular LED array</description>
<circle x="0" y="0" radius="12.192" width="0.4064" layer="51"/>
<wire x1="4.699" y1="1.905" x2="4.699" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="4.699" y1="1.905" x2="4.699" y2="-1.905" width="0.254" layer="21" curve="-286.260205"/>
<wire x1="8.382" y1="0" x2="7.239" y2="-1.143" width="0.1524" layer="51" curve="-90"/>
<wire x1="7.239" y1="1.143" x2="6.096" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="8.89" y1="0" x2="7.239" y2="-1.651" width="0.1524" layer="51" curve="-90"/>
<wire x1="7.239" y1="1.651" x2="5.588" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="9.398" y1="0" x2="7.239" y2="-2.159" width="0.1524" layer="51" curve="-90"/>
<wire x1="7.239" y1="2.159" x2="5.08" y2="0" width="0.1524" layer="51" curve="90"/>
<circle x="7.239" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="A8" x="8.509" y="0" drill="0.8128" shape="octagon" rot="R180"/>
<pad name="K8" x="5.969" y="0" drill="0.8128" shape="octagon" rot="R180"/>
<wire x1="-1.905" y1="-2.54" x2="1.905" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="1.905" y2="-2.54" width="0.254" layer="21" curve="-286.260205"/>
<wire x1="0" y1="1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="-90"/>
<wire x1="-1.143" y1="0" x2="0" y2="-1.143" width="0.1524" layer="51" curve="90"/>
<wire x1="0" y1="1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="-90"/>
<wire x1="-1.651" y1="0" x2="0" y2="-1.651" width="0.1524" layer="51" curve="90"/>
<wire x1="0" y1="2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="-90"/>
<wire x1="-2.159" y1="0" x2="0" y2="-2.159" width="0.1524" layer="51" curve="90"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="A1" x="0" y="1.27" drill="0.8128" shape="octagon" rot="R270"/>
<pad name="K1" x="0" y="-1.27" drill="0.8128" shape="octagon" rot="R270"/>
<wire x1="1.905" y1="-4.699" x2="-1.905" y2="-4.699" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-4.699" x2="-1.905" y2="-4.699" width="0.254" layer="21" curve="-286.260205"/>
<wire x1="0" y1="-8.382" x2="-1.143" y2="-7.239" width="0.1524" layer="51" curve="-90"/>
<wire x1="1.143" y1="-7.239" x2="0" y2="-6.096" width="0.1524" layer="51" curve="90"/>
<wire x1="0" y1="-8.89" x2="-1.651" y2="-7.239" width="0.1524" layer="51" curve="-90"/>
<wire x1="1.651" y1="-7.239" x2="0" y2="-5.588" width="0.1524" layer="51" curve="90"/>
<wire x1="0" y1="-9.398" x2="-2.159" y2="-7.239" width="0.1524" layer="51" curve="-90"/>
<wire x1="2.159" y1="-7.239" x2="0" y2="-5.08" width="0.1524" layer="51" curve="90"/>
<circle x="0" y="-7.239" radius="2.54" width="0.1524" layer="21"/>
<pad name="A2" x="0" y="-8.509" drill="0.8128" shape="octagon" rot="R90"/>
<pad name="K2" x="0" y="-5.969" drill="0.8128" shape="octagon" rot="R90"/>
<wire x1="-4.699" y1="-1.905" x2="-4.699" y2="1.905" width="0.2032" layer="21"/>
<wire x1="-4.699" y1="-1.905" x2="-4.699" y2="1.905" width="0.254" layer="21" curve="-286.260205"/>
<wire x1="-8.382" y1="0" x2="-7.239" y2="1.143" width="0.1524" layer="51" curve="-90"/>
<wire x1="-7.239" y1="-1.143" x2="-6.096" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-8.89" y1="0" x2="-7.239" y2="1.651" width="0.1524" layer="51" curve="-90"/>
<wire x1="-7.239" y1="-1.651" x2="-5.588" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-9.398" y1="0" x2="-7.239" y2="2.159" width="0.1524" layer="51" curve="-90"/>
<wire x1="-7.239" y1="-2.159" x2="-5.08" y2="0" width="0.1524" layer="51" curve="90"/>
<circle x="-7.239" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="A4" x="-8.509" y="0" drill="0.8128" shape="octagon"/>
<pad name="K4" x="-5.969" y="0" drill="0.8128" shape="octagon"/>
<wire x1="-1.905" y1="4.699" x2="1.905" y2="4.699" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="4.699" x2="1.905" y2="4.699" width="0.254" layer="21" curve="-286.260205"/>
<wire x1="0" y1="8.382" x2="1.143" y2="7.239" width="0.1524" layer="51" curve="-90"/>
<wire x1="-1.143" y1="7.239" x2="0" y2="6.096" width="0.1524" layer="51" curve="90"/>
<wire x1="0" y1="8.89" x2="1.651" y2="7.239" width="0.1524" layer="51" curve="-90"/>
<wire x1="-1.651" y1="7.239" x2="0" y2="5.588" width="0.1524" layer="51" curve="90"/>
<wire x1="0" y1="9.398" x2="2.159" y2="7.239" width="0.1524" layer="51" curve="-90"/>
<wire x1="-2.159" y1="7.239" x2="0" y2="5.08" width="0.1524" layer="51" curve="90"/>
<circle x="0" y="7.239" radius="2.54" width="0.1524" layer="21"/>
<pad name="A6" x="0" y="8.509" drill="0.8128" shape="octagon" rot="R270"/>
<pad name="K6" x="0" y="5.969" drill="0.8128" shape="octagon" rot="R270"/>
<wire x1="4.5770625" y1="-1.850296875" x2="1.882984375" y2="-4.544375" width="0.2032" layer="21"/>
<wire x1="4.5770625" y1="-1.850296875" x2="1.882984375" y2="-4.544375" width="0.254" layer="21" curve="-286.260205"/>
<wire x1="5.834296875" y1="-5.801609375" x2="4.217853125" y2="-5.801609375" width="0.1524" layer="51" curve="-90"/>
<wire x1="5.834296875" y1="-4.1851625" x2="4.217853125" y2="-4.1851625" width="0.1524" layer="51" curve="90"/>
<wire x1="6.193509375" y1="-6.16081875" x2="3.858640625" y2="-6.16081875" width="0.1524" layer="51" curve="-90"/>
<wire x1="6.193509375" y1="-3.825953125" x2="3.858640625" y2="-3.825953125" width="0.1524" layer="51" curve="90"/>
<wire x1="6.55271875" y1="-6.52003125" x2="3.49943125" y2="-6.52003125" width="0.1524" layer="51" curve="-90"/>
<wire x1="6.55271875" y1="-3.46674375" x2="3.49943125" y2="-3.46674375" width="0.1524" layer="51" curve="90"/>
<circle x="5.026075" y="-4.9933875" radius="2.54" width="0.1524" layer="21"/>
<pad name="A9" x="5.9241" y="-5.8914125" drill="0.8128" shape="octagon" rot="R135"/>
<pad name="K9" x="4.12805" y="-4.0953625" drill="0.8128" shape="octagon" rot="R135"/>
<wire x1="1.948353125" y1="4.62779375" x2="4.64243125" y2="1.93371875" width="0.2032" layer="21"/>
<wire x1="1.948353125" y1="4.62779375" x2="4.64243125" y2="1.93371875" width="0.254" layer="21" curve="-286.260205"/>
<wire x1="5.899665625" y1="5.88503125" x2="5.899665625" y2="4.268584375" width="0.1524" layer="51" curve="-90"/>
<wire x1="4.283221875" y1="5.88503125" x2="4.283221875" y2="4.268584375" width="0.1524" layer="51" curve="90"/>
<wire x1="6.258878125" y1="6.244240625" x2="6.258878125" y2="3.909375" width="0.1524" layer="51" curve="-90"/>
<wire x1="3.924009375" y1="6.244240625" x2="3.924009375" y2="3.909375" width="0.1524" layer="51" curve="90"/>
<wire x1="6.6180875" y1="6.60345" x2="6.6180875" y2="3.5501625" width="0.1524" layer="51" curve="-90"/>
<wire x1="3.5648" y1="6.60345" x2="3.5648" y2="3.5501625" width="0.1524" layer="51" curve="90"/>
<circle x="5.09144375" y="5.07680625" radius="2.54" width="0.1524" layer="21"/>
<pad name="A7" x="5.98946875" y="5.974834375" drill="0.8128" shape="octagon" rot="R225"/>
<pad name="K7" x="4.19341875" y="4.17878125" drill="0.8128" shape="octagon" rot="R225"/>
<wire x1="-4.65993125" y1="1.90609375" x2="-1.96585625" y2="4.60016875" width="0.2032" layer="21"/>
<wire x1="-4.65993125" y1="1.90609375" x2="-1.96585625" y2="4.60016875" width="0.254" layer="21" curve="-286.260205"/>
<wire x1="-5.91716875" y1="5.85740625" x2="-4.300721875" y2="5.85740625" width="0.1524" layer="51" curve="-90"/>
<wire x1="-5.91716875" y1="4.240959375" x2="-4.300721875" y2="4.240959375" width="0.1524" layer="51" curve="90"/>
<wire x1="-6.276378125" y1="6.216615625" x2="-3.9415125" y2="6.216615625" width="0.1524" layer="51" curve="-90"/>
<wire x1="-6.276378125" y1="3.88175" x2="-3.9415125" y2="3.88175" width="0.1524" layer="51" curve="90"/>
<wire x1="-6.6355875" y1="6.575828125" x2="-3.5823" y2="6.575828125" width="0.1524" layer="51" curve="-90"/>
<wire x1="-6.6355875" y1="3.522540625" x2="-3.5823" y2="3.522540625" width="0.1524" layer="51" curve="90"/>
<circle x="-5.10894375" y="5.049184375" radius="2.54" width="0.1524" layer="21"/>
<pad name="A5" x="-6.00696875" y="5.947209375" drill="0.8128" shape="octagon" rot="R45"/>
<pad name="K5" x="-4.21091875" y="4.15115625" drill="0.8128" shape="octagon" rot="R315"/>
<wire x1="-1.876053125" y1="-4.609196875" x2="-4.570128125" y2="-1.91511875" width="0.2032" layer="21"/>
<wire x1="-1.876053125" y1="-4.609196875" x2="-4.570128125" y2="-1.91511875" width="0.254" layer="21" curve="-286.260205"/>
<wire x1="-5.827365625" y1="-5.86643125" x2="-5.827365625" y2="-4.249984375" width="0.1524" layer="51" curve="-90"/>
<wire x1="-4.21091875" y1="-5.86643125" x2="-4.21091875" y2="-4.249984375" width="0.1524" layer="51" curve="90"/>
<wire x1="-6.186575" y1="-6.225640625" x2="-6.186575" y2="-3.890775" width="0.1524" layer="51" curve="-90"/>
<wire x1="-3.851709375" y1="-6.225640625" x2="-3.851709375" y2="-3.890775" width="0.1524" layer="51" curve="90"/>
<wire x1="-6.545784375" y1="-6.584853125" x2="-6.545784375" y2="-3.531565625" width="0.1524" layer="51" curve="-90"/>
<wire x1="-3.4925" y1="-6.584853125" x2="-3.4925" y2="-3.531565625" width="0.1524" layer="51" curve="90"/>
<circle x="-5.019140625" y="-5.058209375" radius="2.54" width="0.1524" layer="21"/>
<pad name="A3" x="-5.91716875" y="-5.956234375" drill="0.8128" shape="octagon" rot="R45"/>
<pad name="K3" x="-4.121115625" y="-4.160184375" drill="0.8128" shape="octagon" rot="R45"/>
<wire x1="-9.017" y1="0" x2="9.017" y2="0" width="0.127" layer="21"/>
<wire x1="0" y1="-9.017" x2="0" y2="9.017" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-6.35" x2="6.4008" y2="6.4008" width="0.127" layer="21"/>
<wire x1="6.35" y1="-6.35" x2="-6.4008" y2="6.4008" width="0.127" layer="21"/>
</package>
<package name="LED_0805_PIPE">
<description>LED (0805 pkg) with light pipe attachment</description>
<wire x1="-2.6" y1="2.59" x2="2.59" y2="2.59" width="0.1" layer="21"/>
<wire x1="2.59" y1="2.59" x2="2.58" y2="-2.64" width="0.1" layer="21"/>
<wire x1="2.58" y1="-2.64" x2="-2.61" y2="-2.64" width="0.1" layer="21"/>
<wire x1="-2.61" y1="-2.64" x2="-2.6" y2="2.59" width="0.1" layer="21"/>
<hole x="-1.89" y="1.89" drill="1"/>
<hole x="1.89" y="-1.89" drill="1"/>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<smd name="A" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="C" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
<wire x1="0.762" y1="-1.7145" x2="0.127" y2="-1.7145" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-2.3495" x2="-0.889" y2="-1.0795" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-1.0795" x2="0.127" y2="-1.7145" width="0.1524" layer="21"/>
<wire x1="0.127" y1="-1.7145" x2="-0.889" y2="-2.3495" width="0.1524" layer="21"/>
<wire x1="0.127" y1="-2.3495" x2="0.127" y2="-1.7145" width="0.1524" layer="21"/>
<wire x1="0.127" y1="-1.7145" x2="0.127" y2="-1.0795" width="0.1524" layer="21"/>
<wire x1="-0.9525" y1="-1.7145" x2="-1.651" y2="-1.7145" width="0.1524" layer="21"/>
</package>
<package name="LED_ARRAY_50DEG_1206">
<description>LED Array - 1206 SMT LEDs, 50deg view angle

&lt;p&gt; Light circles represent area of light on lens @ 50deg per LED, based on 0.35" distance (base of dome / top of package housing to lens, not board floor)</description>
<circle x="0" y="0" radius="11.43" width="0.127" layer="21"/>
<circle x="-3.4925" y="6.0452" radius="4.1402" width="0.0508" layer="51"/>
<circle x="3.4925" y="6.0452" radius="4.1402" width="0.0508" layer="51"/>
<circle x="0" y="0" radius="4.1402" width="0.0508" layer="51"/>
<circle x="-3.4925" y="-6.0452" radius="4.1402" width="0.0508" layer="51"/>
<circle x="6.985" y="0" radius="4.1402" width="0.0508" layer="51"/>
<circle x="-6.985" y="0" radius="4.1402" width="0.0508" layer="51"/>
<circle x="3.4925" y="-6.0452" radius="4.1402" width="0.0508" layer="51"/>
<wire x1="-7.95" y1="0.787" x2="-6.02" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-7.95" y1="-0.787" x2="-6.02" y2="-0.787" width="0.1016" layer="51"/>
<smd name="A1" x="-8.385" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="C1" x="-5.585" y="0" dx="1.6" dy="1.8" layer="1"/>
<rectangle x1="-8.6868" y1="-0.8509" x2="-7.9367" y2="0.8491" layer="51"/>
<rectangle x1="-6.0333" y1="-0.8491" x2="-5.2832" y2="0.8509" layer="51"/>
<rectangle x1="-7.1849" y1="-0.4001" x2="-6.7851" y2="0.4001" layer="35"/>
<circle x="-6.985" y="0" radius="0.148103125" width="0.0508" layer="51"/>
<circle x="0" y="0" radius="0.148103125" width="0.0508" layer="51"/>
<circle x="6.985" y="0" radius="0.148103125" width="0.0508" layer="51"/>
<circle x="3.4925" y="6.0452" radius="0.148103125" width="0.0508" layer="51"/>
<circle x="-3.4925" y="-6.0452" radius="0.148103125" width="0.0508" layer="51"/>
<circle x="3.4925" y="-6.0452" radius="0.148103125" width="0.0508" layer="51"/>
<circle x="-3.4925" y="6.0452" radius="0.148103125" width="0.0508" layer="51"/>
<wire x1="-6.6294" y1="0.5588" x2="-6.6294" y2="-0.5588" width="0.0508" layer="21"/>
<wire x1="0.787" y1="0.965" x2="0.787" y2="-0.965" width="0.1016" layer="51"/>
<wire x1="-0.787" y1="0.965" x2="-0.787" y2="-0.965" width="0.1016" layer="51"/>
<smd name="A2" x="0" y="1.4" dx="1.6" dy="1.8" layer="1" rot="R270"/>
<smd name="C2" x="0" y="-1.4" dx="1.6" dy="1.8" layer="1" rot="R270"/>
<rectangle x1="-0.37595" y1="0.47675" x2="0.37415" y2="2.17675" layer="51" rot="R270"/>
<rectangle x1="-0.37415" y1="-2.17675" x2="0.37595" y2="-0.47675" layer="51" rot="R270"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35" rot="R270"/>
<wire x1="0.5588" y1="-0.3556" x2="-0.5588" y2="-0.3556" width="0.0508" layer="21"/>
<wire x1="7.95" y1="-0.8505" x2="6.02" y2="-0.8505" width="0.1016" layer="51"/>
<wire x1="7.95" y1="0.7235" x2="6.02" y2="0.7235" width="0.1016" layer="51"/>
<smd name="A3" x="8.385" y="-0.0635" dx="1.6" dy="1.8" layer="1" rot="R180"/>
<smd name="C3" x="5.585" y="-0.0635" dx="1.6" dy="1.8" layer="1" rot="R180"/>
<rectangle x1="7.9367" y1="-0.9126" x2="8.6868" y2="0.7874" layer="51" rot="R180"/>
<rectangle x1="5.2832" y1="-0.9144" x2="6.0333" y2="0.7856" layer="51" rot="R180"/>
<rectangle x1="6.7851" y1="-0.4636" x2="7.1849" y2="0.3366" layer="35" rot="R180"/>
<wire x1="6.6294" y1="-0.6223" x2="6.6294" y2="0.4953" width="0.0508" layer="21"/>
<wire x1="-4.2795" y1="-6.9975" x2="-4.2795" y2="-5.0675" width="0.1016" layer="51"/>
<wire x1="-2.7055" y1="-6.9975" x2="-2.7055" y2="-5.0675" width="0.1016" layer="51"/>
<smd name="A4" x="-3.4925" y="-7.4325" dx="1.6" dy="1.8" layer="1" rot="R90"/>
<smd name="C4" x="-3.4925" y="-4.6325" dx="1.6" dy="1.8" layer="1" rot="R90"/>
<rectangle x1="-3.86665" y1="-8.20925" x2="-3.11655" y2="-6.50925" layer="51" rot="R90"/>
<rectangle x1="-3.86845" y1="-5.55575" x2="-3.11835" y2="-3.85575" layer="51" rot="R90"/>
<rectangle x1="-3.6924" y1="-6.4326" x2="-3.2926" y2="-5.6324" layer="35" rot="R90"/>
<wire x1="-4.0513" y1="-5.6769" x2="-2.9337" y2="-5.6769" width="0.0508" layer="21"/>
<wire x1="2.7055" y1="-6.9975" x2="2.7055" y2="-5.0675" width="0.1016" layer="51"/>
<wire x1="4.2795" y1="-6.9975" x2="4.2795" y2="-5.0675" width="0.1016" layer="51"/>
<smd name="A5" x="3.4925" y="-7.4325" dx="1.6" dy="1.8" layer="1" rot="R90"/>
<smd name="C5" x="3.4925" y="-4.6325" dx="1.6" dy="1.8" layer="1" rot="R90"/>
<rectangle x1="3.11835" y1="-8.20925" x2="3.86845" y2="-6.50925" layer="51" rot="R90"/>
<rectangle x1="3.11655" y1="-5.55575" x2="3.86665" y2="-3.85575" layer="51" rot="R90"/>
<rectangle x1="3.2926" y1="-6.4326" x2="3.6924" y2="-5.6324" layer="35" rot="R90"/>
<wire x1="2.9337" y1="-5.6769" x2="4.0513" y2="-5.6769" width="0.0508" layer="21"/>
<wire x1="-2.7055" y1="6.9975" x2="-2.7055" y2="5.0675" width="0.1016" layer="51"/>
<wire x1="-4.2795" y1="6.9975" x2="-4.2795" y2="5.0675" width="0.1016" layer="51"/>
<smd name="A6" x="-3.4925" y="7.4325" dx="1.6" dy="1.8" layer="1" rot="R270"/>
<smd name="C6" x="-3.4925" y="4.6325" dx="1.6" dy="1.8" layer="1" rot="R270"/>
<rectangle x1="-3.86845" y1="6.50925" x2="-3.11835" y2="8.20925" layer="51" rot="R270"/>
<rectangle x1="-3.86665" y1="3.85575" x2="-3.11655" y2="5.55575" layer="51" rot="R270"/>
<rectangle x1="-3.6924" y1="5.6324" x2="-3.2926" y2="6.4326" layer="35" rot="R270"/>
<wire x1="-2.9337" y1="5.6769" x2="-4.0513" y2="5.6769" width="0.0508" layer="21"/>
<wire x1="4.2795" y1="6.9975" x2="4.2795" y2="5.0675" width="0.1016" layer="51"/>
<wire x1="2.7055" y1="6.9975" x2="2.7055" y2="5.0675" width="0.1016" layer="51"/>
<smd name="A7" x="3.4925" y="7.4325" dx="1.6" dy="1.8" layer="1" rot="R270"/>
<smd name="C7" x="3.4925" y="4.6325" dx="1.6" dy="1.8" layer="1" rot="R270"/>
<rectangle x1="3.11655" y1="6.50925" x2="3.86665" y2="8.20925" layer="51" rot="R270"/>
<rectangle x1="3.11835" y1="3.85575" x2="3.86845" y2="5.55575" layer="51" rot="R270"/>
<rectangle x1="3.2926" y1="5.6324" x2="3.6924" y2="6.4326" layer="35" rot="R270"/>
<wire x1="4.0513" y1="5.6769" x2="2.9337" y2="5.6769" width="0.0508" layer="21"/>
</package>
<package name="LED_ARRAY_60DEG_4PLCC">
<description>LED Array, 4-PLCC SMD LEDs, 60deg View Angle

&lt;p&gt; Light circles represent area of light on lens @ 60deg per LED, based on 0.31" distance (base of dome / top of package housing to lens, not board floor)</description>
<circle x="0" y="0" radius="11.43" width="0.127" layer="21"/>
<circle x="0" y="0" radius="4.5466" width="0.0508" layer="51"/>
<circle x="-3.81" y="-5.334" radius="4.5466" width="0.0508" layer="51"/>
<circle x="0" y="6.5405" radius="4.5466" width="0.0508" layer="51"/>
<circle x="-6.223" y="1.905" radius="4.5466" width="0.0508" layer="51"/>
<circle x="6.223" y="1.905" radius="4.5466" width="0.0508" layer="51"/>
<circle x="3.81" y="-5.334" radius="4.5466" width="0.0508" layer="51"/>
<wire x1="-7.704403125" y1="0.20683125" x2="-4.707203125" y2="0.20683125" width="0.0508" layer="51"/>
<wire x1="-7.704403125" y1="0.20683125" x2="-7.704403125" y2="3.61043125" width="0.0508" layer="21"/>
<wire x1="-7.704403125" y1="3.61043125" x2="-4.707203125" y2="3.61043125" width="0.0508" layer="51"/>
<circle x="-6.205803125" y="1.90863125" radius="1.2065" width="0.0508" layer="51"/>
<smd name="A2" x="-5.456503125" y="3.52788125" dx="0.889" dy="2.032" layer="1" rot="R180"/>
<smd name="A1" x="-6.955103125" y="3.52788125" dx="0.889" dy="2.032" layer="1" rot="R180"/>
<smd name="A3" x="-6.955103125" y="0.28938125" dx="0.889" dy="2.032" layer="1" rot="R180"/>
<smd name="C" x="-5.456503125" y="0.28938125" dx="0.889" dy="2.032" layer="1" rot="R180"/>
<wire x1="-4.707203125" y1="0.20683125" x2="-4.707203125" y2="3.61043125" width="0.0508" layer="21"/>
<wire x1="-7.704403125" y1="2.51823125" x2="-4.707203125" y2="2.51823125" width="0.0508" layer="51"/>
<wire x1="-7.704403125" y1="1.29903125" x2="-4.707203125" y2="1.29903125" width="0.0508" layer="51"/>
<wire x1="-5.89008125" y1="1.59961875" x2="-4.8817" y2="1.59961875" width="0.4064" layer="21"/>
<wire x1="-1.7018" y1="1.4986" x2="-1.7018" y2="-1.4986" width="0.0508" layer="51"/>
<wire x1="-1.7018" y1="1.4986" x2="1.7018" y2="1.4986" width="0.0508" layer="21"/>
<wire x1="1.7018" y1="1.4986" x2="1.7018" y2="-1.4986" width="0.0508" layer="51"/>
<circle x="0" y="0" radius="1.2065" width="0.0508" layer="51"/>
<smd name="A4" x="1.61925" y="-0.7493" dx="0.889" dy="2.032" layer="1" rot="R90"/>
<smd name="A5" x="1.61925" y="0.7493" dx="0.889" dy="2.032" layer="1" rot="R90"/>
<smd name="A6" x="-1.61925" y="0.7493" dx="0.889" dy="2.032" layer="1" rot="R90"/>
<smd name="C1" x="-1.61925" y="-0.7493" dx="0.889" dy="2.032" layer="1" rot="R90"/>
<wire x1="-1.7018" y1="-1.4986" x2="1.7018" y2="-1.4986" width="0.0508" layer="21"/>
<wire x1="0.6096" y1="1.4986" x2="0.6096" y2="-1.4986" width="0.0508" layer="51"/>
<wire x1="-0.6096" y1="1.4986" x2="-0.6096" y2="-1.4986" width="0.0508" layer="51"/>
<wire x1="-0.322578125" y1="-0.27305" x2="-0.322578125" y2="-1.28143125" width="0.4064" layer="21"/>
<wire x1="-2.10934375" y1="-6.82018125" x2="-2.10934375" y2="-3.82298125" width="0.0508" layer="51"/>
<wire x1="-2.10934375" y1="-6.82018125" x2="-5.51294375" y2="-6.82018125" width="0.0508" layer="21"/>
<wire x1="-5.51294375" y1="-6.82018125" x2="-5.51294375" y2="-3.82298125" width="0.0508" layer="51"/>
<circle x="-3.81114375" y="-5.32158125" radius="1.2065" width="0.0508" layer="51"/>
<smd name="A7" x="-5.43039375" y="-4.57228125" dx="0.889" dy="2.032" layer="1" rot="R270"/>
<smd name="A8" x="-5.43039375" y="-6.07088125" dx="0.889" dy="2.032" layer="1" rot="R270"/>
<smd name="A9" x="-2.19189375" y="-6.07088125" dx="0.889" dy="2.032" layer="1" rot="R270"/>
<smd name="C2" x="-2.19189375" y="-4.57228125" dx="0.889" dy="2.032" layer="1" rot="R270"/>
<wire x1="-2.10934375" y1="-3.82298125" x2="-5.51294375" y2="-3.82298125" width="0.0508" layer="21"/>
<wire x1="-4.42074375" y1="-6.82018125" x2="-4.42074375" y2="-3.82298125" width="0.0508" layer="51"/>
<wire x1="-3.20154375" y1="-6.82018125" x2="-3.20154375" y2="-3.82298125" width="0.0508" layer="51"/>
<wire x1="-3.499840625" y1="-4.99859375" x2="-3.499840625" y2="-3.9902125" width="0.4064" layer="21"/>
<wire x1="-1.696821875" y1="8.039175" x2="-1.696821875" y2="5.041975" width="0.0508" layer="51"/>
<wire x1="-1.696821875" y1="8.039175" x2="1.706778125" y2="8.039175" width="0.0508" layer="21"/>
<wire x1="1.706778125" y1="8.039175" x2="1.706778125" y2="5.041975" width="0.0508" layer="51"/>
<circle x="0.004978125" y="6.540575" radius="1.2065" width="0.0508" layer="51"/>
<smd name="A10" x="1.624228125" y="5.791275" dx="0.889" dy="2.032" layer="1" rot="R90"/>
<smd name="A11" x="1.624228125" y="7.289875" dx="0.889" dy="2.032" layer="1" rot="R90"/>
<smd name="A12" x="-1.614271875" y="7.289875" dx="0.889" dy="2.032" layer="1" rot="R90"/>
<smd name="C3" x="-1.614271875" y="5.791275" dx="0.889" dy="2.032" layer="1" rot="R90"/>
<wire x1="-1.696821875" y1="5.041975" x2="1.706778125" y2="5.041975" width="0.0508" layer="21"/>
<wire x1="0.614578125" y1="8.039175" x2="0.614578125" y2="5.041975" width="0.0508" layer="51"/>
<wire x1="-0.604621875" y1="8.039175" x2="-0.604621875" y2="5.041975" width="0.0508" layer="51"/>
<wire x1="-0.322653125" y1="6.272428125" x2="-0.322653125" y2="5.264046875" width="0.4064" layer="21"/>
<wire x1="5.31281875" y1="-3.626459375" x2="2.31561875" y2="-3.626459375" width="0.0508" layer="51"/>
<wire x1="5.31281875" y1="-3.626459375" x2="5.31281875" y2="-7.030059375" width="0.0508" layer="21"/>
<wire x1="5.31281875" y1="-7.030059375" x2="2.31561875" y2="-7.030059375" width="0.0508" layer="51"/>
<circle x="3.81421875" y="-5.328259375" radius="1.2065" width="0.0508" layer="51"/>
<smd name="A13" x="3.06491875" y="-6.947509375" dx="0.889" dy="2.032" layer="1"/>
<smd name="A14" x="4.56351875" y="-6.947509375" dx="0.889" dy="2.032" layer="1"/>
<smd name="A15" x="4.56351875" y="-3.709009375" dx="0.889" dy="2.032" layer="1"/>
<smd name="C4" x="3.06491875" y="-3.709009375" dx="0.889" dy="2.032" layer="1"/>
<wire x1="2.31561875" y1="-3.626459375" x2="2.31561875" y2="-7.030059375" width="0.0508" layer="21"/>
<wire x1="5.31281875" y1="-5.937859375" x2="2.31561875" y2="-5.937859375" width="0.0508" layer="51"/>
<wire x1="5.31281875" y1="-4.718659375" x2="2.31561875" y2="-4.718659375" width="0.0508" layer="51"/>
<wire x1="3.531209375" y1="-5.007203125" x2="2.522828125" y2="-5.007203125" width="0.4064" layer="21"/>
<wire x1="7.717384375" y1="3.613046875" x2="4.720184375" y2="3.613046875" width="0.0508" layer="51"/>
<wire x1="7.717384375" y1="3.613046875" x2="7.717384375" y2="0.209446875" width="0.0508" layer="21"/>
<wire x1="7.717384375" y1="0.209446875" x2="4.720184375" y2="0.209446875" width="0.0508" layer="51"/>
<circle x="6.218784375" y="1.911246875" radius="1.2065" width="0.0508" layer="51"/>
<smd name="A16" x="5.469484375" y="0.291996875" dx="0.889" dy="2.032" layer="1"/>
<smd name="A17" x="6.968084375" y="0.291996875" dx="0.889" dy="2.032" layer="1"/>
<smd name="A18" x="6.968084375" y="3.530496875" dx="0.889" dy="2.032" layer="1"/>
<smd name="C5" x="5.469484375" y="3.530496875" dx="0.889" dy="2.032" layer="1"/>
<wire x1="4.720184375" y1="3.613046875" x2="4.720184375" y2="0.209446875" width="0.0508" layer="21"/>
<wire x1="7.717384375" y1="1.301646875" x2="4.720184375" y2="1.301646875" width="0.0508" layer="51"/>
<wire x1="7.717384375" y1="2.520846875" x2="4.720184375" y2="2.520846875" width="0.0508" layer="51"/>
<wire x1="5.943703125" y1="2.2233625" x2="4.935321875" y2="2.2233625" width="0.4064" layer="21"/>
<wire x1="-6.491553125" y1="1.90863125" x2="-5.951803125" y2="1.90863125" width="0.0508" layer="51"/>
<wire x1="-6.205803125" y1="2.16263125" x2="-6.205803125" y2="1.65463125" width="0.0508" layer="51"/>
<wire x1="-4.06514375" y1="-5.32158125" x2="-3.55714375" y2="-5.32158125" width="0.0508" layer="51"/>
<wire x1="-3.81114375" y1="-5.60733125" x2="-3.81114375" y2="-5.06758125" width="0.0508" layer="51"/>
<wire x1="3.81421875" y1="-5.582259375" x2="3.81421875" y2="-5.074259375" width="0.0508" layer="51"/>
<wire x1="4.09996875" y1="-5.328259375" x2="3.56021875" y2="-5.328259375" width="0.0508" layer="51"/>
<wire x1="0.254" y1="0" x2="-0.254" y2="0" width="0.0508" layer="51"/>
<wire x1="0" y1="0.28575" x2="0" y2="-0.254" width="0.0508" layer="51"/>
<wire x1="0.258978125" y1="6.540575" x2="-0.249021875" y2="6.540575" width="0.0508" layer="51"/>
<wire x1="0.004978125" y1="6.826325" x2="0.004978125" y2="6.286575" width="0.0508" layer="51"/>
<wire x1="6.218784375" y1="1.657246875" x2="6.218784375" y2="2.165246875" width="0.0508" layer="51"/>
<wire x1="6.504534375" y1="1.911246875" x2="5.964784375" y2="1.911246875" width="0.0508" layer="51"/>
<circle x="-6.223" y="1.905" radius="0.3127" width="0.0508" layer="51"/>
<circle x="6.223" y="1.905" radius="0.3127" width="0.0508" layer="51"/>
<circle x="3.81" y="-5.334" radius="0.3127" width="0.0508" layer="51"/>
<circle x="-3.81" y="-5.334" radius="0.3127" width="0.0508" layer="51"/>
<circle x="0" y="0" radius="0.3127" width="0.0508" layer="51"/>
<circle x="0" y="6.5405" radius="0.3127" width="0.0508" layer="51"/>
</package>
<package name="LED_1206_PIPE">
<wire x1="-2.6" y1="2.59" x2="2.59" y2="2.59" width="0.1" layer="21"/>
<wire x1="2.59" y1="2.59" x2="2.58" y2="-2.64" width="0.1" layer="21"/>
<wire x1="2.58" y1="-2.64" x2="-2.61" y2="-2.64" width="0.1" layer="21"/>
<wire x1="-2.61" y1="-2.64" x2="-2.6" y2="2.59" width="0.1" layer="21"/>
<hole x="-1.89" y="1.89" drill="1"/>
<hole x="1.89" y="-1.89" drill="1"/>
<smd name="A" x="-1.4" y="0" dx="1.6764" dy="1.6002" layer="1"/>
<smd name="C" x="1.4" y="0" dx="1.6764" dy="1.6002" layer="1"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<wire x1="0.762" y1="-1.7145" x2="0.127" y2="-1.7145" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-2.3495" x2="-0.889" y2="-1.0795" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-1.0795" x2="0.127" y2="-1.7145" width="0.1524" layer="21"/>
<wire x1="0.127" y1="-1.7145" x2="-0.889" y2="-2.3495" width="0.1524" layer="21"/>
<wire x1="0.127" y1="-2.3495" x2="0.127" y2="-1.7145" width="0.1524" layer="21"/>
<wire x1="0.127" y1="-1.7145" x2="0.127" y2="-1.0795" width="0.1524" layer="21"/>
<wire x1="-0.9525" y1="-1.7145" x2="-1.651" y2="-1.7145" width="0.1524" layer="21"/>
</package>
<package name="PWR_JACK_LRG-B">
<description>2.1X5.5MM</description>
<wire x1="7.14" y1="-2.01" x2="7.14" y2="-3.7" width="0.2032" layer="21"/>
<wire x1="7.2" y1="3.77" x2="7.2" y2="2.17" width="0.2032" layer="21"/>
<wire x1="5.6" y1="3.77" x2="7.2" y2="3.77" width="0.2032" layer="21"/>
<text x="3.259" y="-2.669" size="0.8128" layer="25" rot="R180">&gt;NAME</text>
<wire x1="-6.35" y1="-4.5" x2="-6.35" y2="4.5" width="0.2032" layer="51"/>
<wire x1="8" y1="4.53" x2="-6.3" y2="4.53" width="0.2032" layer="51"/>
<wire x1="7.94" y1="-4.5" x2="-6.36" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="7.99" y1="-4.44" x2="7.99" y2="4.56" width="0.2032" layer="21"/>
<hole x="-1.35" y="0" drill="1.6"/>
<hole x="3.15" y="0" drill="1.6"/>
<wire x1="5.51" y1="-3.71" x2="7.14" y2="-3.7" width="0.2032" layer="21"/>
<wire x1="8.636" y1="0.4445" x2="9.7155" y2="-0.635" width="0.254" layer="21" curve="90"/>
<circle x="9.7155" y="0.508" radius="0.5388125" width="0.254" layer="21"/>
<wire x1="9.7155" y1="-0.635" x2="10.795" y2="0.4445" width="0.254" layer="21" curve="90"/>
<wire x1="9.7155" y1="1.0795" x2="9.7155" y2="1.9685" width="0.254" layer="21"/>
<wire x1="9.7155" y1="-0.635" x2="9.7155" y2="-1.524" width="0.254" layer="21"/>
<wire x1="9.144" y1="-2.794" x2="10.2235" y2="-2.794" width="0.254" layer="21"/>
<wire x1="9.7155" y1="-3.3655" x2="9.7155" y2="-2.2225" width="0.254" layer="21"/>
<wire x1="9.7155" y1="2.794" x2="9.7155" y2="3.6195" width="0.254" layer="21"/>
<smd name="+@1" x="-1.2065" y="-5.9" dx="3.048" dy="2.794" layer="1"/>
<smd name="+@2" x="4.6355" y="-5.8365" dx="3.048" dy="2.794" layer="1"/>
<smd name="-@1" x="-1.2065" y="5.9" dx="3.048" dy="2.794" layer="1"/>
<smd name="-@2" x="4.6355" y="5.9" dx="3.048" dy="2.794" layer="1"/>
</package>
<package name="PWR_JACK_LRG-A">
<wire x1="6.0859" y1="-1.994759375" x2="6.0859" y2="-3.684759375" width="0.2032" layer="21"/>
<wire x1="6.1459" y1="3.785240625" x2="6.1459" y2="2.185240625" width="0.2032" layer="21"/>
<wire x1="4.5459" y1="3.785240625" x2="6.1459" y2="3.785240625" width="0.2032" layer="21"/>
<text x="0" y="0.002540625" size="0.8128" layer="25" rot="R270">&gt;NAME</text>
<wire x1="-7.4041" y1="-4.484759375" x2="-7.4041" y2="4.515240625" width="0.2032" layer="51"/>
<wire x1="7.4041" y1="4.545240625" x2="-7.3541" y2="4.545240625" width="0.2032" layer="51"/>
<wire x1="7.4041" y1="-4.484759375" x2="-7.4141" y2="-4.484759375" width="0.2032" layer="51"/>
<wire x1="7.4041" y1="-4.424759375" x2="7.4041" y2="4.575240625" width="0.2032" layer="21"/>
<hole x="-2.4041" y="0.015240625" drill="1.6"/>
<hole x="2.0959" y="0.015240625" drill="1.6"/>
<wire x1="4.4559" y1="-3.694759375" x2="6.0859" y2="-3.684759375" width="0.2032" layer="21"/>
<wire x1="8.0264" y1="0.142240625" x2="9.1059" y2="-0.937259375" width="0.254" layer="21" curve="90"/>
<circle x="9.1059" y="0.205740625" radius="0.5388125" width="0.254" layer="21"/>
<wire x1="9.1059" y1="-0.937259375" x2="10.1854" y2="0.142240625" width="0.254" layer="21" curve="90"/>
<wire x1="9.1059" y1="0.777240625" x2="9.1059" y2="1.666240625" width="0.254" layer="21"/>
<wire x1="9.1059" y1="-0.937259375" x2="9.1059" y2="-1.826259375" width="0.254" layer="21"/>
<wire x1="8.5344" y1="3.253740625" x2="9.6139" y2="3.253740625" width="0.254" layer="21"/>
<wire x1="9.1059" y1="2.682240625" x2="9.1059" y2="3.825240625" width="0.254" layer="21"/>
<wire x1="9.1059" y1="-3.858259375" x2="9.1059" y2="-3.032759375" width="0.254" layer="21"/>
<smd name="+@1" x="-2.4003" y="5.915240625" dx="3.048" dy="2.794" layer="1"/>
<smd name="+@2" x="3.6957" y="5.915240625" dx="3.048" dy="2.794" layer="1"/>
<smd name="-@1" x="-2.4003" y="-5.884759375" dx="3.048" dy="2.794" layer="1"/>
<smd name="-@2" x="3.6957" y="-5.884759375" dx="3.048" dy="2.794" layer="1"/>
</package>
<package name="FUSE_PTGL">
<description>PTGL Thermistor Fuse (Through Hole) 10mmL x 6mmW x 20mmH, 14mmD</description>
<pad name="1" x="0" y="0" drill="0.8"/>
<pad name="2" x="9.9822" y="0" drill="0.8"/>
<wire x1="0" y1="1.27" x2="9.9695" y2="1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="9.9695" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="1.27" x2="0" y2="-1.27" width="0.127" layer="21" curve="180"/>
<wire x1="9.9695" y1="-1.27" x2="9.9695" y2="1.27" width="0.127" layer="21" curve="180"/>
<text x="2.54" y="1.4605" size="0.508" layer="21">&gt;NAME</text>
<text x="2.4765" y="-2.032" size="0.508" layer="21">&gt;VALUE</text>
</package>
<package name="TH_0.2&quot;">
<description>Generic Through-Hole - 0.2" Pitch</description>
<pad name="1" x="-2.54" y="0" drill="0.8"/>
<pad name="2" x="2.54" y="0" drill="0.8"/>
<wire x1="-4.445" y1="1.27" x2="-4.445" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-4.445" y1="-1.27" x2="4.445" y2="-1.27" width="0.127" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="4.445" y2="1.27" width="0.127" layer="21"/>
<wire x1="4.445" y1="1.27" x2="-4.445" y2="1.27" width="0.127" layer="21"/>
<text x="-4.1275" y="1.5875" size="0.6096" layer="25">&gt;NAME</text>
</package>
<package name="SSQ">
<description>SSQ Fuse Layout</description>
<smd name="1" x="-2.7305" y="0" dx="3.175" dy="2.286" layer="1" rot="R90"/>
<smd name="2" x="2.921" y="0" dx="3.175" dy="2.286" layer="1" rot="R90"/>
<wire x1="-4.2545" y1="1.905" x2="4.3815" y2="1.905" width="0.127" layer="21"/>
<wire x1="4.3815" y1="1.905" x2="4.3815" y2="-1.905" width="0.127" layer="21"/>
<wire x1="4.3815" y1="-1.905" x2="-4.2545" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-4.2545" y1="-1.905" x2="-4.2545" y2="1.905" width="0.127" layer="21"/>
<text x="-4.064" y="2.159" size="0.6096" layer="21">&gt;NAME</text>
</package>
<package name="USB">
<description>TH USB layout based off of 87520-0010BLF footprint.</description>
<wire x1="-6.35" y1="-8.89" x2="6.78941875" y2="-8.89" width="0.127" layer="51"/>
<wire x1="-6.35" y1="-8.89" x2="-6.35" y2="-3.81" width="0.127" layer="51"/>
<wire x1="-6.35" y1="6.830059375" x2="6.78941875" y2="6.830059375" width="0.127" layer="21"/>
<wire x1="6.78941875" y1="-8.89" x2="6.78941875" y2="-3.81" width="0.127" layer="51"/>
<pad name="SH1" x="-6.35" y="1.397" drill="2.301240625"/>
<pad name="SH2" x="6.78941875" y="1.397" drill="2.2987"/>
<pad name="4" x="3.719703125" y="5.08" drill="0.949959375"/>
<pad name="2" x="-0.778509375" y="5.08" drill="0.949959375"/>
<pad name="3" x="1.21793125" y="5.08" drill="0.949959375"/>
<pad name="1" x="-3.280284375" y="5.08" drill="0.949959375"/>
<wire x1="-6.35" y1="-3.81" x2="6.78941875" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-6.35" y1="3.81" x2="-6.35" y2="-0.635" width="0.127" layer="51"/>
<wire x1="6.78941875" y1="3.81" x2="6.78941875" y2="-0.635" width="0.127" layer="51"/>
<wire x1="-6.35" y1="6.830059375" x2="-6.35" y2="3.81" width="0.127" layer="21"/>
<wire x1="6.78941875" y1="6.830059375" x2="6.78941875" y2="3.81" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-3.81" x2="-6.35" y2="-0.635" width="0.127" layer="21"/>
<wire x1="6.78941875" y1="-3.81" x2="6.78941875" y2="-0.635" width="0.127" layer="21"/>
<text x="-2.5908" y="7.0104" size="0.508" layer="25">&gt;NAME</text>
<rectangle x1="-7.112" y1="6.096" x2="7.62" y2="7.62" layer="39"/>
<rectangle x1="-7.112" y1="-8.89" x2="7.366" y2="-0.508" layer="39"/>
<rectangle x1="-4.318" y1="-0.508" x2="4.826" y2="3.81" layer="39"/>
<rectangle x1="-7.112" y1="3.302" x2="-4.318" y2="6.096" layer="39"/>
<rectangle x1="4.826" y1="3.302" x2="7.62" y2="6.096" layer="39"/>
</package>
</packages>
<symbols>
<symbol name="TUSB2046BVF">
<pin name="VCC" x="-22.86" y="38.1" length="short" direction="pwr"/>
<pin name="!RESET" x="-22.86" y="-7.62" length="short" direction="in"/>
<pin name="!OVRCUR1" x="22.86" y="-33.02" length="short" direction="in" rot="R180"/>
<pin name="!OVRCUR2" x="22.86" y="-35.56" length="short" direction="in" rot="R180"/>
<pin name="!OVRCUR3" x="22.86" y="-38.1" length="short" direction="in" rot="R180"/>
<pin name="!OVRCUR4" x="22.86" y="-40.64" length="short" direction="in" rot="R180"/>
<pin name="TSTMODE" x="-22.86" y="-25.4" length="short" direction="in"/>
<pin name="BUSPWR" x="-22.86" y="-22.86" length="short" direction="in"/>
<pin name="!EXTMEM" x="-22.86" y="-10.16" length="short" direction="in"/>
<pin name="XTAL1" x="-22.86" y="10.16" length="short" direction="in"/>
<pin name="TSTPLL/48MCLK" x="-22.86" y="-27.94" length="short"/>
<pin name="EEDATA/GANGED" x="-22.86" y="-15.24" length="short"/>
<pin name="GND" x="-22.86" y="-40.64" length="short" direction="pas"/>
<pin name="SUSPND" x="-22.86" y="-35.56" length="short" direction="out"/>
<pin name="EECLK" x="-22.86" y="-12.7" length="short" direction="out"/>
<pin name="XTAL2" x="-22.86" y="2.54" length="short" direction="out"/>
<pin name="!PWRON1" x="22.86" y="-17.78" length="short" direction="out" rot="R180"/>
<pin name="!PWRON2" x="22.86" y="-20.32" length="short" direction="out" rot="R180"/>
<pin name="!PWRON3" x="22.86" y="-22.86" length="short" direction="out" rot="R180"/>
<pin name="!PWRON4" x="22.86" y="-25.4" length="short" direction="out" rot="R180"/>
<pin name="DP0" x="-22.86" y="25.4" length="short"/>
<pin name="DP1" x="22.86" y="38.1" length="short" rot="R180"/>
<pin name="DP2" x="22.86" y="25.4" length="short" rot="R180"/>
<pin name="DP3" x="22.86" y="12.7" length="short" rot="R180"/>
<pin name="DP4" x="22.86" y="-2.54" length="short" rot="R180"/>
<pin name="DM0" x="-22.86" y="22.86" length="short"/>
<pin name="DM1" x="22.86" y="35.56" length="short" rot="R180"/>
<pin name="DM2" x="22.86" y="22.86" length="short" rot="R180"/>
<pin name="DM3" x="22.86" y="10.16" length="short" rot="R180"/>
<pin name="DM4" x="22.86" y="-5.08" length="short" rot="R180"/>
<wire x1="-20.32" y1="40.64" x2="-20.32" y2="-43.18" width="0.4064" layer="94"/>
<wire x1="-20.32" y1="-43.18" x2="20.32" y2="-43.18" width="0.4064" layer="94"/>
<wire x1="20.32" y1="-43.18" x2="20.32" y2="40.64" width="0.4064" layer="94"/>
<wire x1="20.32" y1="40.64" x2="-20.32" y2="40.64" width="0.4064" layer="94"/>
<text x="-18.1864" y="42.8498" size="2.0828" layer="95" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-16.8148" y="-46.3042" size="2.0828" layer="96" ratio="10" rot="SR0">&gt;VALUE</text>
</symbol>
<symbol name="TPS2044BDR">
<pin name="!EN1" x="-10.16" y="12.7" length="short"/>
<pin name="!EN2" x="-10.16" y="10.16" length="short"/>
<pin name="!EN3" x="-10.16" y="7.62" length="short"/>
<pin name="!EN4" x="-10.16" y="5.08" length="short"/>
<pin name="!OC1" x="-10.16" y="-2.54" length="short"/>
<pin name="!OC2" x="-10.16" y="-5.08" length="short"/>
<pin name="!OC3" x="-10.16" y="-7.62" length="short"/>
<pin name="!OC4" x="-10.16" y="-10.16" length="short"/>
<pin name="IN" x="-10.16" y="-15.24" length="short"/>
<pin name="GND" x="12.7" y="-15.24" length="short" rot="R180"/>
<pin name="OUT1" x="12.7" y="12.7" length="short" rot="R180"/>
<pin name="OUT2" x="12.7" y="7.62" length="short" rot="R180"/>
<pin name="OUT3" x="12.7" y="2.54" length="short" rot="R180"/>
<pin name="OUT4" x="12.7" y="-2.54" length="short" rot="R180"/>
<wire x1="-7.62" y1="15.24" x2="-7.62" y2="-17.78" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-17.78" x2="10.16" y2="-17.78" width="0.254" layer="94"/>
<wire x1="10.16" y1="-17.78" x2="10.16" y2="15.24" width="0.254" layer="94"/>
<wire x1="10.16" y1="15.24" x2="-7.62" y2="15.24" width="0.254" layer="94"/>
<text x="-7.366" y="16.51" size="1.27" layer="95">&gt;NAME</text>
<text x="8.128" y="-18.542" size="1.27" layer="95" rot="R180">&gt;VALUE</text>
</symbol>
<symbol name="USB-DUAL">
<pin name="VBUS_B" x="-10.16" y="-2.54" length="short"/>
<pin name="D-_B" x="-10.16" y="-7.62" length="short"/>
<pin name="D+_B" x="-10.16" y="-10.16" length="short"/>
<pin name="GND_B" x="-10.16" y="-15.24" length="short"/>
<pin name="SHIELD" x="5.08" y="-20.32" length="short" rot="R90"/>
<wire x1="-7.62" y1="20.32" x2="-7.62" y2="1.27" width="0.254" layer="94"/>
<wire x1="-7.62" y1="1.27" x2="-7.62" y2="-17.78" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-17.78" x2="7.62" y2="-17.78" width="0.254" layer="94"/>
<wire x1="7.62" y1="-17.78" x2="7.62" y2="1.016" width="0.254" layer="94"/>
<wire x1="7.62" y1="1.016" x2="7.62" y2="20.32" width="0.254" layer="94"/>
<wire x1="7.62" y1="20.32" x2="-7.62" y2="20.32" width="0.254" layer="94"/>
<text x="-5.588" y="21.59" size="1.778" layer="95">&gt;NAME</text>
<text x="10.922" y="-14.732" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VBUS_T" x="-10.16" y="17.78" length="short"/>
<pin name="D-_T" x="-10.16" y="12.7" length="short"/>
<pin name="D+_T" x="-10.16" y="10.16" length="short"/>
<pin name="GND_T" x="-10.16" y="5.08" length="short"/>
<wire x1="-7.62" y1="1.27" x2="7.62" y2="1.27" width="0.254" layer="94" style="shortdash"/>
<wire x1="7.62" y1="1.27" x2="7.62" y2="1.016" width="0.254" layer="94" style="shortdash"/>
</symbol>
<symbol name="SN75240">
<pin name="A" x="-2.54" y="10.16" visible="pad" length="short" rot="R270"/>
<pin name="C" x="-2.54" y="-10.16" visible="pad" length="short" rot="R90"/>
<pin name="B" x="2.54" y="10.16" visible="pad" length="short" rot="R270"/>
<pin name="D" x="2.54" y="-10.16" visible="pad" length="short" rot="R90"/>
<pin name="GND" x="-7.62" y="0" visible="pad" length="short"/>
<pin name="GND1" x="7.62" y="0" visible="pad" length="short" rot="R180"/>
<wire x1="-2.54" y1="-5.08" x2="-4.064" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-4.064" y1="-2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-1.016" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.016" y1="-2.54" x2="-2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-4.064" y1="-5.08" x2="-2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-1.016" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-4.064" y1="-5.08" x2="-4.572" y2="-5.588" width="0.254" layer="94"/>
<wire x1="-1.016" y1="-5.08" x2="-0.508" y2="-4.572" width="0.254" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-1.016" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.016" y1="2.54" x2="-2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-4.064" y2="2.54" width="0.254" layer="94"/>
<wire x1="-4.064" y1="2.54" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="-1.016" y1="5.08" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-4.064" y2="5.08" width="0.254" layer="94"/>
<wire x1="-1.016" y1="5.08" x2="-0.508" y2="5.588" width="0.254" layer="94"/>
<wire x1="-4.064" y1="5.08" x2="-4.572" y2="4.572" width="0.254" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="1.016" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.016" y1="-2.54" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="4.064" y2="-2.54" width="0.254" layer="94"/>
<wire x1="4.064" y1="-2.54" x2="2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="1.016" y1="-5.08" x2="2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="4.064" y2="-5.08" width="0.254" layer="94"/>
<wire x1="1.016" y1="-5.08" x2="0.508" y2="-5.588" width="0.254" layer="94"/>
<wire x1="4.064" y1="-5.08" x2="4.572" y2="-4.572" width="0.254" layer="94"/>
<wire x1="2.54" y1="5.08" x2="4.064" y2="2.54" width="0.254" layer="94"/>
<wire x1="4.064" y1="2.54" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="1.016" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.016" y1="2.54" x2="2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="4.064" y1="5.08" x2="2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="5.08" x2="1.016" y2="5.08" width="0.254" layer="94"/>
<wire x1="4.064" y1="5.08" x2="4.572" y2="5.588" width="0.254" layer="94"/>
<wire x1="1.016" y1="5.08" x2="0.508" y2="4.572" width="0.254" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="-5.08" y2="0" width="0.254" layer="94"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="7.62" x2="-5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="-7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="2.54" y2="-7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="1.016" width="0.254" layer="94"/>
<text x="-4.318" y="5.842" size="1.27" layer="94">A</text>
<text x="4.318" y="7.112" size="1.27" layer="94" rot="R180">B</text>
<text x="-4.318" y="-7.112" size="1.27" layer="94">C</text>
<text x="3.302" y="-7.112" size="1.27" layer="94">D</text>
<text x="6.096" y="6.604" size="1.27" layer="95">&gt;NAME</text>
<text x="-6.35" y="2.54" size="1.016" layer="95" rot="R90">&gt;VALUE</text>
<wire x1="-2.54" y1="1.016" x2="2.54" y2="1.016" width="0.254" layer="94"/>
<wire x1="2.54" y1="1.016" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="7.62" x2="5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="7.62" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="-7.62" x2="-5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-1.016" x2="2.54" y2="-1.016" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.016" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-5.08" y1="0" x2="-2.54" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="1.016" width="0.254" layer="94"/>
</symbol>
<symbol name="GND">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.905" y="-3.175" size="1.016" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="TLV72033-3.3">
<wire x1="-10.16" y1="-7.62" x2="12.7" y2="-7.62" width="0.254" layer="94"/>
<wire x1="12.7" y1="-7.62" x2="12.7" y2="7.62" width="0.254" layer="94"/>
<wire x1="12.7" y1="7.62" x2="-10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="7.62" x2="-10.16" y2="-7.62" width="0.254" layer="94"/>
<text x="-10.16" y="8.89" size="1.27" layer="95">&gt;NAME</text>
<text x="-10.16" y="-10.16" size="1.27" layer="96">&gt;VALUE</text>
<pin name="VIN" x="-15.24" y="5.08" length="middle"/>
<pin name="GND" x="-15.24" y="-5.08" length="middle"/>
<pin name="EN" x="-15.24" y="0" length="middle"/>
<pin name="NC/FB" x="17.78" y="-5.08" length="middle" rot="R180"/>
<pin name="VOUT" x="17.78" y="5.08" length="middle" rot="R180"/>
</symbol>
<symbol name="CAP_POL">
<description>Polarized Capacitor</description>
<wire x1="-2.54" y1="-0.127" x2="2.54" y2="-0.127" width="0.254" layer="94"/>
<wire x1="0" y1="-1.016" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1" x2="2.4892" y2="-1.8542" width="0.254" layer="94" curve="-37.878202" cap="flat"/>
<wire x1="-2.4669" y1="-1.8504" x2="0" y2="-1.0161" width="0.254" layer="94" curve="-37.376341" cap="flat"/>
<text x="-1.524" y="-2.667" size="1.27" layer="95" rot="R180">&gt;NAME</text>
<text x="5.207" y="-5.334" size="1.016" layer="96" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.253" y1="0.668" x2="-1.364" y2="0.795" layer="94"/>
<rectangle x1="-1.872" y1="0.287" x2="-1.745" y2="1.176" layer="94"/>
<pin name="+" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="-" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="CAP">
<wire x1="0" y1="0" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<text x="-1.27" y="1.397" size="1.27" layer="95" rot="R180">&gt;NAME</text>
<text x="4.445" y="-5.588" size="1.27" layer="96" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-2.032" x2="2.032" y2="-1.524" layer="94"/>
<rectangle x1="-2.032" y1="-1.016" x2="2.032" y2="-0.508" layer="94"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="RES">
<wire x1="-2.413" y1="1.016" x2="-1.778" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.778" y1="-1.016" x2="-1.143" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-1.143" y1="1.016" x2="-0.508" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-0.508" y1="-1.016" x2="0.127" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.127" y1="1.016" x2="0.762" y2="-1.016" width="0.1524" layer="94"/>
<text x="-3.81" y="1.7526" size="1.27" layer="95">&gt;NAME</text>
<text x="-6.096" y="-2.794" size="1.27" layer="96">&gt;VALUE</text>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
<wire x1="-3.048" y1="-1.016" x2="-2.413" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-3.048" y1="-1.016" x2="-3.556" y2="0" width="0.1524" layer="94"/>
<wire x1="-3.556" y1="0" x2="-5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="0.762" y1="-1.016" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
</symbol>
<symbol name="XTAL_OSC">
<description>Crystal Oscillator - no GND</description>
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="-1.778" width="0.254" layer="94"/>
<text x="-2.54" y="2.54" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.556" size="1.27" layer="96">&gt;VALUE</text>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="HDR_3X1">
<description>3x1 Header</description>
<wire x1="3.81" y1="-5.08" x2="-1.27" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="-1.27" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<text x="-1.27" y="-6.604" size="1.27" layer="96">&gt;VALUE</text>
<text x="-1.27" y="5.588" size="1.27" layer="95">&gt;NAME</text>
<pin name="3" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="TAC-SWITCH">
<wire x1="1.905" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="1.905" y1="4.445" x2="1.905" y2="3.175" width="0.254" layer="94"/>
<wire x1="-1.905" y1="4.445" x2="-1.905" y2="3.175" width="0.254" layer="94"/>
<wire x1="1.905" y1="4.445" x2="0" y2="4.445" width="0.254" layer="94"/>
<wire x1="0" y1="4.445" x2="-1.905" y2="4.445" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="94"/>
<wire x1="0" y1="4.445" x2="0" y2="3.175" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="1.905" y2="1.27" width="0.254" layer="94"/>
<circle x="-2.54" y="0" radius="0.127" width="0.4064" layer="94"/>
<circle x="2.54" y="0" radius="0.127" width="0.4064" layer="94"/>
<text x="-2.54" y="5.588" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.794" y="-2.032" size="1.016" layer="96">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="2"/>
<pin name="2" x="5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="TESTPOINT">
<pin name="1" x="0" y="-2.54" visible="off" length="short" rot="R90"/>
<circle x="0" y="1.27" radius="0.254" width="0.254" layer="94"/>
<circle x="0" y="1.27" radius="1.27" width="0.254" layer="94"/>
<text x="-1.016" y="3.302" size="1.27" layer="95">&gt;NAME</text>
</symbol>
<symbol name="LED">
<wire x1="1.27" y1="2.54" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="1.778" x2="-3.429" y2="0.381" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="0.635" x2="-3.302" y2="-0.762" width="0.1524" layer="94"/>
<text x="3.556" y="-2.032" size="1.27" layer="95" rot="R90">&gt;NAME</text>
<text x="5.461" y="-2.032" size="0.8128" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="0.381"/>
<vertex x="-3.048" y="1.27"/>
<vertex x="-2.54" y="0.762"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-0.762"/>
<vertex x="-2.921" y="0.127"/>
<vertex x="-2.413" y="-0.381"/>
</polygon>
</symbol>
<symbol name="POWER-JACK-CUI">
<wire x1="2.54" y1="2.54" x2="-5.08" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.778" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.778" y1="-2.54" x2="-2.54" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-1.016" x2="-3.302" y2="-2.54" width="0.1524" layer="94"/>
<text x="-5.08" y="3.81" size="1.27" layer="95">&gt;NAME</text>
<text x="2.794" y="-3.556" size="1.016" layer="96" rot="R180">&gt;VALUE</text>
<rectangle x1="-5.842" y1="-2.54" x2="-4.318" y2="1.27" layer="94"/>
<pin name="POS" x="5.08" y="2.54" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="GND" x="5.08" y="-2.54" visible="off" length="short" direction="pas" rot="R180"/>
<polygon width="0.1524" layer="94">
<vertex x="0" y="-2.54"/>
<vertex x="-0.508" y="-1.27"/>
<vertex x="0.508" y="-1.27"/>
</polygon>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.54" width="0.127" layer="94"/>
</symbol>
<symbol name="FUSE">
<description>Fuse/Thermistor</description>
<pin name="1" x="-5.08" y="0" visible="pad" length="short"/>
<pin name="2" x="7.62" y="0" visible="pad" length="short" rot="R180"/>
<text x="-2.286" y="3.048" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.54" y="-4.064" size="1.016" layer="96">&gt;VALUE</text>
<wire x1="-2.54" y1="0" x2="1.27" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="5.08" y1="0" x2="1.27" y2="0" width="0.254" layer="94" curve="-180"/>
</symbol>
<symbol name="USB">
<pin name="VBUS" x="-10.16" y="7.62" length="middle"/>
<pin name="D-" x="-10.16" y="2.54" length="middle"/>
<pin name="D+" x="-10.16" y="-2.54" length="middle"/>
<pin name="GND" x="-10.16" y="-7.62" length="middle"/>
<pin name="SHIELD" x="5.08" y="-15.24" length="middle" rot="R90"/>
<wire x1="-5.08" y1="10.16" x2="-5.08" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-10.16" x2="7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="-10.16" x2="7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="10.16" x2="-5.08" y2="10.16" width="0.254" layer="94"/>
<text x="-4.318" y="11.176" size="1.778" layer="95">&gt;NAME</text>
<text x="-0.762" y="-11.43" size="1.778" layer="96" rot="R180">&gt;VALUE</text>
</symbol>
<symbol name="HDR_1X1">
<description>Single Pin Header</description>
<wire x1="-1.27" y1="-2.54" x2="3.81" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="2.54" width="0.4064" layer="94"/>
<wire x1="3.81" y1="2.54" x2="-1.27" y2="2.54" width="0.4064" layer="94"/>
<wire x1="2.54" y1="0" x2="1.27" y2="0" width="0.6096" layer="94"/>
<text x="-2.032" y="-4.318" size="1.27" layer="96">&gt;VALUE</text>
<text x="-1.524" y="3.048" size="1.27" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="HDR_2X1">
<description>2x1 Header</description>
<wire x1="-1.27" y1="-2.54" x2="3.81" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="3.81" y1="5.08" x2="3.81" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="5.08" x2="-1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="2.54" y1="2.54" x2="1.27" y2="2.54" width="0.6096" layer="94"/>
<wire x1="2.54" y1="0" x2="1.27" y2="0" width="0.6096" layer="94"/>
<text x="-2.032" y="-4.318" size="1.27" layer="96">&gt;VALUE</text>
<text x="-1.524" y="5.588" size="1.27" layer="95">&gt;NAME</text>
<pin name="2" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TUSB2046BVF" prefix="U">
<description>&lt;b&gt;TUSB2046BVF 4-Port Hub USB Controller with Serial EEPROM&lt;/b&gt;

&lt;p&gt;3V ~ 3.6V, 40mA, USB 1.0, 32-LQFP</description>
<gates>
<gate name="G$1" symbol="TUSB2046BVF" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LQFP-32_7X7">
<connects>
<connect gate="G$1" pin="!EXTMEM" pad="26"/>
<connect gate="G$1" pin="!OVRCUR1" pad="10"/>
<connect gate="G$1" pin="!OVRCUR2" pad="14"/>
<connect gate="G$1" pin="!OVRCUR3" pad="18"/>
<connect gate="G$1" pin="!OVRCUR4" pad="22"/>
<connect gate="G$1" pin="!PWRON1" pad="9"/>
<connect gate="G$1" pin="!PWRON2" pad="13"/>
<connect gate="G$1" pin="!PWRON3" pad="17"/>
<connect gate="G$1" pin="!PWRON4" pad="21"/>
<connect gate="G$1" pin="!RESET" pad="4"/>
<connect gate="G$1" pin="BUSPWR" pad="8"/>
<connect gate="G$1" pin="DM0" pad="2"/>
<connect gate="G$1" pin="DM1" pad="11"/>
<connect gate="G$1" pin="DM2" pad="15"/>
<connect gate="G$1" pin="DM3" pad="19"/>
<connect gate="G$1" pin="DM4" pad="23"/>
<connect gate="G$1" pin="DP0" pad="1"/>
<connect gate="G$1" pin="DP1" pad="12"/>
<connect gate="G$1" pin="DP2" pad="16"/>
<connect gate="G$1" pin="DP3" pad="20"/>
<connect gate="G$1" pin="DP4" pad="24"/>
<connect gate="G$1" pin="EECLK" pad="5"/>
<connect gate="G$1" pin="EEDATA/GANGED" pad="6"/>
<connect gate="G$1" pin="GND" pad="7 28"/>
<connect gate="G$1" pin="SUSPND" pad="32"/>
<connect gate="G$1" pin="TSTMODE" pad="31"/>
<connect gate="G$1" pin="TSTPLL/48MCLK" pad="27"/>
<connect gate="G$1" pin="VCC" pad="3 25"/>
<connect gate="G$1" pin="XTAL1" pad="30"/>
<connect gate="G$1" pin="XTAL2" pad="29"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TPS2044BDR" prefix="U">
<description>&lt;b&gt;TPS2044BDR Quad Power-Distribution Switch&lt;/b&gt;

&lt;p&gt;2.7V ~ 5.5V, 500mA, 70mOhm Rds, N-Channel, High-Side, 16-SOIC</description>
<gates>
<gate name="G$1" symbol="TPS2044BDR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOIC-16">
<connects>
<connect gate="G$1" pin="!EN1" pad="3"/>
<connect gate="G$1" pin="!EN2" pad="4"/>
<connect gate="G$1" pin="!EN3" pad="7"/>
<connect gate="G$1" pin="!EN4" pad="8"/>
<connect gate="G$1" pin="!OC1" pad="16"/>
<connect gate="G$1" pin="!OC2" pad="13"/>
<connect gate="G$1" pin="!OC3" pad="12"/>
<connect gate="G$1" pin="!OC4" pad="9"/>
<connect gate="G$1" pin="GND" pad="1 5"/>
<connect gate="G$1" pin="IN" pad="2 6"/>
<connect gate="G$1" pin="OUT1" pad="15"/>
<connect gate="G$1" pin="OUT2" pad="14"/>
<connect gate="G$1" pin="OUT3" pad="11"/>
<connect gate="G$1" pin="OUT4" pad="10"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="USB-A_STACK" prefix="CON" uservalue="yes">
<description>&lt;b&gt;5787745-4 DUAL USB_A - STACK&lt;/b&gt;

&lt;p&gt;USB 2.0, 8-pin, TH-RA</description>
<gates>
<gate name="G$1" symbol="USB-DUAL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="USB-A_STACK">
<connects>
<connect gate="G$1" pin="D+_B" pad="B3"/>
<connect gate="G$1" pin="D+_T" pad="T3"/>
<connect gate="G$1" pin="D-_B" pad="B2"/>
<connect gate="G$1" pin="D-_T" pad="T2"/>
<connect gate="G$1" pin="GND_B" pad="B4"/>
<connect gate="G$1" pin="GND_T" pad="T4"/>
<connect gate="G$1" pin="SHIELD" pad="SH1 SH2"/>
<connect gate="G$1" pin="VBUS_B" pad="B1"/>
<connect gate="G$1" pin="VBUS_T" pad="T1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SN75240">
<description>&lt;b&gt;SN75240 USB Port Transient Suppressor&lt;/b&gt;

&lt;p&gt;6V - working, 7V - clamping, 60W, 8-TSSOP</description>
<gates>
<gate name="G$1" symbol="SN75240" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TSSOP-8_5.8MM">
<connects>
<connect gate="G$1" pin="A" pad="8"/>
<connect gate="G$1" pin="B" pad="6"/>
<connect gate="G$1" pin="C" pad="2"/>
<connect gate="G$1" pin="D" pad="4"/>
<connect gate="G$1" pin="GND" pad="5 7"/>
<connect gate="G$1" pin="GND1" pad="1 3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL</description>
<gates>
<gate name="G$1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TLV70233-3.3" prefix="U">
<description>&lt;b&gt;TI Voltage Regulators (LDO)&lt;/b&gt;

&lt;p&gt;TLV72033 - IC REG LDO 3.3V 0.3A SOT23-5
&lt;p&gt;TLV71033 - 3.3Vout, 150mA SOT23-5</description>
<gates>
<gate name="TLV72033" symbol="TLV72033-3.3" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23-5">
<connects>
<connect gate="TLV72033" pin="EN" pad="3"/>
<connect gate="TLV72033" pin="GND" pad="2"/>
<connect gate="TLV72033" pin="NC/FB" pad="4"/>
<connect gate="TLV72033" pin="VIN" pad="1"/>
<connect gate="TLV72033" pin="VOUT" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP_POL" prefix="C" uservalue="yes">
<description>Polarized Capacitors (Aluminum/Tantalum)

&lt;p&gt;Aluminum
&lt;br&gt;EEE-FPE101XAP : 100uF 25V 20% 160m Ohm (6.6mm x 6.6mm)
&lt;br&gt;EEE-1EA100WR : 10uF 25V 20% (4.3mm x 4.3mm)
&lt;br&gt;EEE-FC1V6R8AR : 6.8uF 35V 20% 1.8 Ohms, 95mA Irip (5.3mm x 5.3mm)

&lt;p&gt;Tantalum
&lt;br&gt;T491B334M050AT : 0.33uF, 10%, 50V, 10 Ohm, SMD-1411</description>
<gates>
<gate name="G$1" symbol="CAP_POL" x="0" y="0"/>
</gates>
<devices>
<device name="_6.6" package="153CLV-1012_6.6X6.6">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_4.3" package="153CLV-1012_4.3X4.3">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_5.3" package="153CLV-1012_5.3X5.3">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_2312" package="SMD-2312(6032)">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_1206" package="SMD-1206">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_1411" package="SMD-1411_POL">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_2917" package="SMD-2917">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP" prefix="C" uservalue="yes">
<description>Capacitors - SMD and Through-Hole</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="_0402" package="SMD-0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_1206" package="SMD-1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0805" package="SMD-0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_2512" package="SMD-2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0603" package="SMD-0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_1812" package="SMD-1812">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_TH886" package="CAP_TH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RES" prefix="R" uservalue="yes">
<description>Resistors - SMD and Through-Hole</description>
<gates>
<gate name="G$1" symbol="RES" x="0" y="0"/>
</gates>
<devices>
<device name="_0402" package="SMD-0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_1206" package="SMD-1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0805" package="SMD-0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_2512" package="SMD-2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0603" package="SMD-0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_2010" package="SMD-2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_TALL" package="RES_TH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="XTAL_OSC" prefix="Y" uservalue="yes">
<description>Crystal Oscillators with no GND</description>
<gates>
<gate name="G$1" symbol="XTAL_OSC" x="0" y="0"/>
</gates>
<devices>
<device name="_7XC-7A" package="XTAL_7XC-7A">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_HC49UP" package="XTAL_HC49UP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_HC49US" package="XTAL_HC49US">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_HC49UV" package="XTAL_HC49UV">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_10.5X4.8" package="XTAL_SMD_10.5X4.8">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_TC26H" package="XTAL_TC26H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_TC38H" package="XTAL_TC38H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_11.5X5" package="XTAL_TH_11.5X5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_CM2X0C" package="XTAL_CM2X0C">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_4SMD" package="XTAL_4-SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HDR_3X1" prefix="H" uservalue="yes">
<description>3x1 Header</description>
<gates>
<gate name="HDR_3X1" symbol="HDR_3X1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="HDR_3X1">
<connects>
<connect gate="HDR_3X1" pin="1" pad="1"/>
<connect gate="HDR_3X1" pin="2" pad="2"/>
<connect gate="HDR_3X1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SW_TACTILE" prefix="SW" uservalue="yes">
<description>Switches - Push, Slide, Toggle

&lt;p&gt;K12A: MOM SPST-NO, 100mA 30VDC, 50mOhm RON
&lt;p&gt;PTS525: MOM SPST-NO, 50mA 12VDC, 100mOhm RON
&lt;p&gt;PTS645: MOM SPST-NO, 50mA 12Vdc, 6mm x 6mm
&lt;p&gt;KMR231GLFS: MOM, SPST-NO, 0.05A @ 32VDC, 100mOhm RON</description>
<gates>
<gate name="G$1" symbol="TAC-SWITCH" x="0" y="0"/>
</gates>
<devices>
<device name="_K12A" package="SW_K12A">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PART#" value="CKN10274-ND"/>
<attribute name="SUPPLIER" value="Digikey"/>
</technology>
</technologies>
</device>
<device name="_PTS525" package="SM08">
<connects>
<connect gate="G$1" pin="1" pad="A1 A2"/>
<connect gate="G$1" pin="2" pad="B1 B2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_1825" package="RST_TAC_SW-TH_RA">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_KMR" package="SW_KMR">
<connects>
<connect gate="G$1" pin="1" pad="1 2"/>
<connect gate="G$1" pin="2" pad="3 4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_PTS645" package="PTS645_6MM">
<connects>
<connect gate="G$1" pin="1" pad="A1 A2"/>
<connect gate="G$1" pin="2" pad="B1 B2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TESTPOINT" prefix="TP" uservalue="yes">
<description>1-pin header test point</description>
<gates>
<gate name="G$1" symbol="TESTPOINT" x="0" y="0"/>
</gates>
<devices>
<device name="_TH" package="HDR_1X1">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_SMD" package="HDR_1X1_SMD">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED" prefix="LD" uservalue="yes">
<description>LEDs - Catalog of LED types</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="_0402" package="SMD-0402">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_TH1" package="LED_5MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_ARRAY_9" package="LED_ARRAY_9">
<connects>
<connect gate="G$1" pin="A" pad="A1 A2 A3 A4 A5 A6 A7 A8 A9"/>
<connect gate="G$1" pin="C" pad="K1 K2 K3 K4 K5 K6 K7 K8 K9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0805" package="SMD-0805">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0805P" package="LED_0805_PIPE">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_ARRAY_50D_1206" package="LED_ARRAY_50DEG_1206">
<connects>
<connect gate="G$1" pin="A" pad="A1 A2 A3 A4 A5 A6 A7"/>
<connect gate="G$1" pin="C" pad="C1 C2 C3 C4 C5 C6 C7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_ARRAY_60D_4PLCC" package="LED_ARRAY_60DEG_4PLCC">
<connects>
<connect gate="G$1" pin="A" pad="A1 A2 A3 A4 A5 A6 A7 A8 A9 A10 A11 A12 A13 A14 A15 A16 A17 A18"/>
<connect gate="G$1" pin="C" pad="C C1 C2 C3 C4 C5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0603" package="SMD-0603">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_1206" package="SMD-1206">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_1206P" package="LED_1206_PIPE">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_TH2" package="HDR_2X1">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PWR-JACK-CUI" prefix="CON">
<description>DC CONN POWER JACK - HI CUR</description>
<gates>
<gate name="DC_JACK" symbol="POWER-JACK-CUI" x="0" y="0"/>
</gates>
<devices>
<device name="_B" package="PWR_JACK_LRG-B">
<connects>
<connect gate="DC_JACK" pin="GND" pad="-@1 -@2"/>
<connect gate="DC_JACK" pin="POS" pad="+@1 +@2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PART#" value="CP-002AHPJCT-ND"/>
<attribute name="SUPPLIER" value="Digikey"/>
</technology>
</technologies>
</device>
<device name="_A" package="PWR_JACK_LRG-A">
<connects>
<connect gate="DC_JACK" pin="GND" pad="-@1 -@2"/>
<connect gate="DC_JACK" pin="POS" pad="+@1 +@2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FUSE" prefix="F" uservalue="yes">
<description>Fuse/Thermistor/PTC Resettable

&lt;p&gt;PTCs -
&lt;br&gt; PTGL13AR6R8M6C01B0 : 140V, 575mA trip, 0.37A hold, 1A max, TH
&lt;br&gt;1206L075/13.2WR : 13.2V, 1,5A trip, 0.75A hold, 100A max, 1206
&lt;br&gt;MF-NSMF050-2 : 13.2V, 1A trip, 500mA hold, 100A max, 1206
&lt;br&gt;0ZCJ0025AF2E : 14V, 500mA trip, 250mA hold, 40A max, 1206

&lt;p&gt;Non-PTC -
&lt;br&gt;C1Q3 : 3A, 63Vdc, 125Vac, Fast Response, 1206
&lt;br&gt;0698Q : 5A, 350Vac, 140Vdc, Fast, TH 0.2"
&lt;br&gt;SSQ 750: 750mA, 125Vacdc, Fast, 2-SMD</description>
<gates>
<gate name="FUSE" symbol="FUSE" x="0" y="0"/>
</gates>
<devices>
<device name="_TH" package="FUSE_PTGL">
<connects>
<connect gate="FUSE" pin="1" pad="1"/>
<connect gate="FUSE" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_SMD1206" package="SMD-1206">
<connects>
<connect gate="FUSE" pin="1" pad="1"/>
<connect gate="FUSE" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_TH-0.2&quot;" package="TH_0.2&quot;">
<connects>
<connect gate="FUSE" pin="1" pad="1"/>
<connect gate="FUSE" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_SSQ" package="SSQ">
<connects>
<connect gate="FUSE" pin="1" pad="1"/>
<connect gate="FUSE" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="USB">
<description>USB Type A Receptacle

&lt;p&gt;87520-0010BLF</description>
<gates>
<gate name="G$1" symbol="USB" x="0" y="0"/>
</gates>
<devices>
<device name="" package="USB">
<connects>
<connect gate="G$1" pin="D+" pad="3"/>
<connect gate="G$1" pin="D-" pad="2"/>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="SHIELD" pad="SH1 SH2"/>
<connect gate="G$1" pin="VBUS" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HDR_1X1" prefix="H" uservalue="yes">
<description>Single Pin Header</description>
<gates>
<gate name="PIN" symbol="HDR_1X1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="HDR_1X1">
<connects>
<connect gate="PIN" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HDR_2X1" prefix="H" uservalue="yes">
<description>2x1 Header</description>
<gates>
<gate name="HDR_2X1" symbol="HDR_2X1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="HDR_2X1">
<connects>
<connect gate="HDR_2X1" pin="1" pad="1"/>
<connect gate="HDR_2X1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U2" library="BIT_Library" deviceset="TUSB2046BVF" device=""/>
<part name="U3" library="BIT_Library" deviceset="TPS2044BDR" device=""/>
<part name="CON4" library="BIT_Library" deviceset="USB-A_STACK" device="" value="5787745-4"/>
<part name="CON2" library="BIT_Library" deviceset="USB-A_STACK" device="" value="5787745-4"/>
<part name="D3" library="BIT_Library" deviceset="SN75240" device=""/>
<part name="D1" library="BIT_Library" deviceset="SN75240" device=""/>
<part name="U1" library="BIT_Library" deviceset="TLV70233-3.3" device=""/>
<part name="D2" library="BIT_Library" deviceset="SN75240" device=""/>
<part name="SUPPLY3" library="BIT_Library" deviceset="GND" device=""/>
<part name="SUPPLY4" library="BIT_Library" deviceset="GND" device=""/>
<part name="SUPPLY5" library="BIT_Library" deviceset="GND" device=""/>
<part name="SUPPLY6" library="BIT_Library" deviceset="GND" device=""/>
<part name="SUPPLY7" library="BIT_Library" deviceset="GND" device=""/>
<part name="SUPPLY8" library="BIT_Library" deviceset="GND" device=""/>
<part name="SUPPLY9" library="BIT_Library" deviceset="GND" device=""/>
<part name="SUPPLY10" library="BIT_Library" deviceset="GND" device=""/>
<part name="SUPPLY11" library="BIT_Library" deviceset="GND" device=""/>
<part name="SUPPLY12" library="BIT_Library" deviceset="GND" device=""/>
<part name="SUPPLY13" library="BIT_Library" deviceset="GND" device=""/>
<part name="C5" library="BIT_Library" deviceset="CAP" device="_0603" value="0.1uF, 0603"/>
<part name="C6" library="BIT_Library" deviceset="CAP" device="_0603" value="0.1uF, 0603"/>
<part name="SUPPLY23" library="BIT_Library" deviceset="GND" device=""/>
<part name="C4" library="BIT_Library" deviceset="CAP" device="_0805" value="4.7uF, 0805"/>
<part name="C7" library="BIT_Library" deviceset="CAP" device="_0805" value="4.7uF, 0805"/>
<part name="R11" library="BIT_Library" deviceset="RES" device="_0603" value="27, 0603"/>
<part name="R10" library="BIT_Library" deviceset="RES" device="_0603" value="27, 0603"/>
<part name="C10" library="BIT_Library" deviceset="CAP" device="_0603" value="22pF, 0603"/>
<part name="C11" library="BIT_Library" deviceset="CAP" device="_0603" value="22pF, 0603"/>
<part name="R12" library="BIT_Library" deviceset="RES" device="_0603" value="1.5K, 0603"/>
<part name="Y1" library="BIT_Library" deviceset="XTAL_OSC" device="_HC49UP" value="6MHz"/>
<part name="C16" library="BIT_Library" deviceset="CAP" device="_0402" value="18pF, 0402"/>
<part name="C17" library="BIT_Library" deviceset="CAP" device="_0402" value="18pF, 0402"/>
<part name="SUPPLY22" library="BIT_Library" deviceset="GND" device=""/>
<part name="SUPPLY26" library="BIT_Library" deviceset="GND" device=""/>
<part name="C2" library="BIT_Library" deviceset="CAP" device="_0402" value="22pF, 0402"/>
<part name="C3" library="BIT_Library" deviceset="CAP" device="_0402" value="22pF, 0402"/>
<part name="R3" library="BIT_Library" deviceset="RES" device="_0402" value="1.5K, 0402"/>
<part name="R4" library="BIT_Library" deviceset="RES" device="_0402" value="1.5K, 0402"/>
<part name="R2" library="BIT_Library" deviceset="RES" device="_0402" value="27, 0402"/>
<part name="R1" library="BIT_Library" deviceset="RES" device="_0402" value="27, 0402"/>
<part name="C9" library="BIT_Library" deviceset="CAP" device="_0402" value="22pF, 0402"/>
<part name="C8" library="BIT_Library" deviceset="CAP" device="_0402" value="22pF, 0402"/>
<part name="R8" library="BIT_Library" deviceset="RES" device="_0402" value="1.5K, 0402"/>
<part name="R9" library="BIT_Library" deviceset="RES" device="_0402" value="1.5K, 0402"/>
<part name="R6" library="BIT_Library" deviceset="RES" device="_0402" value="27, 0402"/>
<part name="R7" library="BIT_Library" deviceset="RES" device="_0402" value="27, 0402"/>
<part name="C14" library="BIT_Library" deviceset="CAP" device="_0402" value="22pF, 0402"/>
<part name="C15" library="BIT_Library" deviceset="CAP" device="_0402" value="22pF, 0402"/>
<part name="R19" library="BIT_Library" deviceset="RES" device="_0402" value="1.5K, 0402"/>
<part name="R20" library="BIT_Library" deviceset="RES" device="_0402" value="1.5K, 0402"/>
<part name="R17" library="BIT_Library" deviceset="RES" device="_0402" value="27, 0402"/>
<part name="R18" library="BIT_Library" deviceset="RES" device="_0402" value="27, 0402"/>
<part name="C12" library="BIT_Library" deviceset="CAP" device="_0402" value="22pF, 0402"/>
<part name="C13" library="BIT_Library" deviceset="CAP" device="_0402" value="22pF, 0402"/>
<part name="R15" library="BIT_Library" deviceset="RES" device="_0402" value="1.5K, 0402"/>
<part name="R16" library="BIT_Library" deviceset="RES" device="_0402" value="1.5K, 0402"/>
<part name="R13" library="BIT_Library" deviceset="RES" device="_0402" value="27, 0402"/>
<part name="R14" library="BIT_Library" deviceset="RES" device="_0402" value="27, 0402"/>
<part name="C24" library="BIT_Library" deviceset="CAP" device="_0603" value="0.1uF, 0603"/>
<part name="SUPPLY27" library="BIT_Library" deviceset="GND" device=""/>
<part name="C19" library="BIT_Library" deviceset="CAP_POL" device="_2917" value="100uF, 2917"/>
<part name="C18" library="BIT_Library" deviceset="CAP_POL" device="_2917" value="100uF, 2917"/>
<part name="C21" library="BIT_Library" deviceset="CAP_POL" device="_2917" value="100uF, 2917"/>
<part name="C20" library="BIT_Library" deviceset="CAP_POL" device="_2917" value="100uF, 2917"/>
<part name="H1" library="BIT_Library" deviceset="HDR_3X1" device="" value="PWR_SEL"/>
<part name="SUPPLY28" library="BIT_Library" deviceset="GND" device=""/>
<part name="SUPPLY29" library="BIT_Library" deviceset="GND" device=""/>
<part name="SUPPLY30" library="BIT_Library" deviceset="GND" device=""/>
<part name="SUPPLY31" library="BIT_Library" deviceset="GND" device=""/>
<part name="C23" library="BIT_Library" deviceset="CAP" device="_0603" value="0.1uF, 0603"/>
<part name="SUPPLY32" library="BIT_Library" deviceset="GND" device=""/>
<part name="SW1" library="BIT_Library" deviceset="SW_TACTILE" device="_KMR" value="KMR231GLFS"/>
<part name="R21" library="BIT_Library" deviceset="RES" device="_0603" value="10K, 0603"/>
<part name="C22" library="BIT_Library" deviceset="CAP" device="_0603" value="1uF, 0603"/>
<part name="SUPPLY33" library="BIT_Library" deviceset="GND" device=""/>
<part name="SUPPLY34" library="BIT_Library" deviceset="GND" device=""/>
<part name="TP3" library="BIT_Library" deviceset="TESTPOINT" device="_SMD" value="TP_5VUSB"/>
<part name="TP2" library="BIT_Library" deviceset="TESTPOINT" device="_SMD" value="TP_3V3USB"/>
<part name="TP4" library="BIT_Library" deviceset="TESTPOINT" device="_SMD" value="TP_VBUS4"/>
<part name="TP5" library="BIT_Library" deviceset="TESTPOINT" device="_SMD" value="TP_VBUS3"/>
<part name="TP6" library="BIT_Library" deviceset="TESTPOINT" device="_SMD" value="TP_VBUS2"/>
<part name="TP7" library="BIT_Library" deviceset="TESTPOINT" device="_SMD" value="TP_VBUS1"/>
<part name="R5" library="BIT_Library" deviceset="RES" device="_0603" value="600, 0603"/>
<part name="LD1" library="BIT_Library" deviceset="LED" device="_0603" value="LED_USB"/>
<part name="SUPPLY43" library="BIT_Library" deviceset="GND" device=""/>
<part name="CON1" library="BIT_Library" deviceset="PWR-JACK-CUI" device="_A"/>
<part name="SUPPLY1" library="BIT_Library" deviceset="GND" device=""/>
<part name="F1" library="BIT_Library" deviceset="FUSE" device="_SMD1206" value="C1Q3"/>
<part name="TP1" library="BIT_Library" deviceset="TESTPOINT" device="_SMD" value="TP_5V"/>
<part name="CON3" library="BIT_Library" deviceset="USB" device=""/>
<part name="SUPPLY14" library="BIT_Library" deviceset="GND" device=""/>
<part name="H2" library="BIT_Library" deviceset="HDR_1X1" device=""/>
<part name="SUPPLY2" library="BIT_Library" deviceset="GND" device=""/>
<part name="C25" library="BIT_Library" deviceset="CAP_POL" device="_6.6" value="100uF, 6.6mm"/>
<part name="H3" library="BIT_Library" deviceset="HDR_2X1" device="" value="GND"/>
<part name="SUPPLY17" library="BIT_Library" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="2.54" y="2.54" size="2.54" layer="97">USB 2.0 (x4) Hub w/ External Power
Rev 1.0
SBS 2015</text>
</plain>
<instances>
<instance part="U2" gate="G$1" x="96.52" y="88.9"/>
<instance part="U3" gate="G$1" x="147.32" y="45.72"/>
<instance part="CON4" gate="G$1" x="246.38" y="78.74"/>
<instance part="CON2" gate="G$1" x="246.38" y="134.62"/>
<instance part="D3" gate="G$1" x="220.98" y="81.28"/>
<instance part="D1" gate="G$1" x="220.98" y="137.16"/>
<instance part="U1" gate="TLV72033" x="66.04" y="167.64"/>
<instance part="D2" gate="G$1" x="40.64" y="99.06"/>
<instance part="SUPPLY3" gate="G$1" x="48.26" y="93.98"/>
<instance part="SUPPLY4" gate="G$1" x="33.02" y="96.52"/>
<instance part="SUPPLY5" gate="G$1" x="213.36" y="134.62"/>
<instance part="SUPPLY6" gate="G$1" x="228.6" y="132.08"/>
<instance part="SUPPLY7" gate="G$1" x="213.36" y="78.74"/>
<instance part="SUPPLY8" gate="G$1" x="228.6" y="76.2"/>
<instance part="SUPPLY9" gate="G$1" x="160.02" y="25.4"/>
<instance part="SUPPLY10" gate="G$1" x="71.12" y="43.18"/>
<instance part="SUPPLY11" gate="G$1" x="50.8" y="157.48"/>
<instance part="SUPPLY12" gate="G$1" x="251.46" y="106.68"/>
<instance part="SUPPLY13" gate="G$1" x="251.46" y="50.8"/>
<instance part="C5" gate="G$1" x="40.64" y="167.64"/>
<instance part="C6" gate="G$1" x="91.44" y="167.64"/>
<instance part="SUPPLY23" gate="G$1" x="91.44" y="157.48"/>
<instance part="C4" gate="G$1" x="30.48" y="167.64"/>
<instance part="C7" gate="G$1" x="101.6" y="167.64"/>
<instance part="R11" gate="G$1" x="43.18" y="121.92" rot="R90"/>
<instance part="R10" gate="G$1" x="33.02" y="121.92" rot="R90"/>
<instance part="C10" gate="G$1" x="55.88" y="106.68"/>
<instance part="C11" gate="G$1" x="66.04" y="106.68"/>
<instance part="R12" gate="G$1" x="66.04" y="121.92" rot="R90"/>
<instance part="Y1" gate="G$1" x="35.56" y="78.74"/>
<instance part="C16" gate="G$1" x="30.48" y="68.58"/>
<instance part="C17" gate="G$1" x="40.64" y="68.58"/>
<instance part="SUPPLY22" gate="G$1" x="30.48" y="60.96"/>
<instance part="SUPPLY26" gate="G$1" x="40.64" y="60.96"/>
<instance part="C2" gate="G$1" x="160.02" y="165.1"/>
<instance part="C3" gate="G$1" x="170.18" y="165.1"/>
<instance part="R3" gate="G$1" x="180.34" y="165.1" rot="R90"/>
<instance part="R4" gate="G$1" x="187.96" y="165.1" rot="R90"/>
<instance part="R2" gate="G$1" x="147.32" y="167.64"/>
<instance part="R1" gate="G$1" x="147.32" y="175.26"/>
<instance part="C9" gate="G$1" x="167.64" y="137.16"/>
<instance part="C8" gate="G$1" x="157.48" y="137.16"/>
<instance part="R8" gate="G$1" x="177.8" y="134.62" rot="R270"/>
<instance part="R9" gate="G$1" x="185.42" y="134.62" rot="R270"/>
<instance part="R6" gate="G$1" x="147.32" y="147.32"/>
<instance part="R7" gate="G$1" x="147.32" y="139.7"/>
<instance part="C14" gate="G$1" x="157.48" y="78.74"/>
<instance part="C15" gate="G$1" x="167.64" y="78.74"/>
<instance part="R19" gate="G$1" x="177.8" y="78.74" rot="R90"/>
<instance part="R20" gate="G$1" x="185.42" y="78.74" rot="R90"/>
<instance part="R17" gate="G$1" x="144.78" y="88.9"/>
<instance part="R18" gate="G$1" x="144.78" y="81.28"/>
<instance part="C12" gate="G$1" x="157.48" y="106.68"/>
<instance part="C13" gate="G$1" x="167.64" y="106.68"/>
<instance part="R15" gate="G$1" x="177.8" y="106.68" rot="R90"/>
<instance part="R16" gate="G$1" x="185.42" y="106.68" rot="R90"/>
<instance part="R13" gate="G$1" x="147.32" y="116.84"/>
<instance part="R14" gate="G$1" x="147.32" y="109.22"/>
<instance part="C24" gate="G$1" x="134.62" y="25.4"/>
<instance part="SUPPLY27" gate="G$1" x="134.62" y="17.78"/>
<instance part="C19" gate="G$1" x="193.04" y="40.64"/>
<instance part="C18" gate="G$1" x="185.42" y="48.26"/>
<instance part="C21" gate="G$1" x="177.8" y="38.1"/>
<instance part="C20" gate="G$1" x="167.64" y="38.1"/>
<instance part="H1" gate="HDR_3X1" x="22.86" y="147.32"/>
<instance part="SUPPLY28" gate="G$1" x="160.02" y="154.94"/>
<instance part="SUPPLY29" gate="G$1" x="157.48" y="127"/>
<instance part="SUPPLY30" gate="G$1" x="157.48" y="96.52"/>
<instance part="SUPPLY31" gate="G$1" x="157.48" y="68.58"/>
<instance part="C23" gate="G$1" x="124.46" y="25.4"/>
<instance part="SUPPLY32" gate="G$1" x="124.46" y="17.78"/>
<instance part="SW1" gate="G$1" x="33.02" y="35.56"/>
<instance part="R21" gate="G$1" x="43.18" y="43.18" rot="R90"/>
<instance part="C22" gate="G$1" x="43.18" y="30.48"/>
<instance part="SUPPLY33" gate="G$1" x="27.94" y="33.02"/>
<instance part="SUPPLY34" gate="G$1" x="43.18" y="22.86"/>
<instance part="TP3" gate="G$1" x="43.18" y="149.86"/>
<instance part="TP2" gate="G$1" x="91.44" y="175.26"/>
<instance part="TP4" gate="G$1" x="172.72" y="63.5"/>
<instance part="TP5" gate="G$1" x="177.8" y="63.5"/>
<instance part="TP6" gate="G$1" x="185.42" y="63.5"/>
<instance part="TP7" gate="G$1" x="193.04" y="63.5"/>
<instance part="R5" gate="G$1" x="111.76" y="167.64" rot="R90"/>
<instance part="LD1" gate="G$1" x="111.76" y="154.94"/>
<instance part="SUPPLY43" gate="G$1" x="111.76" y="149.86"/>
<instance part="CON1" gate="DC_JACK" x="22.86" y="185.42"/>
<instance part="SUPPLY1" gate="G$1" x="27.94" y="180.34"/>
<instance part="F1" gate="FUSE" x="40.64" y="187.96"/>
<instance part="TP1" gate="G$1" x="50.8" y="190.5"/>
<instance part="CON3" gate="G$1" x="12.7" y="129.54" rot="MR0"/>
<instance part="SUPPLY14" gate="G$1" x="7.62" y="109.22"/>
<instance part="H2" gate="PIN" x="53.34" y="25.4" rot="R90"/>
<instance part="SUPPLY2" gate="G$1" x="45.72" y="132.08"/>
<instance part="C25" gate="G$1" x="45.72" y="139.7"/>
<instance part="H3" gate="HDR_2X1" x="68.58" y="193.04"/>
<instance part="SUPPLY17" gate="G$1" x="78.74" y="187.96"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="GND1"/>
<pinref part="SUPPLY3" gate="G$1" pin="GND"/>
<wire x1="48.26" y1="96.52" x2="48.26" y2="99.06" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="2"/>
<wire x1="48.26" y1="99.06" x2="55.88" y2="99.06" width="0.1524" layer="91"/>
<wire x1="55.88" y1="99.06" x2="66.04" y2="99.06" width="0.1524" layer="91"/>
<wire x1="66.04" y1="99.06" x2="66.04" y2="101.6" width="0.1524" layer="91"/>
<junction x="48.26" y="99.06"/>
<pinref part="C10" gate="G$1" pin="2"/>
<wire x1="55.88" y1="99.06" x2="55.88" y2="101.6" width="0.1524" layer="91"/>
<junction x="55.88" y="99.06"/>
</segment>
<segment>
<pinref part="D2" gate="G$1" pin="GND"/>
<pinref part="SUPPLY4" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="D1" gate="G$1" pin="GND"/>
<pinref part="SUPPLY5" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="D1" gate="G$1" pin="GND1"/>
<pinref part="SUPPLY6" gate="G$1" pin="GND"/>
<wire x1="228.6" y1="134.62" x2="228.6" y2="137.16" width="0.1524" layer="91"/>
<wire x1="228.6" y1="137.16" x2="231.14" y2="137.16" width="0.1524" layer="91"/>
<wire x1="231.14" y1="137.16" x2="231.14" y2="139.7" width="0.1524" layer="91"/>
<junction x="228.6" y="137.16"/>
<pinref part="CON2" gate="G$1" pin="GND_T"/>
<wire x1="231.14" y1="139.7" x2="236.22" y2="139.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D3" gate="G$1" pin="GND"/>
<pinref part="SUPPLY7" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="D3" gate="G$1" pin="GND1"/>
<pinref part="SUPPLY8" gate="G$1" pin="GND"/>
<wire x1="228.6" y1="78.74" x2="228.6" y2="81.28" width="0.1524" layer="91"/>
<wire x1="228.6" y1="81.28" x2="233.68" y2="81.28" width="0.1524" layer="91"/>
<wire x1="233.68" y1="81.28" x2="233.68" y2="83.82" width="0.1524" layer="91"/>
<junction x="228.6" y="81.28"/>
<pinref part="CON4" gate="G$1" pin="GND_T"/>
<wire x1="233.68" y1="83.82" x2="236.22" y2="83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="GND"/>
<pinref part="SUPPLY9" gate="G$1" pin="GND"/>
<wire x1="160.02" y1="27.94" x2="160.02" y2="30.48" width="0.1524" layer="91"/>
<pinref part="C19" gate="G$1" pin="-"/>
<wire x1="160.02" y1="30.48" x2="167.64" y2="30.48" width="0.1524" layer="91"/>
<wire x1="167.64" y1="30.48" x2="177.8" y2="30.48" width="0.1524" layer="91"/>
<wire x1="177.8" y1="30.48" x2="185.42" y2="30.48" width="0.1524" layer="91"/>
<wire x1="185.42" y1="30.48" x2="193.04" y2="30.48" width="0.1524" layer="91"/>
<wire x1="193.04" y1="30.48" x2="193.04" y2="35.56" width="0.1524" layer="91"/>
<junction x="160.02" y="30.48"/>
<pinref part="C18" gate="G$1" pin="-"/>
<wire x1="185.42" y1="43.18" x2="185.42" y2="30.48" width="0.1524" layer="91"/>
<junction x="185.42" y="30.48"/>
<pinref part="C21" gate="G$1" pin="-"/>
<wire x1="177.8" y1="33.02" x2="177.8" y2="30.48" width="0.1524" layer="91"/>
<junction x="177.8" y="30.48"/>
<pinref part="C20" gate="G$1" pin="-"/>
<wire x1="167.64" y1="33.02" x2="167.64" y2="30.48" width="0.1524" layer="91"/>
<junction x="167.64" y="30.48"/>
</segment>
<segment>
<pinref part="U1" gate="TLV72033" pin="GND"/>
<pinref part="SUPPLY11" gate="G$1" pin="GND"/>
<wire x1="50.8" y1="160.02" x2="50.8" y2="162.56" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="30.48" y1="162.56" x2="30.48" y2="160.02" width="0.1524" layer="91"/>
<wire x1="30.48" y1="160.02" x2="40.64" y2="160.02" width="0.1524" layer="91"/>
<junction x="50.8" y="160.02"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="40.64" y1="160.02" x2="50.8" y2="160.02" width="0.1524" layer="91"/>
<wire x1="40.64" y1="162.56" x2="40.64" y2="160.02" width="0.1524" layer="91"/>
<junction x="40.64" y="160.02"/>
</segment>
<segment>
<pinref part="SUPPLY12" gate="G$1" pin="GND"/>
<pinref part="CON2" gate="G$1" pin="SHIELD"/>
<wire x1="251.46" y1="114.3" x2="251.46" y2="109.22" width="0.1524" layer="91"/>
<junction x="251.46" y="109.22"/>
<pinref part="CON2" gate="G$1" pin="GND_B"/>
<wire x1="236.22" y1="119.38" x2="236.22" y2="109.22" width="0.1524" layer="91"/>
<wire x1="236.22" y1="109.22" x2="251.46" y2="109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="CON4" gate="G$1" pin="SHIELD"/>
<pinref part="SUPPLY13" gate="G$1" pin="GND"/>
<wire x1="251.46" y1="58.42" x2="251.46" y2="53.34" width="0.1524" layer="91"/>
<pinref part="CON4" gate="G$1" pin="GND_B"/>
<wire x1="236.22" y1="63.5" x2="236.22" y2="53.34" width="0.1524" layer="91"/>
<wire x1="236.22" y1="53.34" x2="251.46" y2="53.34" width="0.1524" layer="91"/>
<junction x="251.46" y="53.34"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="EEDATA/GANGED"/>
<wire x1="73.66" y1="73.66" x2="71.12" y2="73.66" width="0.1524" layer="91"/>
<pinref part="SUPPLY10" gate="G$1" pin="GND"/>
<wire x1="71.12" y1="73.66" x2="71.12" y2="66.04" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="GND"/>
<wire x1="71.12" y1="66.04" x2="71.12" y2="48.26" width="0.1524" layer="91"/>
<wire x1="71.12" y1="48.26" x2="71.12" y2="45.72" width="0.1524" layer="91"/>
<wire x1="73.66" y1="48.26" x2="71.12" y2="48.26" width="0.1524" layer="91"/>
<junction x="71.12" y="48.26"/>
<pinref part="U2" gate="G$1" pin="BUSPWR"/>
<wire x1="73.66" y1="66.04" x2="71.12" y2="66.04" width="0.1524" layer="91"/>
<junction x="71.12" y="66.04"/>
</segment>
<segment>
<pinref part="C6" gate="G$1" pin="2"/>
<pinref part="SUPPLY23" gate="G$1" pin="GND"/>
<wire x1="91.44" y1="160.02" x2="91.44" y2="162.56" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="2"/>
<wire x1="101.6" y1="162.56" x2="101.6" y2="160.02" width="0.1524" layer="91"/>
<wire x1="101.6" y1="160.02" x2="91.44" y2="160.02" width="0.1524" layer="91"/>
<junction x="91.44" y="160.02"/>
</segment>
<segment>
<pinref part="C16" gate="G$1" pin="2"/>
<pinref part="SUPPLY22" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="C17" gate="G$1" pin="2"/>
<pinref part="SUPPLY26" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="C24" gate="G$1" pin="2"/>
<pinref part="SUPPLY27" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="C2" gate="G$1" pin="2"/>
<pinref part="SUPPLY28" gate="G$1" pin="GND"/>
<wire x1="160.02" y1="160.02" x2="160.02" y2="157.48" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="160.02" y1="157.48" x2="170.18" y2="157.48" width="0.1524" layer="91"/>
<wire x1="170.18" y1="157.48" x2="180.34" y2="157.48" width="0.1524" layer="91"/>
<wire x1="180.34" y1="157.48" x2="187.96" y2="157.48" width="0.1524" layer="91"/>
<wire x1="187.96" y1="157.48" x2="187.96" y2="160.02" width="0.1524" layer="91"/>
<junction x="160.02" y="157.48"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="180.34" y1="160.02" x2="180.34" y2="157.48" width="0.1524" layer="91"/>
<junction x="180.34" y="157.48"/>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="170.18" y1="160.02" x2="170.18" y2="157.48" width="0.1524" layer="91"/>
<junction x="170.18" y="157.48"/>
</segment>
<segment>
<pinref part="C8" gate="G$1" pin="2"/>
<pinref part="SUPPLY29" gate="G$1" pin="GND"/>
<wire x1="157.48" y1="132.08" x2="157.48" y2="129.54" width="0.1524" layer="91"/>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="157.48" y1="129.54" x2="167.64" y2="129.54" width="0.1524" layer="91"/>
<wire x1="167.64" y1="129.54" x2="177.8" y2="129.54" width="0.1524" layer="91"/>
<wire x1="177.8" y1="129.54" x2="185.42" y2="129.54" width="0.1524" layer="91"/>
<wire x1="185.42" y1="129.54" x2="185.42" y2="132.08" width="0.1524" layer="91"/>
<junction x="157.48" y="129.54"/>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="177.8" y1="132.08" x2="177.8" y2="129.54" width="0.1524" layer="91"/>
<junction x="177.8" y="129.54"/>
<pinref part="C9" gate="G$1" pin="2"/>
<wire x1="167.64" y1="132.08" x2="167.64" y2="129.54" width="0.1524" layer="91"/>
<junction x="167.64" y="129.54"/>
</segment>
<segment>
<pinref part="C12" gate="G$1" pin="2"/>
<pinref part="SUPPLY30" gate="G$1" pin="GND"/>
<wire x1="157.48" y1="101.6" x2="157.48" y2="99.06" width="0.1524" layer="91"/>
<pinref part="R16" gate="G$1" pin="1"/>
<wire x1="157.48" y1="99.06" x2="167.64" y2="99.06" width="0.1524" layer="91"/>
<wire x1="167.64" y1="99.06" x2="177.8" y2="99.06" width="0.1524" layer="91"/>
<wire x1="177.8" y1="99.06" x2="185.42" y2="99.06" width="0.1524" layer="91"/>
<wire x1="185.42" y1="99.06" x2="185.42" y2="101.6" width="0.1524" layer="91"/>
<junction x="157.48" y="99.06"/>
<pinref part="R15" gate="G$1" pin="1"/>
<wire x1="177.8" y1="101.6" x2="177.8" y2="99.06" width="0.1524" layer="91"/>
<junction x="177.8" y="99.06"/>
<pinref part="C13" gate="G$1" pin="2"/>
<wire x1="167.64" y1="101.6" x2="167.64" y2="99.06" width="0.1524" layer="91"/>
<junction x="167.64" y="99.06"/>
</segment>
<segment>
<pinref part="C14" gate="G$1" pin="2"/>
<wire x1="157.48" y1="73.66" x2="157.48" y2="71.12" width="0.1524" layer="91"/>
<wire x1="157.48" y1="71.12" x2="167.64" y2="71.12" width="0.1524" layer="91"/>
<pinref part="R20" gate="G$1" pin="1"/>
<wire x1="167.64" y1="71.12" x2="177.8" y2="71.12" width="0.1524" layer="91"/>
<wire x1="177.8" y1="71.12" x2="185.42" y2="71.12" width="0.1524" layer="91"/>
<wire x1="185.42" y1="73.66" x2="185.42" y2="71.12" width="0.1524" layer="91"/>
<pinref part="R19" gate="G$1" pin="1"/>
<wire x1="177.8" y1="73.66" x2="177.8" y2="71.12" width="0.1524" layer="91"/>
<junction x="177.8" y="71.12"/>
<pinref part="C15" gate="G$1" pin="2"/>
<wire x1="167.64" y1="73.66" x2="167.64" y2="71.12" width="0.1524" layer="91"/>
<junction x="167.64" y="71.12"/>
<pinref part="SUPPLY31" gate="G$1" pin="GND"/>
<junction x="157.48" y="71.12"/>
</segment>
<segment>
<pinref part="C23" gate="G$1" pin="2"/>
<pinref part="SUPPLY32" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="SW1" gate="G$1" pin="1"/>
<pinref part="SUPPLY33" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="SUPPLY34" gate="G$1" pin="GND"/>
<pinref part="C22" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="LD1" gate="G$1" pin="C"/>
<pinref part="SUPPLY43" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="CON1" gate="DC_JACK" pin="GND"/>
<pinref part="SUPPLY1" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="CON3" gate="G$1" pin="SHIELD"/>
<pinref part="SUPPLY14" gate="G$1" pin="GND"/>
<wire x1="7.62" y1="114.3" x2="7.62" y2="111.76" width="0.1524" layer="91"/>
<pinref part="CON3" gate="G$1" pin="GND"/>
<wire x1="7.62" y1="111.76" x2="22.86" y2="111.76" width="0.1524" layer="91"/>
<wire x1="22.86" y1="111.76" x2="22.86" y2="121.92" width="0.1524" layer="91"/>
<junction x="7.62" y="111.76"/>
</segment>
<segment>
<pinref part="SUPPLY2" gate="G$1" pin="GND"/>
<pinref part="C25" gate="G$1" pin="-"/>
</segment>
<segment>
<pinref part="H3" gate="HDR_2X1" pin="1"/>
<pinref part="SUPPLY17" gate="G$1" pin="GND"/>
<wire x1="76.2" y1="195.58" x2="78.74" y2="195.58" width="0.1524" layer="91"/>
<wire x1="78.74" y1="195.58" x2="78.74" y2="193.04" width="0.1524" layer="91"/>
<pinref part="H3" gate="HDR_2X1" pin="2"/>
<wire x1="78.74" y1="193.04" x2="78.74" y2="190.5" width="0.1524" layer="91"/>
<wire x1="76.2" y1="193.04" x2="78.74" y2="193.04" width="0.1524" layer="91"/>
<junction x="78.74" y="193.04"/>
</segment>
</net>
<net name="+3V3_USB" class="0">
<segment>
<pinref part="U1" gate="TLV72033" pin="VOUT"/>
<wire x1="83.82" y1="172.72" x2="91.44" y2="172.72" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="91.44" y1="172.72" x2="101.6" y2="172.72" width="0.1524" layer="91"/>
<wire x1="91.44" y1="170.18" x2="91.44" y2="172.72" width="0.1524" layer="91"/>
<junction x="91.44" y="172.72"/>
<label x="111.76" y="175.26" size="1.778" layer="95" rot="R180"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="101.6" y1="170.18" x2="101.6" y2="172.72" width="0.1524" layer="91"/>
<junction x="101.6" y="172.72"/>
<pinref part="TP2" gate="G$1" pin="1"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="101.6" y1="172.72" x2="111.76" y2="172.72" width="0.1524" layer="91"/>
<wire x1="111.76" y1="172.72" x2="111.76" y2="170.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="VCC"/>
<wire x1="73.66" y1="127" x2="66.04" y2="127" width="0.1524" layer="91"/>
<wire x1="66.04" y1="127" x2="66.04" y2="132.08" width="0.1524" layer="91"/>
<label x="66.04" y="132.08" size="1.778" layer="95" rot="R180"/>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="66.04" y1="127" x2="66.04" y2="124.46" width="0.1524" layer="91"/>
<junction x="66.04" y="127"/>
</segment>
<segment>
<pinref part="R21" gate="G$1" pin="2"/>
<wire x1="43.18" y1="45.72" x2="43.18" y2="48.26" width="0.1524" layer="91"/>
<label x="43.18" y="48.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="BB_D-" class="0">
<segment>
<pinref part="R10" gate="G$1" pin="2"/>
<wire x1="33.02" y1="132.08" x2="33.02" y2="124.46" width="0.1524" layer="91"/>
<pinref part="CON3" gate="G$1" pin="D-"/>
<wire x1="22.86" y1="132.08" x2="33.02" y2="132.08" width="0.1524" layer="91"/>
<label x="22.86" y="132.08" size="1.778" layer="95"/>
</segment>
</net>
<net name="BB_D+" class="0">
<segment>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="43.18" y1="127" x2="43.18" y2="124.46" width="0.1524" layer="91"/>
<pinref part="CON3" gate="G$1" pin="D+"/>
<wire x1="22.86" y1="127" x2="43.18" y2="127" width="0.1524" layer="91"/>
<label x="22.86" y="127" size="1.778" layer="95"/>
</segment>
</net>
<net name="DM0" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="DM0"/>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="73.66" y1="111.76" x2="55.88" y2="111.76" width="0.1524" layer="91"/>
<wire x1="55.88" y1="111.76" x2="38.1" y2="111.76" width="0.1524" layer="91"/>
<wire x1="38.1" y1="111.76" x2="33.02" y2="111.76" width="0.1524" layer="91"/>
<wire x1="33.02" y1="111.76" x2="33.02" y2="116.84" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="A"/>
<wire x1="38.1" y1="109.22" x2="38.1" y2="111.76" width="0.1524" layer="91"/>
<junction x="38.1" y="111.76"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="55.88" y1="109.22" x2="55.88" y2="111.76" width="0.1524" layer="91"/>
<junction x="55.88" y="111.76"/>
</segment>
</net>
<net name="DP0" class="0">
<segment>
<pinref part="R11" gate="G$1" pin="1"/>
<wire x1="43.18" y1="116.84" x2="43.18" y2="114.3" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="DP0"/>
<wire x1="43.18" y1="114.3" x2="66.04" y2="114.3" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="B"/>
<wire x1="66.04" y1="114.3" x2="73.66" y2="114.3" width="0.1524" layer="91"/>
<wire x1="43.18" y1="109.22" x2="43.18" y2="114.3" width="0.1524" layer="91"/>
<junction x="43.18" y="114.3"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="66.04" y1="109.22" x2="66.04" y2="114.3" width="0.1524" layer="91"/>
<junction x="66.04" y="114.3"/>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="66.04" y1="114.3" x2="66.04" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="USB_XTAL1" class="0">
<segment>
<pinref part="C16" gate="G$1" pin="1"/>
<wire x1="30.48" y1="71.12" x2="30.48" y2="78.74" width="0.1524" layer="91"/>
<pinref part="Y1" gate="G$1" pin="1"/>
<wire x1="30.48" y1="78.74" x2="33.02" y2="78.74" width="0.1524" layer="91"/>
<label x="30.48" y="78.74" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="XTAL1"/>
<wire x1="73.66" y1="99.06" x2="68.58" y2="99.06" width="0.1524" layer="91"/>
<wire x1="68.58" y1="99.06" x2="68.58" y2="96.52" width="0.1524" layer="91"/>
<label x="68.58" y="96.52" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="USB_XTAL2" class="0">
<segment>
<pinref part="Y1" gate="G$1" pin="2"/>
<pinref part="C17" gate="G$1" pin="1"/>
<wire x1="38.1" y1="78.74" x2="40.64" y2="78.74" width="0.1524" layer="91"/>
<wire x1="40.64" y1="78.74" x2="40.64" y2="71.12" width="0.1524" layer="91"/>
<label x="40.64" y="78.74" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="XTAL2"/>
<wire x1="73.66" y1="91.44" x2="68.58" y2="91.44" width="0.1524" layer="91"/>
<label x="68.58" y="91.44" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="!USB_NRST" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="!RESET"/>
<wire x1="73.66" y1="81.28" x2="71.12" y2="81.28" width="0.1524" layer="91"/>
<label x="71.12" y="81.28" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="R21" gate="G$1" pin="1"/>
<wire x1="43.18" y1="38.1" x2="43.18" y2="35.56" width="0.1524" layer="91"/>
<pinref part="SW1" gate="G$1" pin="2"/>
<wire x1="38.1" y1="35.56" x2="43.18" y2="35.56" width="0.1524" layer="91"/>
<junction x="43.18" y="35.56"/>
<pinref part="C22" gate="G$1" pin="1"/>
<wire x1="43.18" y1="35.56" x2="43.18" y2="33.02" width="0.1524" layer="91"/>
<label x="60.96" y="38.1" size="1.778" layer="95" rot="R180"/>
<pinref part="H2" gate="PIN" pin="1"/>
<wire x1="43.18" y1="35.56" x2="53.34" y2="35.56" width="0.1524" layer="91"/>
<wire x1="53.34" y1="35.56" x2="53.34" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DP1_T" class="0">
<segment>
<wire x1="152.4" y1="172.72" x2="170.18" y2="172.72" width="0.1524" layer="91"/>
<wire x1="170.18" y1="172.72" x2="187.96" y2="172.72" width="0.1524" layer="91"/>
<wire x1="187.96" y1="172.72" x2="193.04" y2="172.72" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="170.18" y1="167.64" x2="170.18" y2="172.72" width="0.1524" layer="91"/>
<junction x="170.18" y="172.72"/>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="187.96" y1="167.64" x2="187.96" y2="172.72" width="0.1524" layer="91"/>
<junction x="187.96" y="172.72"/>
<label x="193.04" y="172.72" size="1.778" layer="95"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="149.86" y1="175.26" x2="152.4" y2="175.26" width="0.1524" layer="91"/>
<wire x1="152.4" y1="175.26" x2="152.4" y2="172.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D1" gate="G$1" pin="B"/>
<wire x1="223.52" y1="147.32" x2="231.14" y2="147.32" width="0.1524" layer="91"/>
<wire x1="231.14" y1="147.32" x2="231.14" y2="144.78" width="0.1524" layer="91"/>
<pinref part="CON2" gate="G$1" pin="D+_T"/>
<wire x1="231.14" y1="144.78" x2="236.22" y2="144.78" width="0.1524" layer="91"/>
<label x="223.52" y="147.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="DM1_T" class="0">
<segment>
<wire x1="152.4" y1="170.18" x2="160.02" y2="170.18" width="0.1524" layer="91"/>
<wire x1="160.02" y1="170.18" x2="180.34" y2="170.18" width="0.1524" layer="91"/>
<wire x1="180.34" y1="170.18" x2="193.04" y2="170.18" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="160.02" y1="167.64" x2="160.02" y2="170.18" width="0.1524" layer="91"/>
<junction x="160.02" y="170.18"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="180.34" y1="167.64" x2="180.34" y2="170.18" width="0.1524" layer="91"/>
<junction x="180.34" y="170.18"/>
<label x="193.04" y="170.18" size="1.778" layer="95"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="149.86" y1="167.64" x2="152.4" y2="167.64" width="0.1524" layer="91"/>
<wire x1="152.4" y1="167.64" x2="152.4" y2="170.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D1" gate="G$1" pin="A"/>
<wire x1="218.44" y1="147.32" x2="218.44" y2="149.86" width="0.1524" layer="91"/>
<wire x1="218.44" y1="149.86" x2="233.68" y2="149.86" width="0.1524" layer="91"/>
<wire x1="233.68" y1="149.86" x2="233.68" y2="147.32" width="0.1524" layer="91"/>
<pinref part="CON2" gate="G$1" pin="D-_T"/>
<wire x1="233.68" y1="147.32" x2="236.22" y2="147.32" width="0.1524" layer="91"/>
<label x="218.44" y="149.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="DP2_B" class="0">
<segment>
<wire x1="152.4" y1="144.78" x2="167.64" y2="144.78" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="167.64" y1="144.78" x2="185.42" y2="144.78" width="0.1524" layer="91"/>
<wire x1="185.42" y1="144.78" x2="190.5" y2="144.78" width="0.1524" layer="91"/>
<wire x1="149.86" y1="147.32" x2="152.4" y2="147.32" width="0.1524" layer="91"/>
<wire x1="152.4" y1="147.32" x2="152.4" y2="144.78" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="167.64" y1="139.7" x2="167.64" y2="144.78" width="0.1524" layer="91"/>
<junction x="167.64" y="144.78"/>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="185.42" y1="139.7" x2="185.42" y2="144.78" width="0.1524" layer="91"/>
<junction x="185.42" y="144.78"/>
<label x="190.5" y="144.78" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="D1" gate="G$1" pin="C"/>
<wire x1="218.44" y1="127" x2="218.44" y2="124.46" width="0.1524" layer="91"/>
<pinref part="CON2" gate="G$1" pin="D+_B"/>
<wire x1="218.44" y1="124.46" x2="236.22" y2="124.46" width="0.1524" layer="91"/>
<label x="223.52" y="124.46" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DM2_B" class="0">
<segment>
<wire x1="152.4" y1="142.24" x2="157.48" y2="142.24" width="0.1524" layer="91"/>
<label x="190.5" y="142.24" size="1.778" layer="95"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="157.48" y1="142.24" x2="177.8" y2="142.24" width="0.1524" layer="91"/>
<wire x1="177.8" y1="142.24" x2="190.5" y2="142.24" width="0.1524" layer="91"/>
<wire x1="149.86" y1="139.7" x2="152.4" y2="139.7" width="0.1524" layer="91"/>
<wire x1="152.4" y1="139.7" x2="152.4" y2="142.24" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="157.48" y1="139.7" x2="157.48" y2="142.24" width="0.1524" layer="91"/>
<junction x="157.48" y="142.24"/>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="177.8" y1="139.7" x2="177.8" y2="142.24" width="0.1524" layer="91"/>
<junction x="177.8" y="142.24"/>
</segment>
<segment>
<pinref part="D1" gate="G$1" pin="D"/>
<pinref part="CON2" gate="G$1" pin="D-_B"/>
<wire x1="223.52" y1="127" x2="236.22" y2="127" width="0.1524" layer="91"/>
<label x="228.6" y="127" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DP4_B" class="0">
<segment>
<wire x1="167.64" y1="86.36" x2="185.42" y2="86.36" width="0.1524" layer="91"/>
<wire x1="185.42" y1="86.36" x2="190.5" y2="86.36" width="0.1524" layer="91"/>
<pinref part="C15" gate="G$1" pin="1"/>
<wire x1="167.64" y1="81.28" x2="167.64" y2="86.36" width="0.1524" layer="91"/>
<pinref part="R20" gate="G$1" pin="2"/>
<wire x1="185.42" y1="81.28" x2="185.42" y2="86.36" width="0.1524" layer="91"/>
<junction x="185.42" y="86.36"/>
<label x="190.5" y="86.36" size="1.778" layer="95"/>
<pinref part="R17" gate="G$1" pin="2"/>
<wire x1="147.32" y1="88.9" x2="149.86" y2="88.9" width="0.1524" layer="91"/>
<wire x1="149.86" y1="88.9" x2="149.86" y2="86.36" width="0.1524" layer="91"/>
<wire x1="149.86" y1="86.36" x2="167.64" y2="86.36" width="0.1524" layer="91"/>
<junction x="167.64" y="86.36"/>
</segment>
<segment>
<pinref part="D3" gate="G$1" pin="C"/>
<wire x1="218.44" y1="71.12" x2="218.44" y2="68.58" width="0.1524" layer="91"/>
<pinref part="CON4" gate="G$1" pin="D+_B"/>
<wire x1="218.44" y1="68.58" x2="236.22" y2="68.58" width="0.1524" layer="91"/>
<label x="223.52" y="68.58" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DM4_B" class="0">
<segment>
<wire x1="157.48" y1="83.82" x2="177.8" y2="83.82" width="0.1524" layer="91"/>
<wire x1="177.8" y1="83.82" x2="190.5" y2="83.82" width="0.1524" layer="91"/>
<pinref part="C14" gate="G$1" pin="1"/>
<wire x1="157.48" y1="81.28" x2="157.48" y2="83.82" width="0.1524" layer="91"/>
<pinref part="R19" gate="G$1" pin="2"/>
<wire x1="177.8" y1="81.28" x2="177.8" y2="83.82" width="0.1524" layer="91"/>
<junction x="177.8" y="83.82"/>
<label x="190.5" y="83.82" size="1.778" layer="95"/>
<pinref part="R18" gate="G$1" pin="2"/>
<wire x1="147.32" y1="81.28" x2="149.86" y2="81.28" width="0.1524" layer="91"/>
<wire x1="149.86" y1="81.28" x2="149.86" y2="83.82" width="0.1524" layer="91"/>
<wire x1="149.86" y1="83.82" x2="157.48" y2="83.82" width="0.1524" layer="91"/>
<junction x="157.48" y="83.82"/>
</segment>
<segment>
<pinref part="D3" gate="G$1" pin="D"/>
<pinref part="CON4" gate="G$1" pin="D-_B"/>
<wire x1="223.52" y1="71.12" x2="236.22" y2="71.12" width="0.1524" layer="91"/>
<label x="231.14" y="71.12" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="!OC4" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="!OVRCUR4"/>
<wire x1="119.38" y1="48.26" x2="119.38" y2="35.56" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="!OC4"/>
<wire x1="119.38" y1="35.56" x2="137.16" y2="35.56" width="0.1524" layer="91"/>
<label x="129.54" y="35.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="!OC3" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="!OVRCUR3"/>
<wire x1="119.38" y1="50.8" x2="121.92" y2="50.8" width="0.1524" layer="91"/>
<wire x1="121.92" y1="50.8" x2="121.92" y2="38.1" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="!OC3"/>
<wire x1="121.92" y1="38.1" x2="137.16" y2="38.1" width="0.1524" layer="91"/>
<label x="129.54" y="38.1" size="1.778" layer="95"/>
</segment>
</net>
<net name="!OC2" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="!OVRCUR2"/>
<wire x1="119.38" y1="53.34" x2="124.46" y2="53.34" width="0.1524" layer="91"/>
<wire x1="124.46" y1="53.34" x2="124.46" y2="40.64" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="!OC2"/>
<wire x1="124.46" y1="40.64" x2="137.16" y2="40.64" width="0.1524" layer="91"/>
<label x="129.54" y="40.64" size="1.778" layer="95"/>
</segment>
</net>
<net name="!OC1" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="!OVRCUR1"/>
<wire x1="119.38" y1="55.88" x2="127" y2="55.88" width="0.1524" layer="91"/>
<wire x1="127" y1="55.88" x2="127" y2="43.18" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="!OC1"/>
<wire x1="127" y1="43.18" x2="137.16" y2="43.18" width="0.1524" layer="91"/>
<label x="129.54" y="43.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="!EN4" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="!EN4"/>
<wire x1="137.16" y1="50.8" x2="129.54" y2="50.8" width="0.1524" layer="91"/>
<wire x1="129.54" y1="50.8" x2="129.54" y2="63.5" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="!PWRON4"/>
<wire x1="129.54" y1="63.5" x2="119.38" y2="63.5" width="0.1524" layer="91"/>
<label x="121.92" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="!EN3" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="!EN3"/>
<wire x1="137.16" y1="53.34" x2="132.08" y2="53.34" width="0.1524" layer="91"/>
<wire x1="132.08" y1="53.34" x2="132.08" y2="66.04" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="!PWRON3"/>
<wire x1="132.08" y1="66.04" x2="119.38" y2="66.04" width="0.1524" layer="91"/>
<label x="121.92" y="66.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="!EN2" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="!EN2"/>
<wire x1="137.16" y1="55.88" x2="134.62" y2="55.88" width="0.1524" layer="91"/>
<wire x1="134.62" y1="55.88" x2="134.62" y2="68.58" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="!PWRON2"/>
<wire x1="134.62" y1="68.58" x2="119.38" y2="68.58" width="0.1524" layer="91"/>
<label x="121.92" y="68.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="!EN1" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="!EN1"/>
<wire x1="137.16" y1="58.42" x2="137.16" y2="71.12" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="!PWRON1"/>
<wire x1="137.16" y1="71.12" x2="119.38" y2="71.12" width="0.1524" layer="91"/>
<label x="121.92" y="71.12" size="1.778" layer="95"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="IN"/>
<wire x1="137.16" y1="30.48" x2="134.62" y2="30.48" width="0.1524" layer="91"/>
<pinref part="C24" gate="G$1" pin="1"/>
<wire x1="134.62" y1="30.48" x2="124.46" y2="30.48" width="0.1524" layer="91"/>
<wire x1="124.46" y1="30.48" x2="121.92" y2="30.48" width="0.1524" layer="91"/>
<wire x1="134.62" y1="27.94" x2="134.62" y2="30.48" width="0.1524" layer="91"/>
<junction x="134.62" y="30.48"/>
<label x="121.92" y="30.48" size="1.27" layer="95" rot="R180"/>
<pinref part="C23" gate="G$1" pin="1"/>
<wire x1="124.46" y1="27.94" x2="124.46" y2="30.48" width="0.1524" layer="91"/>
<junction x="124.46" y="30.48"/>
</segment>
<segment>
<pinref part="F1" gate="FUSE" pin="2"/>
<wire x1="48.26" y1="187.96" x2="50.8" y2="187.96" width="0.1524" layer="91"/>
<label x="53.34" y="187.96" size="1.778" layer="95"/>
<pinref part="TP1" gate="G$1" pin="1"/>
<wire x1="50.8" y1="187.96" x2="53.34" y2="187.96" width="0.1524" layer="91"/>
<junction x="50.8" y="187.96"/>
</segment>
<segment>
<pinref part="H1" gate="HDR_3X1" pin="1"/>
<wire x1="30.48" y1="149.86" x2="33.02" y2="149.86" width="0.1524" layer="91"/>
<label x="33.02" y="149.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="VBUS4" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="OUT4"/>
<pinref part="C20" gate="G$1" pin="+"/>
<wire x1="160.02" y1="43.18" x2="167.64" y2="43.18" width="0.1524" layer="91"/>
<wire x1="167.64" y1="43.18" x2="167.64" y2="40.64" width="0.1524" layer="91"/>
<label x="162.56" y="43.18" size="1.778" layer="95"/>
<pinref part="TP4" gate="G$1" pin="1"/>
<wire x1="167.64" y1="43.18" x2="172.72" y2="43.18" width="0.1524" layer="91"/>
<wire x1="172.72" y1="43.18" x2="172.72" y2="60.96" width="0.1524" layer="91"/>
<junction x="167.64" y="43.18"/>
</segment>
<segment>
<pinref part="CON4" gate="G$1" pin="VBUS_B"/>
<wire x1="236.22" y1="76.2" x2="233.68" y2="76.2" width="0.1524" layer="91"/>
<wire x1="233.68" y1="76.2" x2="233.68" y2="66.04" width="0.1524" layer="91"/>
<wire x1="233.68" y1="66.04" x2="231.14" y2="66.04" width="0.1524" layer="91"/>
<label x="231.14" y="66.04" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="VBUS3" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="OUT3"/>
<pinref part="C21" gate="G$1" pin="+"/>
<wire x1="160.02" y1="48.26" x2="177.8" y2="48.26" width="0.1524" layer="91"/>
<wire x1="177.8" y1="48.26" x2="177.8" y2="40.64" width="0.1524" layer="91"/>
<label x="162.56" y="48.26" size="1.778" layer="95"/>
<pinref part="TP5" gate="G$1" pin="1"/>
<wire x1="177.8" y1="48.26" x2="177.8" y2="60.96" width="0.1524" layer="91"/>
<junction x="177.8" y="48.26"/>
</segment>
<segment>
<pinref part="CON4" gate="G$1" pin="VBUS_T"/>
<wire x1="236.22" y1="96.52" x2="233.68" y2="96.52" width="0.1524" layer="91"/>
<wire x1="233.68" y1="96.52" x2="233.68" y2="99.06" width="0.1524" layer="91"/>
<label x="233.68" y="99.06" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="VBUS2" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="OUT2"/>
<pinref part="C18" gate="G$1" pin="+"/>
<wire x1="160.02" y1="53.34" x2="185.42" y2="53.34" width="0.1524" layer="91"/>
<wire x1="185.42" y1="53.34" x2="185.42" y2="50.8" width="0.1524" layer="91"/>
<label x="162.56" y="53.34" size="1.778" layer="95"/>
<pinref part="TP6" gate="G$1" pin="1"/>
<wire x1="185.42" y1="53.34" x2="185.42" y2="60.96" width="0.1524" layer="91"/>
<junction x="185.42" y="53.34"/>
</segment>
<segment>
<pinref part="CON2" gate="G$1" pin="VBUS_B"/>
<wire x1="236.22" y1="132.08" x2="233.68" y2="132.08" width="0.1524" layer="91"/>
<wire x1="233.68" y1="132.08" x2="233.68" y2="121.92" width="0.1524" layer="91"/>
<wire x1="233.68" y1="121.92" x2="231.14" y2="121.92" width="0.1524" layer="91"/>
<label x="231.14" y="121.92" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="VBUS1" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="OUT1"/>
<wire x1="160.02" y1="58.42" x2="193.04" y2="58.42" width="0.1524" layer="91"/>
<pinref part="C19" gate="G$1" pin="+"/>
<wire x1="193.04" y1="58.42" x2="193.04" y2="43.18" width="0.1524" layer="91"/>
<label x="162.56" y="58.42" size="1.778" layer="95"/>
<pinref part="TP7" gate="G$1" pin="1"/>
<wire x1="193.04" y1="58.42" x2="193.04" y2="60.96" width="0.1524" layer="91"/>
<junction x="193.04" y="58.42"/>
</segment>
<segment>
<pinref part="CON2" gate="G$1" pin="VBUS_T"/>
<wire x1="236.22" y1="152.4" x2="233.68" y2="152.4" width="0.1524" layer="91"/>
<wire x1="233.68" y1="152.4" x2="233.68" y2="154.94" width="0.1524" layer="91"/>
<label x="233.68" y="154.94" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="+5V_USB" class="0">
<segment>
<pinref part="H1" gate="HDR_3X1" pin="2"/>
<wire x1="30.48" y1="147.32" x2="43.18" y2="147.32" width="0.1524" layer="91"/>
<label x="33.02" y="147.32" size="1.27" layer="95"/>
<pinref part="TP3" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="U1" gate="TLV72033" pin="VIN"/>
<wire x1="27.94" y1="172.72" x2="30.48" y2="172.72" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="30.48" y1="172.72" x2="40.64" y2="172.72" width="0.1524" layer="91"/>
<wire x1="40.64" y1="172.72" x2="48.26" y2="172.72" width="0.1524" layer="91"/>
<wire x1="48.26" y1="172.72" x2="50.8" y2="172.72" width="0.1524" layer="91"/>
<wire x1="40.64" y1="170.18" x2="40.64" y2="172.72" width="0.1524" layer="91"/>
<junction x="40.64" y="172.72"/>
<pinref part="U1" gate="TLV72033" pin="EN"/>
<wire x1="50.8" y1="167.64" x2="48.26" y2="167.64" width="0.1524" layer="91"/>
<wire x1="48.26" y1="167.64" x2="48.26" y2="172.72" width="0.1524" layer="91"/>
<junction x="48.26" y="172.72"/>
<label x="27.94" y="172.72" size="1.778" layer="95" rot="R180"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="30.48" y1="170.18" x2="30.48" y2="172.72" width="0.1524" layer="91"/>
<junction x="30.48" y="172.72"/>
</segment>
</net>
<net name="DP3_T" class="0">
<segment>
<pinref part="R13" gate="G$1" pin="2"/>
<wire x1="149.86" y1="116.84" x2="152.4" y2="116.84" width="0.1524" layer="91"/>
<wire x1="152.4" y1="116.84" x2="152.4" y2="114.3" width="0.1524" layer="91"/>
<wire x1="152.4" y1="114.3" x2="167.64" y2="114.3" width="0.1524" layer="91"/>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="167.64" y1="114.3" x2="185.42" y2="114.3" width="0.1524" layer="91"/>
<wire x1="185.42" y1="114.3" x2="190.5" y2="114.3" width="0.1524" layer="91"/>
<wire x1="167.64" y1="109.22" x2="167.64" y2="114.3" width="0.1524" layer="91"/>
<junction x="167.64" y="114.3"/>
<pinref part="R16" gate="G$1" pin="2"/>
<wire x1="185.42" y1="109.22" x2="185.42" y2="114.3" width="0.1524" layer="91"/>
<junction x="185.42" y="114.3"/>
<label x="190.5" y="114.3" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="D3" gate="G$1" pin="B"/>
<wire x1="223.52" y1="91.44" x2="231.14" y2="91.44" width="0.1524" layer="91"/>
<wire x1="231.14" y1="91.44" x2="231.14" y2="88.9" width="0.1524" layer="91"/>
<pinref part="CON4" gate="G$1" pin="D+_T"/>
<wire x1="231.14" y1="88.9" x2="236.22" y2="88.9" width="0.1524" layer="91"/>
<label x="223.52" y="91.44" size="1.778" layer="95"/>
</segment>
</net>
<net name="DM3_T" class="0">
<segment>
<pinref part="R14" gate="G$1" pin="2"/>
<wire x1="149.86" y1="109.22" x2="152.4" y2="109.22" width="0.1524" layer="91"/>
<wire x1="152.4" y1="109.22" x2="152.4" y2="111.76" width="0.1524" layer="91"/>
<wire x1="152.4" y1="111.76" x2="157.48" y2="111.76" width="0.1524" layer="91"/>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="157.48" y1="111.76" x2="177.8" y2="111.76" width="0.1524" layer="91"/>
<wire x1="177.8" y1="111.76" x2="190.5" y2="111.76" width="0.1524" layer="91"/>
<wire x1="157.48" y1="109.22" x2="157.48" y2="111.76" width="0.1524" layer="91"/>
<junction x="157.48" y="111.76"/>
<pinref part="R15" gate="G$1" pin="2"/>
<wire x1="177.8" y1="109.22" x2="177.8" y2="111.76" width="0.1524" layer="91"/>
<junction x="177.8" y="111.76"/>
<label x="190.5" y="111.76" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="CON4" gate="G$1" pin="D-_T"/>
<wire x1="236.22" y1="91.44" x2="233.68" y2="91.44" width="0.1524" layer="91"/>
<wire x1="233.68" y1="91.44" x2="233.68" y2="93.98" width="0.1524" layer="91"/>
<pinref part="D3" gate="G$1" pin="A"/>
<wire x1="233.68" y1="93.98" x2="218.44" y2="93.98" width="0.1524" layer="91"/>
<wire x1="218.44" y1="93.98" x2="218.44" y2="91.44" width="0.1524" layer="91"/>
<label x="218.44" y="93.98" size="1.778" layer="95"/>
</segment>
</net>
<net name="DP1" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<pinref part="U2" gate="G$1" pin="DP1"/>
<wire x1="119.38" y1="127" x2="127" y2="127" width="0.1524" layer="91"/>
<wire x1="127" y1="127" x2="127" y2="175.26" width="0.1524" layer="91"/>
<wire x1="127" y1="175.26" x2="142.24" y2="175.26" width="0.1524" layer="91"/>
<label x="121.92" y="127" size="1.778" layer="95"/>
</segment>
</net>
<net name="DM1" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="1"/>
<pinref part="U2" gate="G$1" pin="DM1"/>
<wire x1="119.38" y1="124.46" x2="129.54" y2="124.46" width="0.1524" layer="91"/>
<wire x1="129.54" y1="124.46" x2="129.54" y2="167.64" width="0.1524" layer="91"/>
<wire x1="129.54" y1="167.64" x2="142.24" y2="167.64" width="0.1524" layer="91"/>
<label x="121.92" y="124.46" size="1.778" layer="95"/>
</segment>
</net>
<net name="DP2" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="1"/>
<pinref part="U2" gate="G$1" pin="DP2"/>
<wire x1="119.38" y1="114.3" x2="132.08" y2="114.3" width="0.1524" layer="91"/>
<wire x1="132.08" y1="114.3" x2="132.08" y2="147.32" width="0.1524" layer="91"/>
<wire x1="132.08" y1="147.32" x2="142.24" y2="147.32" width="0.1524" layer="91"/>
<label x="121.92" y="114.3" size="1.778" layer="95"/>
</segment>
</net>
<net name="DM2" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="1"/>
<pinref part="U2" gate="G$1" pin="DM2"/>
<wire x1="119.38" y1="111.76" x2="134.62" y2="111.76" width="0.1524" layer="91"/>
<wire x1="134.62" y1="111.76" x2="134.62" y2="139.7" width="0.1524" layer="91"/>
<wire x1="134.62" y1="139.7" x2="142.24" y2="139.7" width="0.1524" layer="91"/>
<label x="121.92" y="111.76" size="1.778" layer="95"/>
</segment>
</net>
<net name="DP3" class="0">
<segment>
<pinref part="R13" gate="G$1" pin="1"/>
<pinref part="U2" gate="G$1" pin="DP3"/>
<wire x1="119.38" y1="101.6" x2="137.16" y2="101.6" width="0.1524" layer="91"/>
<wire x1="137.16" y1="101.6" x2="137.16" y2="116.84" width="0.1524" layer="91"/>
<wire x1="137.16" y1="116.84" x2="142.24" y2="116.84" width="0.1524" layer="91"/>
<label x="121.92" y="101.6" size="1.778" layer="95"/>
</segment>
</net>
<net name="DM3" class="0">
<segment>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="142.24" y1="109.22" x2="139.7" y2="109.22" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="DM3"/>
<wire x1="119.38" y1="99.06" x2="139.7" y2="99.06" width="0.1524" layer="91"/>
<wire x1="139.7" y1="99.06" x2="139.7" y2="109.22" width="0.1524" layer="91"/>
<label x="121.92" y="99.06" size="1.778" layer="95"/>
</segment>
</net>
<net name="DP4" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="DP4"/>
<wire x1="119.38" y1="86.36" x2="134.62" y2="86.36" width="0.1524" layer="91"/>
<wire x1="134.62" y1="86.36" x2="134.62" y2="88.9" width="0.1524" layer="91"/>
<pinref part="R17" gate="G$1" pin="1"/>
<wire x1="134.62" y1="88.9" x2="139.7" y2="88.9" width="0.1524" layer="91"/>
<label x="121.92" y="86.36" size="1.778" layer="95"/>
</segment>
</net>
<net name="DM4" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="DM4"/>
<wire x1="119.38" y1="83.82" x2="134.62" y2="83.82" width="0.1524" layer="91"/>
<wire x1="134.62" y1="83.82" x2="134.62" y2="81.28" width="0.1524" layer="91"/>
<pinref part="R18" gate="G$1" pin="1"/>
<wire x1="134.62" y1="81.28" x2="139.7" y2="81.28" width="0.1524" layer="91"/>
<label x="121.92" y="83.82" size="1.778" layer="95"/>
</segment>
</net>
<net name="LD1_A" class="0">
<segment>
<pinref part="LD1" gate="G$1" pin="A"/>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="111.76" y1="160.02" x2="111.76" y2="162.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWR_IN" class="0">
<segment>
<pinref part="CON1" gate="DC_JACK" pin="POS"/>
<pinref part="F1" gate="FUSE" pin="1"/>
<wire x1="27.94" y1="187.96" x2="35.56" y2="187.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BB_VBUS" class="0">
<segment>
<pinref part="CON3" gate="G$1" pin="VBUS"/>
<label x="22.86" y="137.16" size="1.778" layer="95"/>
<wire x1="22.86" y1="137.16" x2="35.56" y2="137.16" width="0.1524" layer="91"/>
<wire x1="35.56" y1="137.16" x2="35.56" y2="144.78" width="0.1524" layer="91"/>
<pinref part="H1" gate="HDR_3X1" pin="3"/>
<wire x1="35.56" y1="144.78" x2="30.48" y2="144.78" width="0.1524" layer="91"/>
<pinref part="C25" gate="G$1" pin="+"/>
<wire x1="35.56" y1="144.78" x2="45.72" y2="144.78" width="0.1524" layer="91"/>
<wire x1="45.72" y1="144.78" x2="45.72" y2="142.24" width="0.1524" layer="91"/>
<junction x="35.56" y="144.78"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="104,1,73.66,127,U2,VCC,+3V3_USB,,,"/>
<approved hash="202,1,73.66,63.5,U2,TSTMODE,,,,"/>
<approved hash="202,1,73.66,78.74,U2,!EXTMEM,,,,"/>
</errors>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
